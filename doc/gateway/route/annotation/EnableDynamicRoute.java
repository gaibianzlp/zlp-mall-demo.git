package com.zlp.common.route.annotation;

//import com.zlp.common.route.configuration.DynamicRouteAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启动态路由
 * 
 * @author admin
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
//@Import(DynamicRouteAutoConfiguration.class)
public @interface EnableDynamicRoute {
}
