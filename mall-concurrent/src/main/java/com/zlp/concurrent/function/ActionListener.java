package com.zlp.concurrent.function;

import com.zlp.concurrent.function.entity.OrderDTO;


@FunctionalInterface
public interface ActionListener  {

    /**
     * Invoked when an action occurs.
     */
    void actionPerformed(OrderDTO orderDTO);

}