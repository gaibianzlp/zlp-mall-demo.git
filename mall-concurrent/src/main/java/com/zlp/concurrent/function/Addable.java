package com.zlp.concurrent.function;

@FunctionalInterface
public interface Addable {
    int add(int x, int y);
}