package com.zlp.concurrent.function;

public class AddableDemo {
    public static void main(String[] args) {
        useAddable((int x, int y) -> x + y);
    }
 
    public static void useAddable(Addable a) {
        int sum = a.add(10, 20);
        System.out.println("sum="+sum);
    }
}