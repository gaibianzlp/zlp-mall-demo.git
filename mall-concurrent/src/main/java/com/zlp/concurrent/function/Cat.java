package com.zlp.concurrent.function;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname Cat
 * @Description TODO
 * @Date 2023/11/28 12:32
 * @Created by ZouLiPing
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cat {
    String name;
    int age;
}
