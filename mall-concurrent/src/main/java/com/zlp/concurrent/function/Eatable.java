package com.zlp.concurrent.function;

@FunctionalInterface
public interface Eatable {

    void eat();
}