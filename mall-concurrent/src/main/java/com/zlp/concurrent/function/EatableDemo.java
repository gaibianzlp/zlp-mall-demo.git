package com.zlp.concurrent.function;

public class EatableDemo {
    public static void main(String[] args) {
        useEatable(() -> {
                    System.out.println("一天一苹果,医生远离我");
                    System.out.println("一天二苹果,能活100岁");
                }
        );
    }

    private static void useEatable(Eatable e) {
        e.eat();
    }
}