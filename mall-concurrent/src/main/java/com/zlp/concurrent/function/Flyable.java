package com.zlp.concurrent.function;

@FunctionalInterface
public interface Flyable {
    void fly(String s);
}