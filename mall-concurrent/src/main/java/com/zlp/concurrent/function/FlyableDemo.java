package com.zlp.concurrent.function;

public class FlyableDemo {
    public static void main(String[] args) {
        useFlyable(s -> System.out.println(s));
    }
 
    public static void useFlyable(Flyable f) {
        f.fly("风和日丽,晴空万里");
    }
}