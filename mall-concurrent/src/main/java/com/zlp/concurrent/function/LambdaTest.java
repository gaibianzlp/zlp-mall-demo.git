package com.zlp.concurrent.function;

import com.zlp.concurrent.function.entity.OrderDTO;

import java.util.*;

/**
 * @Classname LambdaTest
 * @Description TODO
 * @Date 2023/11/27 18:08
 * @Created by ZouLiPing
 */
public class LambdaTest {


    public static void main(String[] args) {

//        test1();

        test2(orderDTO -> System.out.println(orderDTO));

    }

    private static void test2(ActionListener actionListener) {
        List<OrderDTO> orderList = getOrderList();
        Optional<OrderDTO> maxOptionalOrder = orderList.stream().max(Comparator.comparingInt(OrderDTO::getSumFee));
        // 求最高价格的订单
        actionListener.actionPerformed(maxOptionalOrder.get());


    }

    private static void test1() {

        List<OrderDTO> orderList = getOrderList();


        Optional<OrderDTO> maxOptionalOrder = orderList.stream().max(Comparator.comparingInt(OrderDTO::getSumFee));
        // 求最高价格的订单
        maxOptionalOrder.ifPresent(orderDTO -> System.out.println(orderDTO));
    }

    private static List<OrderDTO> getOrderList() {

        List<OrderDTO> orderList = new ArrayList<>();

        List<OrderDTO.OrderItemDTO> orderItemDTOList1 = new ArrayList<>();
        Collections.addAll(orderItemDTOList1,new OrderDTO.OrderItemDTO("耐克运动裤子",200,1,"衣服,裤子,鞋子"),new OrderDTO.OrderItemDTO("iphone14Plus",7200,1,"电器,手机,iPhone"));
        OrderDTO orderDTO1 = new OrderDTO("JD1234511227001","上海市浦东新区",234,"182713456",7400,2,10,orderItemDTOList1);

        List<OrderDTO.OrderItemDTO> orderItemDTOList2 = new ArrayList<>();
        Collections.addAll(orderItemDTOList2,new OrderDTO.OrderItemDTO("LOCK&LOCK运动水杯",80,1,"日常生活用品,杯子,LOCK"));
        OrderDTO orderDTO2 = new OrderDTO("JD1234511227002","上海市浦东新区",673,"1827134561",80,1,10,orderItemDTOList1);

        List<OrderDTO.OrderItemDTO> orderItemDTOList3 = new ArrayList<>();
        Collections.addAll(orderItemDTOList3,new OrderDTO.OrderItemDTO("纸巾派位500张",20,10,"日常生活用品,纸巾,派位"),new OrderDTO.OrderItemDTO("HUAWEI-Mate50Pro",6200,1,"电器,手机,华为"));
        OrderDTO orderDTO3 = new OrderDTO("JD1234511227003","上海市浦东新区",345,"182713456",6400,11,10,orderItemDTOList1);
        Collections.addAll(orderList,orderDTO1,orderDTO2,orderDTO3);

        return orderList;
    }
}
