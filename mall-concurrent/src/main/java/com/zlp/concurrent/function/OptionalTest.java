package com.zlp.concurrent.function;

import java.util.Optional;

/**
 * @Classname OptionTest
 * @Description TODO
 * @Date 2023/11/28 12:01
 * @Created by ZouLiPing
 */
public class OptionalTest {

    public static void main(String[] args) {
//        System.out.println(Optional.ofNullable(null).isPresent());
//
//        System.out.println(Optional.ofNullable(new Cat("tom", 3)).orElse(new Cat("rose", 4)));
//        System.out.println(Optional.ofNullable(null).orElse(new Cat("rose", 4)));

//        System.out.println(Optional.ofNullable(new Cat("tom", 3)).orElse(new Cat("rose", 4)));
//        System.out.println(Optional.ofNullable(null).orElse(new Cat("rose", 4)));

//        System.out.println(Optional.ofNullable(new Cat("tom", 3)).orElseThrow(() -> new RuntimeException("cat为空")));
//        System.out.println(Optional.ofNullable(null).orElseThrow(() -> new RuntimeException("cat为空")));

        System.out.println(Optional.ofNullable(new Cat("tom", 3)).get());
        System.out.println(Optional.ofNullable(null).get());


    }
}
