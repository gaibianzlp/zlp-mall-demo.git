package com.zlp.concurrent.function.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 订单转换对象信息
 * @Classname OrderDTO
 * @Date 2023/11/27 18:08
 * @Created by ZouLiPing
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO implements Serializable {

    /**
     *  订单号
     */
    String orderNo;
    /**
     *  下单地址信息
     */
    String address;
    /**
     * 用户ID
     */
    Integer userId;
    /**
     * 手机号码
     */
    String phone;
    /**
     * 订单商品总金额
     */
    Integer sumFee;
    /**
     * 订单商品总数量
     */
    Integer sumQuantity;

    /**
     * 订单状态
     */
    Integer orderStatus;

    List<OrderItemDTO> orderItemDTOList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderItemDTO implements Serializable {

        /**
         * 商品名称
         */
        String name;
        /**
         * 商品金额
         */
        Integer fee;
        /**
         * 商品数量
         */
        Integer quantity;

        /**
         * 分类名称，不同等级分类用逗号隔开，如：衣服,鞋子,Nick运动鞋
         */
        String categoryName;

    }
}
