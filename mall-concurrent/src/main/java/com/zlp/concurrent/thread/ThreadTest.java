package com.zlp.concurrent.thread;

/**
 * @Classname ThreadTest
 * @Description TODO
 * @Date 2023/11/23 11:17
 * @Created by ZouLiPing
 */
public class ThreadTest {

    public static void main(String[] args) {
        test2();
    }

    private static void test2() {
        System.out.println(Thread.currentThread().getName()+"主线程执行该方法");
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"执行Runnable方法");
            }
        }).start();

        new Thread(()-> System.out.println(Thread.currentThread().getName()+"执行run方法")).start();


    }
}
