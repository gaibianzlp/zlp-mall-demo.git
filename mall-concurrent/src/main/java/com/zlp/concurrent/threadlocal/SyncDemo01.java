package com.zlp.concurrent.threadlocal;

public class SyncDemo01 {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static void main(String[] args) {
        SyncDemo01 demo01 = new SyncDemo01();
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                String threadName = "设置：线程名称" + Thread.currentThread().getName();
                System.out.print(threadName + "\t");
                demo01.setContent(threadName);
                System.out.println("获取：" + demo01.getContent());
                System.out.println("=============================================");
            }).start();

        }
    }
}
