package com.zlp.concurrent.threadlocal;

public class ThredLocalDemo01 {

    private ThreadLocal<String> content = new ThreadLocal<>();

    public String getContent() {
        return content.get();
    }

    public void setContent(String content1) {
        content.set(content1);
    }

    public static void main(String[] args) {
        ThredLocalDemo01 demo01 = new ThredLocalDemo01();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                String threadName = "设置：线程名称" + Thread.currentThread().getName();
                System.out.print(threadName + "\t");
                demo01.setContent(threadName);
                System.out.println("获取：" + demo01.getContent());
                System.out.println("=============================================");
            }).start();

        }
    }
}
