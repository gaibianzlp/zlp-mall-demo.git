package com.zlp.easyexcel.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.google.common.net.HttpHeaders;
import com.zlp.easyexcel.domain.ExportData;
import com.zlp.easyexcel.domain.ExportTitle;
import com.zlp.easyexcel.excel.ExcelAttach;
import com.zlp.easyexcel.excel.util.ZipUtils;
import com.zlp.easyexcel.util.ExportUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static cn.hutool.core.date.DatePattern.PURE_DATETIME_FORMAT;

@Slf4j
@Controller
@RestController
@Api(tags = "导出模块", value = "ExportController")
public class ExportController {


    @ApiOperation("导出测试")
    @GetMapping("/export01")
    public void export(HttpServletResponse response) {

        //标题数据
        ExportTitle exportTitle = new ExportTitle();
        exportTitle.setTitle("某某大学");
        //列表数据
        List<ExportData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ExportData exportData = new ExportData();
            exportData.setName("张三");
            exportData.setNumber(i);
            list.add(exportData);
        }
        String fileName = DateUtil.format(DateUtil.date(), DateFormat.getDateInstance()) + ".xlsx";
        try {
            String template = "demo.xlsx";
            ExportUtil.fillExcel(response, exportTitle, list, fileName, template);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @ApiOperation("导出测试02")
    @GetMapping("/export02")
    public void export02(HttpServletResponse response) {

        //列表数据
        List<ExportData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ExportData exportData = new ExportData();
            exportData.setName("张三" + i);
            exportData.setNumber(i);
            list.add(exportData);
        }
        String fileName = DateUtil.format(DateUtil.date(), DateFormat.getDateTimeInstance()) + RandomUtil.randomNumbers(5) + ".xlsx";
        try {
            String template = "demo01.xlsx";

            ExportUtil.fillExcel(response, null, list, fileName, template);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("导出测试03")
    @GetMapping("/export03")
    public void exportExcel(HttpServletResponse response) {
        try {
            // 分为两层
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            //获取需要导出的数据
            List<ExportData> dataList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                ExportData exportData = new ExportData();
                exportData.setName("张三" + i);
                exportData.setNumber(i);
                dataList.add(exportData);
            }
            response.setContentType("application/vnd.openxmlformats-officedocument.speadsheetml.sheet");
            response.setCharacterEncoding("UTF-8");
            // 这里URLEncoder.encode可以防止中文乱码, 与EasyExcel无关
            String fileName = URLEncoder.encode("这里是Excel文件名", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), ExportData.class).sheet("巡课截图信息").doWrite(dataList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @ApiOperation("exportZip")
    @GetMapping("/exportZip")
    public void exportZip(HttpServletResponse response) {


        //准备需要导出的数据
        //获取需要导出的数据
        List<ExportData> dataList = getExportData();
        //图片链接的前缀
        String urlPrefix = "";
        byte[] buf = new byte[1024];
        BufferedOutputStream bos = null;
        ZipOutputStream out = null;
        ByteArrayOutputStream baos = getByteArrayOutputStream(response);
        try {
            // 重置
            response.reset();
            // 允许http://127.0.0.1:8080进行跨域访问
//            response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:8099");
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE"); // 允许的访问方法
            response.setHeader("Access-Control-Max-Age", "9600"); // 缓存该结果的时间，单位为秒
            response.setHeader("Access-Control-Allow-Headers",
                    "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers"); // 允许的自定义请求头
            response.setContentType("application/octet-stream");
            response.setCharacterEncoding("utf-8");
            String zipName = RandomUtil.randomNumbers(5);
            response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(zipName + "巡课截图.zip",
                    "UTF-8"));

            bos = new BufferedOutputStream(response.getOutputStream());
            out = new ZipOutputStream(bos);
            // 分为两层


            for (ExportData screenshot : dataList) {
                String imgUrl = urlPrefix + screenshot.getSrc(); //拼接图片完整路径
                //图片重命名
                String imgName = screenshot.getName() + imgUrl.substring(imgUrl.lastIndexOf("."));

                URL url = new URL(imgUrl);
                URLConnection conn = url.openConnection();
                InputStream in = conn.getInputStream();
                out.putNextEntry(new ZipEntry(imgName));
                int len = -1;
                while ((len = in.read(buf)) != -1) {
                    out.write(buf, 0, len);
                }
                in.close();
            }
           /* // 将Workbook写入内存流
            out.putNextEntry(new ZipEntry(RandomUtil.randomNumbers(5)));
            // 将内存流写入Zip文件
            out.write(baos.toByteArray());*/
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (out != null)
                    out.close();
                if (bos != null)
                    bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private List<ExportData> getExportData() {
        List<ExportData> dataList = new ArrayList<>();
        List<String> imageList = Arrays.asList("https://newbie-assets-uat.digitalvolvo.com/picture/03C48C95-905B-4EC7-9512-C0AAE2CCE539.png",
                "https://newbie-assets-uat.digitalvolvo.com/picture/03C48C95-905B-4EC7-9512-C0AAE2CCE539.png",
                "https://newbie-assets-uat.digitalvolvo.com/picture/94C0D3C9-9910-46DA-AA7D-44C3D0DFB020.png",
                "https://newbie-assets-uat.digitalvolvo.com/picture/94C0D3C9-9910-46DA-AA7D-44C3D0DFB020.png",
                "https://newbie-assets-uat.digitalvolvo.com/picture/586247F4-283F-4CF4-960F-48512F5FBC74.jpg",
                "https://newbie-assets-uat.digitalvolvo.com/picture/AD0170BA-5DF0-4CF2-8113-9FCD8F9AC99D.jpg"
        );
        for (int i = 0; i < 4; i++) {
            ExportData exportData = new ExportData();
            exportData.setSrc(imageList.get(i));
            exportData.setName("张三" + i);
            exportData.setNumber(i);
            dataList.add(exportData);
        }
        return dataList;
    }

    public ByteArrayOutputStream getByteArrayOutputStream(HttpServletResponse response) {
        try {
            //首先创建任意一个OutPutStream流
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            //组装header
            OutputStream outputStream = responseSetHeader(response);

            //填充列表开启自动换行,自动换行表示每次写入一条list数据是都会重新生成一行空行,此选项默认是关闭的,需要提前设置为true
            //创建Sheet页、填充格式设置
            //文件模板输入流
            InputStream inputStream = new ClassPathResource("templates/" + "demo02.xlsx").getInputStream();
            ExcelWriter excelWriter = EasyExcel.write(outputStream).withTemplate(inputStream).build();
            WriteSheet writeSheet = EasyExcel.writerSheet().build();
            FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            //导出信息
            List<ExportData> exportDataList = getExportData();
            //开始读取
            excelWriter.fill(exportDataList, fillConfig, writeSheet);
            excelWriter.finish();
            return bos;
        } catch (IOException e) {
            log.error("下载票单异常:{}", e);
        }
        return null;
    }


    private OutputStream responseSetHeader(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
//        response.setHeader("Content-disposition", "attachment;filename=" +RandomUtil.randomNumbers(4));
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.ms-excel");
//        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=invoice.xls");
        // xls
//         response.setContentType("application/vnd.ms-excel");
        // xlsx
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        return response.getOutputStream();
    }


    @SneakyThrows
    @ApiOperation("exportZip02")
    @GetMapping("/exportZip02")
    public void exportZip02(HttpServletResponse response) {
        String now = DateUtil.format(DateUtil.date(), DatePattern.PURE_DATETIME_FORMAT);
        String filename = "商品数据" + now + ".zip";
        String zipFilePath = ZipUtils.getTempPath() + File.separator + filename;
        ExcelAttach.exportZipByExcelAndAttach(zipFilePath);
        //设置文件路径
        File filePath = new File(zipFilePath);
        if (Objects.equals(Boolean.FALSE, filePath.exists())) {
            return;
        }
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(filename.getBytes("utf-8"), "ISO8859-1"));
            byte[] buffer = new byte[4096];
            fis = new FileInputStream(filePath);
            bis = new BufferedInputStream(fis);
            OutputStream os = response.getOutputStream();
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                    // 删除临时文件
                    filePath.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
