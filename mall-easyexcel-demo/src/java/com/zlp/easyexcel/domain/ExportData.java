package com.zlp.easyexcel.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ExportData {


    @ExcelProperty("学号")
    private Integer number;

    @ExcelProperty("姓名")
    private String name;

    @ExcelProperty("图片链接地址")
    private String src;
}
