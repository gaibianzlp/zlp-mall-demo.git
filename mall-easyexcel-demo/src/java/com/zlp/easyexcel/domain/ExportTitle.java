package com.zlp.easyexcel.domain;

import lombok.Data;

@Data
public class ExportTitle {

    private String title;
}
