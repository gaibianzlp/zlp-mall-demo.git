//package com.zlp.easyexcel.excel;
//
//
//import com.spire.xls.FileFormat;
//import com.spire.xls.Workbook;
//import com.spire.xls.Worksheet;
//import com.spire.xls.core.spreadsheet.HTMLOptions;
//
///**
// * excel表格转图片 或html
// * https://blog.csdn.net/qq_33697094/article/details/122736603
// * aspose工具word转pdf文件，在linux服务器上出现内容乱码问题
// * https://www.cnblogs.com/spll/p/16800114.html
// * java 解析word、excel转换为html、pdf
// * https://www.cnblogs.com/vofill/p/14841803.html
// * Java 将 Excel 转为图片、html、XPS、XML、CSV
// * https://www.e-iceblue.cn/spirexlsjavaconversion/convert-excel-to-image-html-xps-xml-csv.html
// */
//public class ExcelToImg {
//
//    public static void main(String[] args) {
//        //加载excel文件
//        // intellij idea 中解决 java.lang.VerifyError: Expecting a stackmap frame at branch target 的方法
//        // https://www.cnblogs.com/x1mercy/p/9222722.html
//        // https://my.oschina.net/u/4437884/blog/4941575
//        // Java程序中使用Spire Jar包报java.lang.NoSuchMethodError类型错误的解决方法
//        // https://blog.csdn.net/Eiceblue/article/details/120154276
//        //TODO 免费版可以商用吗
//        Workbook wb = new Workbook();
//
//        wb.loadFromFile("D:\\export_tmp\\甘特图模板1.xlsx");
//
//        //将excel 转化为 html (如果excel里有多个sheet,会将多个sheet都转化)
//        wb.saveToFile("D:\\export_tmp\\ToHtml.html", FileFormat.HTML);
//
//        //将excel 中的某一个 sheet 转化为 html ,这里get(0)即取第一个sheet转化为html
//        Worksheet sheet = wb.getWorksheets().get(0);
//        HTMLOptions options = new HTMLOptions();
//        options.setImageEmbedded(true);
//        sheet.saveToHtml("D:\\export_tmp\\ToHtmlWithImageEmbeded.html", options);
//
//        //调用方法将Excel工作表保存为图片
//        sheet.saveToImage("D:\\export_tmp\\ToImg.png");
//    }
//
//}
