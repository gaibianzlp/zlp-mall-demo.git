package com.zlp.easyexcel.util;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.zlp.easyexcel.domain.ExportData;
import com.zlp.easyexcel.domain.ExportTitle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

@Slf4j(topic = "ExportUtil")
public class ExportUtil {

    public static void fillExcel(HttpServletResponse response, ExportTitle exportTitle, List<ExportData> list, String fileName, String template) throws IOException {
        log.info("fileName={},template={}",fileName,template);
        ServletOutputStream out = response.getOutputStream();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("UTF-8");
        //文件名字
        response.setHeader("Content-disposition", "attachment;filename=" +fileName);
        //文件模板输入流
        InputStream inputStream = new ClassPathResource("templates/"+template).getInputStream();

        ExcelWriter writer = EasyExcel.write(out).withTemplate(inputStream).build();
        WriteSheet sheet = EasyExcel.writerSheet(0).build();
        //填充列表开启自动换行,自动换行表示每次写入一条list数据是都会重新生成一行空行,此选项默认是关闭的,需要提前设置为true
        FillConfig fillConfig = FillConfig.builder().forceNewRow(true).build();
        //填充标题
        if (Objects.nonNull(exportTitle)) {
            writer.fill(exportTitle,sheet);
        }
        //填充数据
        writer.fill(list,fillConfig,sheet);
        //填充完成
        writer.finish();
        out.flush();

    }
}
