package com.zlp.order.service;

import com.zlp.order.entity.Order;

/**
 * 订单服务
 */
public interface OrderService {

    /**
     * 根据主键查询订单
     *
     * @param id
     * @return
     */
    Order selectOrderById(Integer id);

}
