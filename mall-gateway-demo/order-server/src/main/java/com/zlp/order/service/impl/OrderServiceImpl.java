package com.zlp.order.service.impl;

import com.zlp.order.service.OrderService;
import com.zlp.order.entity.Order;
import com.zlp.order.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * 订单服务
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private  ProductService productService;

    /**
     * 根据主键查询订单
     *
     * @param id
     * @return
     */
    @Override
    public Order selectOrderById(Integer id) {
        return new Order(id, "order-001", "中国", 2666D,
                Arrays.asList(productService.selectProductById(1)));
    }

}