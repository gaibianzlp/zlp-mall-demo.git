package com.zlp.common.route.support;

import com.alibaba.fastjson.JSON;
import com.zlp.common.route.vo.RouteDefinitionVo;
import com.zlp.utils.CacheConstants;
import com.zlp.utils.RedisUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * redis 保存路由信息，优先级比配置文件高
 *
 * @author Zou.LiPing
 * @date: 2021/4/26 9:21
 */
@Component
@AllArgsConstructor
@Slf4j(topic = "RedisRouteDefinitionWriter")
public class RedisRouteDefinitionWriter implements RouteDefinitionRepository, ApplicationEventPublisherAware {

	private final RedisUtils redisUtils;
	private ApplicationEventPublisher publisher;
	private final GatewayProperties gatewayProperties;

	@Override
	public Mono<Void> save(Mono<RouteDefinition> route) {
		return route.flatMap(r -> {
			RouteDefinitionVo vo = new RouteDefinitionVo();
			BeanUtils.copyProperties(r, vo);
			log.info("保存路由信息{}", vo);
			redisUtils.hPut(CacheConstants.ROUTE_KEY,r.getId(), JSON.toJSONString(vo));
			refreshRoutes();
			return Mono.empty();
		});
	}

	@Override
	public Mono<Void> delete(Mono<String> routeId) {
        routeId.subscribe(id -> {
            log.info("删除路由信息={}", id);
            redisUtils.hDelete(CacheConstants.ROUTE_KEY, id);
            refreshRoutes();
        });
        return Mono.empty();
	}

	/**
	 * 刷新路由
	 * @date: 2021/4/26 9:21
	 * @return: void
	 */
	private void refreshRoutes() {
		this.publisher.publishEvent(new RefreshRoutesEvent(this));
	}

	/**
	 * 动态路由入口
	 * @date: 2021/4/26 9:23
	 * @return: reactor.core.publisher.Flux<org.springframework.cloud.gateway.route.RouteDefinition>
	 */
	@Override
	public Flux<RouteDefinition> getRouteDefinitions() {
		List<RouteDefinition> definitionList = new ArrayList<>();
		Map<Object, Object> objectMap = redisUtils.hGetAll(CacheConstants.ROUTE_KEY);
		if (Objects.nonNull(objectMap)) {
			for (Map.Entry<Object, Object> objectObjectEntry : objectMap.entrySet()) {
				RouteDefinition routeDefinition = JSON.parseObject(objectObjectEntry.getValue().toString(),RouteDefinition.class);
				definitionList.add(routeDefinition);
			}
		}
		gatewayProperties.setRoutes(definitionList);
		log.info("redis 中路由定义条数： {}， definitionList={}", definitionList.size(), JSON.toJSONString(definitionList));
		return Flux.fromIterable(definitionList);
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.publisher = applicationEventPublisher;
	}

}
