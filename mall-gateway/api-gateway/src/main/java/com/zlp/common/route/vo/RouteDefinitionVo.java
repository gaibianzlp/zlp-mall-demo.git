package com.zlp.common.route.vo
	;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.cloud.gateway.route.RouteDefinition;

import java.io.Serializable;

/**
 * 扩展此类支持序列化-RouteDefinition
 * @author Zou.LiPing
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RouteDefinitionVo extends RouteDefinition implements Serializable {

	private static final long serialVersionUID = -4046906501288976257L;
	/**
	 * 路由名称
	 */
	private String routeName;
}
