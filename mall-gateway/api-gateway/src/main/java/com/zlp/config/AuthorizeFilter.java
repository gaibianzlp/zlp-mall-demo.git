package com.zlp.config;

import com.alibaba.fastjson.JSON;
import com.zlp.common.api.Result;
import com.zlp.common.api.ResultCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * 认证过滤器
 * @date: 2021/4/22 19:30
 */
//@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        ServerHttpResponse serverHttpResponse = exchange.getResponse();
        // 获取 token 参数
        String token = serverHttpRequest.getHeaders().getFirst("token");

        if (StringUtils.isBlank(token)) {
            serverHttpResponse.setStatusCode(HttpStatus.UNAUTHORIZED);
            return getVoidMono(serverHttpResponse, ResultCode.TOKEN_MISSION);
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {

        return 1;
    }

    /** 
     * 错误信息响应到客户端
     * @param serverHttpResponse Response
     * @param resultCode 错误枚举
     * @date: 2021/4/20 9:13
     * @return: reactor.core.publisher.Mono<java.lang.Void> 
     */
    private Mono<Void> getVoidMono(ServerHttpResponse serverHttpResponse, ResultCode resultCode) {
        serverHttpResponse.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        DataBuffer dataBuffer = serverHttpResponse.bufferFactory().wrap(JSON.toJSONString(Result.failed(resultCode)).getBytes());
        return serverHttpResponse.writeWith(Flux.just(dataBuffer));
    }

}
