package com.zlp.config;

import com.zlp.component.CustomGatewayFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

//@Configuration
public class GatewayRotesConfig {


    @Bean
    protected RouteLocator routeLocator(RouteLocatorBuilder builder) {

        return builder.routes().route(
                // 请求字服务路径以product/**
                r -> r.path("/product/**","/catgory/**")
                .uri("lb://api-product")
                .id("product")
                .filter(new CustomGatewayFilter())
        ).build();

    }
}
