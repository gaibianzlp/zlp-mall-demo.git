package com.zlp.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

/**
 * 路由限流配置
 * @author Zou.LiPing
 */
@Configuration
public class KeyResolverConfig {

    /**
     * IP 限流
     * @date: 2021/4/26 9:17
     * @return: org.springframework.cloud.gateway.filter.ratelimit.KeyResolver
     */
	@Bean(value = "remoteAddrKeyResolver")
	public KeyResolver remoteAddrKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
	}

	/**
	 * URL 限流
	 * @date: 2021/4/26 9:17
	 * @return: org.springframework.cloud.gateway.filter.ratelimit.KeyResolver
	 */
//	@Bean(value = "pathKeyResolver")
	public KeyResolver pathKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getURI().getPath());
	}

	/**
	 * 自定义参数限流
	 * @date: 2021/4/26 9:17
	 * @return: org.springframework.cloud.gateway.filter.ratelimit.KeyResolver
	 */
//	@Bean(value = "paramsKeyResolver")
	public KeyResolver paramsKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getQueryParams().getFirst("userId"));
	}

}
