//package com.zlp.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.zlp.common.route.support.RedisRouteDefinitionWriter;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.gateway.filter.FilterDefinition;
//import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
//import org.springframework.cloud.gateway.route.RouteDefinition;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.util.UriComponentsBuilder;
//import reactor.core.publisher.Mono;
//import java.net.URI;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * route 接口控制
// * @date: 2021/4/20 14:52
// * @author Zou.LiPing
// */
//@Slf4j
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/route")
//public class RouteController {
//
//    private final RedisRouteDefinitionWriter redisRouteDefinitionWriter;
//
//    /**
//     * 添加修改路由节点
//     * @param route RouteDefinition
//     * @date: 2021/4/24 15:35
//     */
//    @PostMapping("save")
//    public void save(RouteDefinition route) {
//        redisRouteDefinitionWriter.save(Mono.just(route));
//    }
//
//    /**
//     * 删除路由节点信息
//     * @param routeId key
//     * @date: 2021/4/24 15:35
//     */
//    @GetMapping("delete")
//    public void delete(String routeId) {
//        Mono<String> just = Mono.just(routeId);
//        redisRouteDefinitionWriter.delete(just);
//    }
//
//    @PostMapping("testSave")
//    public void testSave(RouteDefinition route) {
//        route = bulidRouteDefinitionParam(route);
//        redisRouteDefinitionWriter.save(Mono.just(route));
//    }
//
//
//    private RouteDefinition bulidRouteDefinitionParam(RouteDefinition definition) {
//
//        definition.setId("user");
//        URI uri = UriComponentsBuilder.fromHttpUrl("http://localhost:8200").build().toUri();
//        definition.setUri(uri);
//
//        //定义第一个断言
//        Map<String, String> predicateParams = new HashMap<>(8);
//        PredicateDefinition predicate = new PredicateDefinition();
//        predicate.setName("Path");
//        predicateParams.put("pattern", "/user/**");
//        predicate.setArgs(predicateParams);
//
//        //定义Filter
//        Map<String, String> filterParams = new HashMap<>(8);
//        FilterDefinition filter = new FilterDefinition();
//        filter.setName("RequestRateLimiter");
//        //该_genkey_前缀是固定的，见@link org.springframework.cloud.gateway.support.NameUtils类
//        filterParams.put("redis-rate-limiter.replenishRate", "1");
//        filterParams.put("redis-rate-limiter.burstCapacity", "2");
//        filterParams.put("key-resolver", "#{@remoteAddrKeyResolver}");
//        filter.setArgs(filterParams);
//
//        Map<String, String> filter1Params = new HashMap<>(8);
//        FilterDefinition RewritePath = new FilterDefinition();
//        RewritePath.setName("RewritePath");
//        // /user/(?<segment>.*),/$\{segment}
//        filter1Params.put("regexp", "/user/(?<segment>.*)");
//        filter1Params.put("replacement", "/$\\{segment}");
//        RewritePath.setArgs(filter1Params);
//
//        definition.setFilters(Arrays.asList(filter, RewritePath));
//        definition.setPredicates(Arrays.asList(predicate));
//        log.info("definition:{}",JSON.toJSONString(definition));
//        return definition;
//    }
//
//}
