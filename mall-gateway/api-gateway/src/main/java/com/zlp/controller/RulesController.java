package com.zlp.controller;

import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.GatewayApiDefinitionManager;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * sentinel 接口控制
 * @date: 2021/4/20 14:52
 */
@RestController
public class RulesController {

    /**
     * Api定义
     * @date: 2021/4/22 10:23
     * @return: java.util.Set<com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition>
     */
    @GetMapping("/api")
    @SentinelResource("api")
    public Set<ApiDefinition> apiRules() {
        return GatewayApiDefinitionManager.getApiDefinitions();
    }

    /** 
     * 获取 Route 限流配置信息
     * @date: 2021/4/22 10:22
     * @return: java.util.Set<com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule> 
     */
    @GetMapping("/gateway")
    @SentinelResource("gateway")
    public Set<GatewayFlowRule> apiGateway() {
        return GatewayRuleManager.getRules();
    }

    /**
     * 流规则
     * @date: 2021/4/22 10:24
     * @return: java.util.List<com.alibaba.csp.sentinel.slots.block.flow.FlowRule>
     */
    @GetMapping("/flow")
    @SentinelResource("flow")
    public List<FlowRule> apiFlow() {
        return FlowRuleManager.getRules();
    }

}
