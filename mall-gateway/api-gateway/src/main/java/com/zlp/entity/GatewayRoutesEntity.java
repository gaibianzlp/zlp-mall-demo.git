package com.zlp.entity;
import lombok.Data;



@Data
public class GatewayRoutesEntity {

    /** 
     * 路由的ID
     */
    private String id;

    /**
     * 服务ID
     */
    private String serviceId;

    /**
     * 目标服务地址
     */
    private String uri;

    /**
     * 断言
     */
    private String predicates;

    /**
     * 过滤器
     */
    private String filters;

}
