//package com.zlp.exception;
//
//import com.alibaba.csp.sentinel.slots.block.BlockException;
//import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
//import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
//import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
//import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
//import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
//import com.alibaba.fastjson.JSON;
//import com.zlp.common.api.Result;
//import com.zlp.common.api.ResultCode;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//
//@Slf4j
////@RestControllerAdvice
//public class ExcpetionHandler {
//
//    @ExceptionHandler(ParamFlowException.class)
//    public Result paramFlowException(BlockException e) {
//        log.info("paramFlowException.resp e={}", JSON.toJSONString(e));
//        return Result.failed(ResultCode.AUTHORIZATION_CONTROL);
//    }
//
//    @ExceptionHandler(FlowException.class)
//    public Result flowException(BlockException e) {
//        log.info("flowException.resp e={}", JSON.toJSONString(e));
//        return Result.failed(ResultCode.FLOW_CONTROL);
//    }
//
//    @ExceptionHandler(DegradeException.class)
//    public Result degradeException(BlockException e) {
//        log.info("degradeException.resp e={}", JSON.toJSONString(e));
//        return Result.failed(ResultCode.DOWNGRADE_CONTROL);
//    }
//
//    @ExceptionHandler(AuthorityException.class)
//    public Result authorityException(BlockException e) {
//        log.info("authorityException.resp e={}", JSON.toJSONString(e));
//        return Result.failed(ResultCode.AUTHORIZATION_CONTROL);
//    }
//
//    @ExceptionHandler(SystemBlockException.class)
//    public Result systemblockexception(BlockException e) {
//        log.info("systemblockexception.resp e={}", JSON.toJSONString(e));
//        return Result.failed(ResultCode.SYSTEM_CONTROL);
//    }
//
//}
