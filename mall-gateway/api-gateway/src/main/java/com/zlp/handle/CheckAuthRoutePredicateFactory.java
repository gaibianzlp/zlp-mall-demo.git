package com.zlp.handle;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.function.Predicate;

/**
 * 自定义路由断言工厂 <br/>
 * <p>命名需要以RoutePredicateFactory结尾 比aRoutePredicateFactory 那么yml在使用时a就是断言工厂的名字</p>
 * @date: 2021/4/16 14:07
 */
@Slf4j
@Component
public class CheckAuthRoutePredicateFactory extends AbstractRoutePredicateFactory<CheckAuthRoutePredicateFactory.User> {

    public CheckAuthRoutePredicateFactory() {
        super(User.class);
    }

    /**
     * 自定义配置类
     * @param config
     * @date: 2021/4/16 14:04
     * @return: java.util.function.Predicate<org.springframework.web.server.ServerWebExchange>
     */
    @Override
    public Predicate<ServerWebExchange> apply(User config) {

        return exchange -> {
            log.info("进入apply：name={}" ,config.getName());
            if (config.getName().equals("kitty")) {
                return true;
            }
            return false;
        };
    }

    public static class User{

        @Setter
        @Getter
        private String name;



    }

}