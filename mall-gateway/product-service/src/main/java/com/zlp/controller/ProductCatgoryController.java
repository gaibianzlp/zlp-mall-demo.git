package com.zlp.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/catgory")
@Api(value = "catgory", tags = "商品分类模块")
public class ProductCatgoryController {

    @GetMapping("/getProduct")
    @ApiOperation(value = "获取商品信息")
    public String getProduct(@RequestParam (value = "productId") Long productId) {

        return "getProduct ===>" +productId;
    }


}
