package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.common.api.Result;
import com.zlp.dto.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/product")
@Api(value = "product", tags = "商品模块")
public class ProductController {

    @GetMapping("/getProduct")
    @ApiOperation(value = "获取商品信息")
    public Result<Product> getProduct(HttpServletRequest request, @RequestParam (value = "productId") Long productId) {

        String header = request.getHeader("Authorization");
        log.info("header={}",header);

        Product product = new Product();
        product.setProductId(productId);
        product.setProductName("IOS Pro");
        product.setDescription("5G 速度、iPhone 迄今最快的芯片 A14 仿生OLED 全面屏、将抗跌落能力提升至四倍的超瓷晶面板，" +
                        "以及遍布每个摄像头的夜间模式。这两款尺寸称手的 iPhone 12，该有的都有了。");
        product.setProductStock(100L);
        product.setProductPrice(520000L);
        return Result.success(product);
    }

    @PostMapping("/insertProduct")
    @ApiOperation(value = "新增商品信息")
    public Result<Boolean> insertProduct(@RequestBody Product product) {

        product.setProductId(product.getProductId());
        product.setProductName("IOS Pro");
        product.setDescription("5G 速度、iPhone 迄今最快的芯片 A14 仿生OLED 全面屏、将抗跌落能力提升至四倍的超瓷晶面板，" +
                "以及遍布每个摄像头的夜间模式。这两款尺寸称手的 iPhone 12，该有的都有了。");
        product.setProductStock(100L);
        product.setProductPrice(520000L);
        log.info("insertProduct.product resp={}", JSON.toJSONString(product));
        return Result.success(Boolean.TRUE);
    }


}
