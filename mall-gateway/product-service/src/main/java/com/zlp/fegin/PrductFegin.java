package com.zlp.fegin;

import com.zlp.common.api.Result;
import com.zlp.common.fegin.FeignConfigInterceptor;
import com.zlp.dto.ProductResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@FeignClient(value = "api-product", name = "PrductFegin", configuration = FeignConfigInterceptor.class)
public interface PrductFegin {

    @GetMapping("/api/getProduct")
    Result<ProductResp> getProduct(HttpServletRequest request, @RequestParam("productId") Long productId);


}
