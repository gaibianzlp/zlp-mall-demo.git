package com.zlp.fegin.impl;

import com.zlp.common.api.Result;
import com.zlp.dto.Product;
import com.zlp.dto.ProductResp;
import com.zlp.fegin.PrductFegin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class PrductFeginImpl implements PrductFegin {


    @Override
    @GetMapping("/api/getProduct")
    public Result<ProductResp> getProduct(HttpServletRequest request,Long productId) {

        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("header={}",header);

        ProductResp product = new ProductResp();
        product.setProductId(productId);
        product.setProductName("IOS Pro");
        product.setDescription("5G 速度、iPhone 迄今最快的芯片 A14 仿生OLED 全面屏、将抗跌落能力提升至四倍的超瓷晶面板，" +
                "以及遍布每个摄像头的夜间模式。这两款尺寸称手的 iPhone 12，该有的都有了。");
        product.setProductStock(100L);
        product.setProductPrice(520000L);
        return Result.success(product);
    }

   
}
