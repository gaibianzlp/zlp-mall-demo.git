package com.zlp.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 从配置文件获取URL
 * @date: 2021/4/29 14:48
 */
@Data
@Component
@ConfigurationProperties("remote.call")
public class RemoteCallProperties {

    private String productUrl;


}
