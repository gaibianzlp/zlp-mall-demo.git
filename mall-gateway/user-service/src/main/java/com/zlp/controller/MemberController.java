package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.dto.UserAddReq;
import com.zlp.dto.UserResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("member")
@Api(value = "会员模块", tags = "会员模块")
@Slf4j(topic = "MemberController")
public class MemberController {

    @GetMapping("{userId}")
    @ApiOperation(value = "获取用户根据用户ID")
    public String getUserById(@PathVariable ("userId") Long userId) {
        return "getUser===>" +userId;
    }

    @GetMapping("getUserInfo")
    @ApiOperation(value = "获取用户信息")
    public UserResp getUserInfo(
            @RequestParam("userId") @ApiParam(name = "userId",value = "用户ID") Long userId
    ) {
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }

    @PostMapping("addUser")
    @ApiOperation(value = "添加用户信息")
    public UserResp addUser(UserAddReq userAddReq) {
        log.info("addUser.req userAddReq={}", JSON.toJSONString(userAddReq));
        UserResp userResp = UserResp.builder()
                .id(userAddReq.getId())
                .createTime(new Date())
                .nickname(userAddReq.getNickname())
                .username(userAddReq.getUsername())
                .roleName(userAddReq.getRoleName())
                .build();
        return userResp;
    }

    @GetMapping("getUserHead")
    @ApiOperation(value = "获取用户头部信息")
    public UserResp getUserHead(HttpServletRequest request,
            @RequestParam("userId") @ApiParam(name = "userId",value = "用户ID") Long userId
    ) {
        String header = request.getHeader("X-Request-Foo");
        log.info("getUserHead.resp header={}",header);
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }

    @GetMapping("getUserParam")
    @ApiOperation(value = "获取用户参数信息")
    public UserResp getUserParam(HttpServletRequest request,
                                @RequestParam("userId") @ApiParam(name = "userId",value = "用户ID") Long userId
    ) {
        String foo = request.getParameter("foo");
        log.info("getUserHead.resp foo={}",foo);
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }


}
