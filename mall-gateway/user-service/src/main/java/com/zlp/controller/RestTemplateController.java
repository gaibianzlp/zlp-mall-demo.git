package com.zlp.controller;

import com.zlp.dto.ProductResp;
import com.zlp.service.RemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("remote")
@RequiredArgsConstructor
@Api(value = "远程调用模块", tags = "远程调用模块")
@Slf4j(topic = "RestTemplateController")
public class RestTemplateController {

    private final RemoteService remoteService;

    @GetMapping("/getProduct")
    @ApiOperation(value = "获取商品信息")
    public ProductResp getProduct(@RequestParam (value = "productId") Long productId) {

        return remoteService.getProduct(productId);
    }

    @PostMapping("/insertProduct")
    @ApiOperation(value = "新增商品信息")
    public Boolean insertProduct(@RequestBody ProductResp productResp) throws Exception {

        return remoteService.insertProduct(productResp);
    }

    @GetMapping("/getByProductId")
    @ApiOperation(value = "获取商品信息根据商品ID")
    public ProductResp getByProductId(@RequestParam (value = "productId") Long productId) {

        return remoteService.getByProductId(productId);
    }

    @GetMapping("/getByProductFeign")
    @ApiOperation(value = "获取商品信息根据商品Feign")
    public ProductResp getByProductFeign(@RequestParam (value = "productId") Long productId) {

        return null;
    }

    /**
     *  将字符串以文件的方式上传
     *
     * @param url 上传的接口 url
     * @param content 上传的字符串内容
     * @param fileName 文件的名称
     * @param toPath 存放在服务器上的位置
     * @return  RestTemplate 的请求结果
     * @author daleyzou
     */
    public static ResponseEntity<String> postFileData(String url, String content, String fileName, String toPath) {
        RestTemplate client = new RestTemplate();
        MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
        param.add("name", fileName);
        param.add("filename", fileName);
        param.add("path", toPath);
        // 构建字节流数组
        ByteArrayResource resource = new ByteArrayResource(content.getBytes()) {
            @Override
            public String getFilename() {
                // 文件名
                return fileName;
            }
        };
        param.add("file", resource);
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(param,headers);
        return client.exchange(url, HttpMethod.POST, httpEntity, String.class);
    }

}
