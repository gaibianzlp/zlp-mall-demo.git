package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.dto.LoginUserReq;
import com.zlp.dto.UserAddReq;
import com.zlp.dto.UserResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("user")
@Api(value = "用户模块", tags = "用户模块")
@Slf4j(topic = "UserController")
public class UserController {

    @GetMapping("{userId}")
    @ApiOperation(value = "获取用户根据用户ID")
    public UserResp getUserById(@PathVariable ("userId") Long userId) {
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }

    @GetMapping("getUserInfo")
    @ApiOperation(value = "获取用户信息")
    public UserResp getUserInfo(
            @RequestParam("userId") @ApiParam(name = "userId",value = "用户ID") Long userId
    ) {
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }

    @PostMapping("addUser")
    @ApiOperation(value = "添加用户信息")
    public UserResp addUser(UserAddReq userAddReq) {
        UserResp userResp = UserResp.builder()
                .id(userAddReq.getId())
                .createTime(new Date())
                .nickname(userAddReq.getNickname())
                .username(userAddReq.getUsername())
                .roleName(userAddReq.getRoleName())
                .build();
        return userResp;
    }

    @GetMapping("getUserHead")
    @ApiOperation(value = "获取用户头部信息")
    public UserResp getUserHead(HttpServletRequest request,
            @RequestParam("userId") @ApiParam(name = "userId",value = "用户ID") Long userId
    ) {
        String header = request.getHeader("X-Request-Foo");
        log.info("getUserHead.resp header={}",header);
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }

    @GetMapping("getUserParam")
    @ApiOperation(value = "获取用户参数信息")
    public UserResp getUserParam(HttpServletRequest request,
                                @RequestParam("userId") @ApiParam(name = "userId",value = "用户ID") Long userId
    ) {
        String foo = request.getParameter("foo");
        log.info("getUserHead.resp foo={}",foo);
        UserResp userResp = UserResp.builder()
                .id(userId)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }


    @GetMapping("login")
    @ApiOperation(value = "登入-Get")
    public UserResp loginMethodGet(
            @RequestParam("username") @ApiParam(name = "username",value = "用户名") String username,
            @RequestParam("password") @ApiParam(name = "password",value = "密码")String password
    ) {

        log.info("login.resp username={},password={}",username,password);
        UserResp userResp = UserResp.builder()
                .id(1L)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }

    @PostMapping("loginMethodPost")
    @ApiOperation(value = "登入-Post")
    public UserResp loginMethodPost(@RequestBody LoginUserReq loginUserReq) {

        log.info("login.resp loginUserReq={}", JSON.toJSONString(loginUserReq));
        UserResp userResp = UserResp.builder()
                .id(1L)
                .createTime(new Date())
                .nickname("change")
                .username("Zou.LiPing")
                .roleName("超级管理员")
                .build();
        return userResp;
    }




}
