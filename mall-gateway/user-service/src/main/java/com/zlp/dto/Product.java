package com.zlp.dto;

import lombok.Data;

@Data
public class Product {

    private Long productId;
    private String productName;
    private String description;
    private Long productPrice;
    private Long productStock;

}
