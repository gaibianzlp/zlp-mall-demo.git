package com.zlp.service;

import com.zlp.dto.ProductResp;

public interface RemoteService {

    /**
     * 获取商品信息
     * @param productId
     * @date: 2021/4/27 16:25
     * @return: com.zlp.dto.ProductResp
     */
    ProductResp getProduct(Long productId);

    /**
     * 新增商品信息
     * @param productResp
     * @date: 2021/4/27 17:49
     * @return: com.zlp.dto.ProductResp
     */
    Boolean insertProduct(ProductResp productResp) throws Exception;

    /**
     * 获取商品信息根据商品ID
     * @param productId
     * @date: 2021/4/28 11:55
     * @return: com.zlp.dto.ProductResp
     */
    ProductResp getByProductId(Long productId);

    ProductResp getByProductFeign(Long productId);
}
