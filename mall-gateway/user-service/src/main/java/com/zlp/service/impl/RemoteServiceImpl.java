package com.zlp.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zlp.common.api.Result;
import com.zlp.config.RemoteCallProperties;
import com.zlp.dto.Product;
import com.zlp.dto.ProductResp;
import com.zlp.service.RemoteService;
import com.zlp.utils.ProductApiUrlConstants;
import com.zlp.utils.RestTemplateUtils;
import com.zlp.utils.UrlUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j(topic = "RemoteServiceImpl")
public class RemoteServiceImpl implements RemoteService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RemoteCallProperties remoteCallProperties;



    @SneakyThrows
    @Override
    public ProductResp getProduct(Long productId) {

        log.info("getProduct.req productId={}", productId);
        ProductResp productResp = new ProductResp();
        Map<String, Object> mapPram = new HashMap<>(1);
        mapPram.put("productId", productId);
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Authorization", UUID.randomUUID().toString());
        HttpEntity<String> requestEntity = new HttpEntity<>(null, requestHeaders);
        // 请求调用
        String urlAppendParm = UrlUtils.getUrlAppendParm(remoteCallProperties.getProductUrl() + ProductApiUrlConstants.GET_PRODUCT, mapPram);
        log.info("getProduct  resp={}", urlAppendParm);
        ResponseEntity<String> response = restTemplate.exchange(urlAppendParm, HttpMethod.GET, requestEntity, String.class);
        HttpStatus statusCode = response.getStatusCode();
        if (HttpStatus.OK.equals(statusCode)) {
            String body = response.getBody();
            if (StringUtils.isNotBlank(body)) {
                String data = JSON.parseObject(body).getString("data");
                productResp = JSON.parseObject(data, ProductResp.class);
                log.info("getProduct.urlAppendParm={}  resp={}", ProductApiUrlConstants.GET_PRODUCT, JSON.toJSONString(productResp));
            }
        } else {
            throw new Exception(String.format("远程调用 [%s] 接口失败！", urlAppendParm));
        }
        return productResp;
    }

    @Override
    public Boolean insertProduct(ProductResp productResp) throws Exception {

        log.info("insertProduct.req productResp={}", JSON.toJSONString(productResp));
        HttpEntity<String> httpEntity = RestTemplateUtils.buildPostForEntity(JSON.toJSONString(productResp));
        String insertPorductUrl = "";
        try {
            insertPorductUrl = remoteCallProperties.getProductUrl() + ProductApiUrlConstants.INSERT_PRODUCT;
            ResponseEntity<String> response = restTemplate.postForEntity(insertPorductUrl, httpEntity, String.class);
            HttpStatus statusCode = response.getStatusCode();
            if (HttpStatus.OK.equals(statusCode)) {
                log.info("insertProduct.resp={}", response.getBody());
            }
        } catch (Exception e) {
            throw new Exception(String.format("远程调用 [%s] 接口失败！", insertPorductUrl));
        }
        return Boolean.TRUE;
    }

    @Override
    public ProductResp getByProductId(Long productId) {

        log.info("getProduct.req productId={}", productId);
        ProductResp productResp = new ProductResp();
        Map<String, Object> mapPram = new HashMap<>(1);
        mapPram.put("productId", productId);
        String urlAppendParm = UrlUtils.getUrlAppendParm(remoteCallProperties.getProductUrl() + ProductApiUrlConstants.GET_PRODUCT, mapPram);
        ResponseEntity<Result> response = restTemplate.getForEntity(urlAppendParm, Result.class, productId);
        if (HttpStatus.OK.equals(response.getStatusCode())) {
            Result<Product> result = response.getBody();
            // java.util.LinkedHashMap cannot be cast to com.zlp.dto.Product
            Product product = dataConverObj(result);
            BeanUtils.copyProperties(product,productResp);
        }
        return productResp;
    }

    @Override
    public ProductResp getByProductFeign(Long productId) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        return null;
    }

    private Product dataConverObj(Result<Product> result) {
        return new Gson().fromJson(new Gson().toJson(result.getData()), new TypeToken<Product>(){}.getType());
    }

}
