package com.zlp.utils;

/**
 * 商品服务URL常量
 */
public interface ProductApiUrlConstants {

	/**
	 * 获取商品信息
	 */
	String GET_PRODUCT = "/product/getProduct";

    String INSERT_PRODUCT = "/product/insertProduct";
}
