package com.zlp.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

/** 
 *  RestTemplate 请求参数封装
 * @date: 2020/6/27 11:20
 */
public class RestTemplateUtils {

    /** 
     * 构建post请求参数
     * @param jsonData
     * @date: 2020/6/27 11:21
     * @return: org.springframework.http.HttpEntity<java.lang.String> 
     */
    public static HttpEntity<String> buildPostForEntity(String jsonData){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        HttpEntity<String> httpEntity = new HttpEntity(jsonData,httpHeaders);
        return httpEntity;
    }

    /**
     * 构建请求参数
     * @param args
     * @date: 2020/6/27 11:21
     * @return: org.springframework.http.HttpEntity<java.lang.String>
     */
    public static Map<String,Object> buildReqParam(Object...args){
        Map<String,Object> mapPram = new HashMap<>(args.length);
        for (Object obj : args) {
            mapPram.put("productId",obj);
        }
        return mapPram;
    }


}
