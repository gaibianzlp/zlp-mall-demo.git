package com.zlp.Test;
import lombok.SneakyThrows;

import java.io.InputStream;
import java.util.Objects;

public class ClassLoaderTest {


    public static void main(String[] args) throws Exception {
        ClassLoader myLoader = new ClassLoader() {
            @SneakyThrows
            public Class<?> loadClass(String name) {
                byte[] b = null;
                InputStream is = null;
                try {
                    String fileName = name.substring(name.lastIndexOf(".") + 1)
                            + ".class";
                    System.out.println(fileName);
                    is = getClass().getResourceAsStream(fileName);
                    if (is == null) {
                        return super.loadClass(name);
                    }
                    is.read(b);
                    b = new byte[is.available()];
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }finally {
                    if (Objects.nonNull(is))
                        is.close();
                }
                assert b != null;
                return defineClass(name, b, 0, b.length);
            }
        };
        Object obj = myLoader.loadClass("classloadertest.A").newInstance();
        System.out.println(obj.getClass());
//        System.out.println(obj instanceof classloadertest.A);
    }
}
