package com.zlp.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Slf4j(topic = "RGBTest")
public class RGBTest {


   final static int counter = 68;


    public static void main(String[] args) {
//        int r = 255;
//        int g = 0;
//        int b = 0;

//        System.out.println(convertRGBToHex(r, g, b));
//        System.out.println(nearInfraredData(60, 10, 20));
//        for (int i = 0; i < 100; i++) {
//
//            Random random = new Random();//这是随机数发生器
////            int x=random.nextInt(90)+10;//生成一个[10,100)范围内的随机数
//            int x = random.nextInt(55) + 200;//生成一个[10,100)范围内的随机数
//            System.out.println(x);
//        }

//        16 320
        int start = 0;
        int end = 200;
        int times = 5; //  遍历次数
        // 峰值点
        List<Integer> peaks = Arrays.asList(235,236,237,238,239,240,481,482,483,484,485,720,721,722,
                723,724,725,955,956,957,958,959,1195,1196,1197,1198,1199,1120);
        List<String> rgbLists = new ArrayList<>();
        for (int i = 0; i < 4*60*5; i++) {
            String rgbs;
            if (peaks.contains(i)) {
                rgbs = nearInfraredTest01(200, 50, times);
                rgbLists.add(rgbs);
            }else{
                rgbs = nearInfraredTest01(start, end, times);
                rgbLists.add(rgbs);
            }
        }
        String value = JSON.toJSONString(rgbLists);
        String filePath = "D:\\file\\nearInfrared7.txt";
        writeFile(value, filePath);
    }

    /**
     * 将rgb色彩值转成16进制代码
     *
     * @param r
     * @param g
     * @param b
     * @date: 2021/1/12 11:54
     * @return: java.lang.String
     */
    public static String convertRGBToHex(int r, int g, int b) {

        String rFString, rSString, gFString, gSString,
                bFString, bSString, result;
        int red, green, blue;
        int rred, rgreen, rblue;
        red = r / 16;
        rred = r % 16;
        if (red == 10) rFString = "A";
        else if (red == 11) rFString = "B";
        else if (red == 12) rFString = "C";
        else if (red == 13) rFString = "D";
        else if (red == 14) rFString = "E";
        else if (red == 15) rFString = "F";
        else rFString = String.valueOf(red);

        if (rred == 10) rSString = "A";
        else if (rred == 11) rSString = "B";
        else if (rred == 12) rSString = "C";
        else if (rred == 13) rSString = "D";
        else if (rred == 14) rSString = "E";
        else if (rred == 15) rSString = "F";
        else rSString = String.valueOf(rred);

        rFString = rFString + rSString;

        green = g / 16;
        rgreen = g % 16;

        if (green == 10) gFString = "A";
        else if (green == 11) gFString = "B";
        else if (green == 12) gFString = "C";
        else if (green == 13) gFString = "D";
        else if (green == 14) gFString = "E";
        else if (green == 15) gFString = "F";
        else gFString = String.valueOf(green);

        if (rgreen == 10) gSString = "A";
        else if (rgreen == 11) gSString = "B";
        else if (rgreen == 12) gSString = "C";
        else if (rgreen == 13) gSString = "D";
        else if (rgreen == 14) gSString = "E";
        else if (rgreen == 15) gSString = "F";
        else gSString = String.valueOf(rgreen);

        gFString = gFString + gSString;

        blue = b / 16;
        rblue = b % 16;

        if (blue == 10) bFString = "A";
        else if (blue == 11) bFString = "B";
        else if (blue == 12) bFString = "C";
        else if (blue == 13) bFString = "D";
        else if (blue == 14) bFString = "E";
        else if (blue == 15) bFString = "F";
        else bFString = String.valueOf(blue);

        if (rblue == 10) bSString = "A";
        else if (rblue == 11) bSString = "B";
        else if (rblue == 12) bSString = "C";
        else if (rblue == 13) bSString = "D";
        else if (rblue == 14) bSString = "E";
        else if (rblue == 15) bSString = "F";
        else bSString = String.valueOf(rblue);
        bFString = bFString + bSString;
        result = "#" + rFString + gFString + bFString;
        return result;

    }


    /**
     * 模拟近红外
     */
    private static String nearInfraredTest01( int start, int end, int times) {

        JSONArray array = new JSONArray();
        for (int i = 0; i < counter ; i++) {
            Random random = new Random();//这是随机数发生器
            int r = random.nextInt(end) + start;
            int g = random.nextInt(end) + start;
            int b = random.nextInt(end) + start;
            int bb = 0;
            if (b < 150) {
                int b1 = random.nextInt(100) + start;
                bb = b1 + b;
            }
            String rgb = convertRGBToHex(r, g, bb);
            array.add(rgb);
        }
        return array.toJSONString();
    }

    /**
     * 模拟近红外
     */
    private static String nearInfraredData(int count, int start, int end) {

        try {
            Random random = new Random();
            JSONArray array = new JSONArray();
            for (int i = 0; i < count; i++) {
                array.add(BigDecimal.valueOf((start - Math.random() * end)).setScale(0, BigDecimal.ROUND_HALF_UP));
//                array.add(BigDecimal.valueOf((10 - Math.random() * 20)).setScale(0,BigDecimal.ROUND_HALF_UP));
//                array.add(String.valueOf(r.nextDouble()*0.2-0.1).substring(0, 5));
            }
            return array.toJSONString();
        } catch (Exception e) {
            log.error("nearInfraredData 随机数异常!", e);
        }
        return null;
    }

    /**
     * 写入文件
     *
     * @param json     文件内容
     * @param filePath 文件路径
     * @date: 2020/12/21 16:45
     * @return: void
     */
    @SneakyThrows
    private static void writeFile(String json, String filePath) {

        BufferedWriter bufferWritter = null;
        try {
            File file = new File(filePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            } else {
                file.delete();
                file.createNewFile();
            }
            // true = append file
            FileWriter fileWritter = new FileWriter(file, false);
            bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(json);
            log.info("writeFile 写入文件成功！");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(bufferWritter))
                bufferWritter.close();
        }
    }

}
