package com.zlp.algorithm;

public class BinarySearch {

    public static void main(String[] args) {

//        biSearch(new int[]{4,5,6,7,8,10,3},3);
        System.out.println(biSearch(new int[]{4, 5, 6, 7, 8, 10, 11,12}, 8));

    }

    public static int biSearch(int[] array, int a) {
        int lo = 0;// 低位
        int hi = array.length - 1; // 高位
        int mid;
        while (lo <= hi) {
            mid = (lo + hi) / 2;//中间位置
            if (array[mid] == a) {
                return mid + 1;
            } else if (array[mid] < a) { //向右查找
                lo = mid + 1;
            } else { //向左查找
                hi = mid - 1;
            }
        }
        return -1;
    }
}
