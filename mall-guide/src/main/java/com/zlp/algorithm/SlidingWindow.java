package com.zlp.algorithm;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class SlidingWindow {


    public static void main(String[] args) {

        int[] nums = {4,1, 3, -1, -3, 5, 3, 6, 7};
        int k = 3;

        int indx = 15&3232323;
        System.out.println(indx);
//        System.out.println(slidingWindowTest(nums, k).toString());
        System.out.println("========");
        Arrays.stream(slidingWindowTest(nums, k)).forEach(index -> System.out.println(index));
    }

    private static int[] slidingWindowTest(int[] nums, int k) {

        if (k == 0 || nums.length == 0) {
            return new int[]{};
        }

        // 队列表示滑动窗口
        Deque<Integer> queue = new LinkedList<>();

        // 结果
        int resIndex = 0;
        int[] res = new int[nums.length - k + 1];

        for (int i = 0; i < nums.length; i++) {

            // 头尾尾头口诀
            // 1.头：清理超期元素（清理i-k位置元素）
            System.out.println(String.format("下标i=%d,queue.peek=%d,(i - k)=%d",i,queue.peek(),i - k));
            if (!queue.isEmpty() && queue.peek() == i - k) {
                System.out.println("fds");
                queue.remove();
            }

            // 2. 尾：维护单调递减队列（清除队列内<新入队的元素）
            // 删除所有比新入队小的就元素
            while (!queue.isEmpty() && nums[i] > nums[queue.peekLast()]) {
                queue.removeLast();
            }
            //3. 尾:加入新的队列
            queue.add(i);

            // 4. 头：获取滑动窗口最大值 （返回头部元素，i>=k-1时）
            if (i >= k - 1) {
                res[resIndex++] = nums[queue.peek()];
            }

        }
        return res;
    }
}
