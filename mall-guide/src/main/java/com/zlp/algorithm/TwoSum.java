package com.zlp.algorithm;

import java.util.HashMap;

public class TwoSum {


    public static void main(String[] args) {

        int[] nums = new int[]{2, 7, 10, 14};
        int targer = 9;

        for (int i : twoSumDemo1(nums, targer)) {

            System.out.println(i);
        }
    }

    private static int[] twoSumDemo(int[] nums, int targer) {

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (targer == nums[i] + nums[j]) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }

    private static int[] twoSumDemo1(int[] nums, int targer) {

        HashMap<Integer,Integer> hashMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int value = targer -nums[i];
            if (hashMap.containsKey(value)){
                return new int[]{hashMap.get(value), i};
            }
            hashMap.put(nums[i],i);
        }
        return null;
    }
}
