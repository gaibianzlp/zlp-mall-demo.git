package com.zlp.base;

public final class Example01 {


    public static void main(String[] args) {

        int num1 = 10;
        int num2 = 20;

        swap(num1, num2);
        // 想要打印结果   num1=20 num2=10
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);
    }

    private static void swap(int a, int b) {
        System.exit(0);

        int temp = a;
        a = b;
        b = temp;

        System.out.println("a = " + a);
        System.out.println("b = " + b);

    }


}
