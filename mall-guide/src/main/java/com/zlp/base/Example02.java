package com.zlp.base;

public class Example02 {

    public static void main(String[] args) {
        System.out.println(f(1));
    }

    public static int f(int value) {
        try {
            return value * value;
        } finally {
            if (value == 2) {
                return 0;
            }else {
                return 2;
            }
        }
    }
}
