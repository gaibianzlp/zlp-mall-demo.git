package com.zlp.concurrent;

import org.openjdk.jol.info.ClassLayout;

public class SynchDemo implements Runnable{



    static DemoLock demoLock = new DemoLock();

    @Override
    public void run() {
        synchronized (demoLock){

        }

    }

    public static void main(String[] args) throws InterruptedException {

        Thread.sleep(4000L);
        System.out.println(demoLock.hashCode());
        System.out.println(Integer.toHexString(demoLock.hashCode()));


        // 对象在内存中的布局
        System.out.println(ClassLayout.parseInstance(demoLock).toPrintable());

    }
}
