package com.zlp.concurrent.lock;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicPushOrder {

    private static AtomicInteger count = new AtomicInteger(50000);

    public void reduct(){
        count.decrementAndGet();
    }

    public static void main(String[] args) throws InterruptedException {

        AtomicPushOrder pushOrder = new AtomicPushOrder();
        for (int i = 0; i < 5 ; i++) {
            new Thread(()->{
                for (int j = 0; j < 10000 ; j++) {
                    pushOrder.reduct();
                }
            }).start();
        }

        Thread.sleep(3000L);
        System.out.println(String.format("count数量=%d",count.get()));
    }
}
