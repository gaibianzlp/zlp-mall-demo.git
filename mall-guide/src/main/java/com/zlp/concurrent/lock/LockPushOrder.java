package com.zlp.concurrent.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockPushOrder {


    private static int count = 50000;
    Lock lock = new ReentrantLock();
    public void reduct() {
        try {
            count--;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        LockPushOrder lockPushOrder = new LockPushOrder();
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    lockPushOrder.reduct();
                }
            }).start();
        }

        Thread.sleep(3000L);
        System.out.println(String.format("count数量=%d", count));
    }
}
