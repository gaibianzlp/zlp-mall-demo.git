package com.zlp.concurrent.lock;

import java.util.concurrent.atomic.AtomicInteger;

public class PushOrder {


    private static int count = 50000;

    public void reduct(){
        count--;
    }

    public static void main(String[] args) throws InterruptedException {

        PushOrder pushOrder = new PushOrder();
        for (int i = 0; i < 5 ; i++) {
            new Thread(()->{
                for (int j = 0; j < 10000 ; j++) {
                    pushOrder.reduct();
                }
            }).start();
        }

        Thread.sleep(3000L);
        System.out.println(String.format("count数量=%d",count));
    }
}
