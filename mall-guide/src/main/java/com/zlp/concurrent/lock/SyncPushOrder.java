package com.zlp.concurrent.lock;

public class SyncPushOrder {


    private static int count = 50000;
    Object object = new Object();
    public void reduct(){
        synchronized (object){
            count--;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        SyncPushOrder pushOrder = new SyncPushOrder();
        for (int i = 0; i < 5 ; i++) {
            new Thread(()->{
                for (int j = 0; j < 10000 ; j++) {
                    pushOrder.reduct();
                }
            }).start();
        }

        Thread.sleep(3000L);
        System.out.println(String.format("count数量=%d",count));
    }
}
