package com.zlp.concurrent.sync;


public class SynchronizedTest01 {
 

    static Object objLock = new Object();
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(1000L);
        new Thread(()->{
            synchronized (objLock){
                try {
                    objLock.wait();
                    System.out.println(Thread.currentThread().getName() +"\t"+"阻塞-------");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() +"\t"+"我又活了-------");

        },"A").start();
        Thread.sleep(1000L);
        new Thread(()->{
            synchronized (objLock){
                try {
                    objLock.notify();
                    System.out.println(Thread.currentThread().getName() +"\t"+"唤醒-------");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },"B").start();
    }
}