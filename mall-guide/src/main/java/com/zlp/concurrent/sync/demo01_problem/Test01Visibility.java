package com.zlp.concurrent.sync.demo01_problem;

public class Test01Visibility {

    // 多个线程访问，看共享变量会发生变化吗
    private static Boolean flag = Boolean.TRUE;

    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            while (flag){
                System.out.println(flag);
            }

        }).start();


        Thread.sleep(1000);
        new Thread(()->{
            flag = Boolean.FALSE;
            System.out.println("修改共享变量flag为false");
        }).start();
    }



}
