package com.zlp.hash;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j(topic = "HashMapTest01")
public class HashMapTest01 {

    public static void main(String[] args) {

        HashMap<String, Object> hashMaps = new HashMap<>();
        hashMaps.put("k1","李健");
        hashMaps.put("k2","周杰伦");
        hashMaps.put("k3","科比");

        // high 高、low低 Segment

        hashMaps.remove("k3");

        System.out.println(hashMaps.get("key1"));

        System.out.println(Integer.highestOneBit(17));

        String key1 = "hash2";
        String key2 = "hash3";
        int i = System.identityHashCode(key1);
        int i2 = System.identityHashCode(key2);
        System.out.println(i);
        System.out.println(i2);



    }

}
