package com.zlp.hash;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j(topic = "HashMapTest01")
public class HashMapTest02 {

    public static void main(String[] args) {

        HashMap<Student, String> hashMaps = new HashMap<>();

        hashMaps.put(new Student("ZLP",23),"18271476280");
        hashMaps.put(new Student("ZLP",45),"18271476280");
        hashMaps.put(new Student("ZLP01",45),"18271476280");

        System.out.println(hashMaps.size());

        for (Map.Entry<Student, String> studentEntry : hashMaps.entrySet()) {

            System.out.println("学生对象："+studentEntry.getKey()+"手机号码："+studentEntry.getValue());

        }




    }

}
