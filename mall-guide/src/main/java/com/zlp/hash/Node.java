package com.zlp.hash;

public class Node {

    private Object obj;

    private Node next;

    public Node(Object obj,Node next){
        this.obj = obj;
        this.next = next;
    }

    public static void main(String[] args) {

        // 第一个节点
        Node head = new Node(new Object(), null);

        // 第二个节点
        Node newNode1 = new Node(new Object(), null);
        head.next = newNode1;

        while (head.next != null) {
            // 把第三个节点赋值给第一个
            Node newNode2 = new Node(new Object(), null);
            head.next = newNode2;
            head = head.next;
        }

        System.out.println(head);

    }
}
