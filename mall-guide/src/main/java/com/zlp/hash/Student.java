package com.zlp.hash;

public class Student extends Object{

    private String name;
    private Integer age;


    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj){
            return true;
        }
        Student o = (Student) obj;
        if (this.age == o.getAge() && this.name.equals(o.name)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {

        return name.hashCode()+age.hashCode();
    }
}
