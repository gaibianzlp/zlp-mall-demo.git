package com.zlp.jvm;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * 内存溢出 -Xmx40m -Xms40m
 * 命令监控：jvisualvm
 * @date: 2022/4/7 10:16
 * @return: 
 */
public class OutOfMemory {

    String [] container = new String[1204*1000*30]; // 1M

    public static void main(String[] args) throws InterruptedException {

        List<OutOfMemory>  conObjects = new ArrayList<>();
        int i = 0;
        while (true){
            conObjects.add(new OutOfMemory());
            Thread.sleep(10);
            System.out.println(i++);
        }
    }
}
