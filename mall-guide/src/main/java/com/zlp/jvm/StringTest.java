package com.zlp.jvm;


/**
 * == 判断
 * @date: 2022/4/7 10:08
 * @return:
 */
public class StringTest {

    public static void main(String[] args) {

        String a = "abc";
        String b = "abc";
        String c = new String("abc");
        String d = new String("efg");
        System.out.println(a==b);
        System.out.println(a==c);
        System.out.println(a==c.intern());

    }
}
