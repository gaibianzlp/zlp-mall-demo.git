package com.zlp.list;

import java.util.ArrayList;

public class ArrayListTest {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        list.add("TEST1");
        list.add("TEST2");
        list.add("TEST3");

        // 获取
        System.out.println(list.get(2));

        // 修改
        list.add(2,"TEST3");

        System.out.println(list.get(2));

    }
}
