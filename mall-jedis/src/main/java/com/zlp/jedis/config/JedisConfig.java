package com.zlp.jedis.config;

import redis.clients.jedis.Jedis;

import java.util.Objects;

public class JedisConfig {



    private JedisConfig(){}

    protected static Jedis jedis = null;
    private static final String HOST = "47.103.20.21";
    private static final Integer PORT = 6379;
    private static final String PASSWORD = "zlp123456";


    /** 
     * 连接Jedis
     * @date: 2022/3/26 18:14
     */
    public static Jedis jedis(){
        if (Objects.isNull(jedis)) {
            jedis = new Jedis(HOST, PORT);
            jedis.auth(PASSWORD);
        }
        return jedis;
    }

    /**
     * 关闭连接
     * @date: 2022/3/26 18:14
     */
    public static void close(){
        if (Objects.isNull(jedis)) {
            jedis.close();
        }
    }



}
