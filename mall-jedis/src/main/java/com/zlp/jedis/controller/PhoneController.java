package com.zlp.jedis.controller;

import com.zlp.jedis.config.JedisConfig;
import redis.clients.jedis.Jedis;

import java.util.Objects;
import java.util.Random;

public class PhoneController {

    public static void main(String[] args) {

        String phone = "13525667891";
        String code = getPhone(phone);
        veriyCode(code,phone);
    }

    private static String getPhone(String phone){
        String code = null;
        // 手机验证码key
        Jedis jedis = JedisConfig.jedis();
        String phoneKey = "veriy:"+phone+":code";
        // 校验次数key
        String checkKey = "veriy:"+phone+":check";
        String checkValue = jedis.get(checkKey);
        if (Objects.isNull(checkValue)){
            jedis.set(checkKey,"1","ex", 60*60*24);
        }else if (Integer.valueOf(checkValue) <= 2) {
            jedis.incr(checkKey);
            jedis.close();
            return code;
        }else {
            System.out.println("您手机号每天只能发送三次验证码");
            jedis.close();
            return code;
        }
        code = getCode();
        jedis.set(phoneKey,code,"ex", 60*2);
        jedis.close();
        return code;

    }

    private static String getCode() {
        String code = "";
        for (int i = 0; i < 6; i++) {
            int rand = new Random().nextInt(10);
            code = code + rand;
        }
        System.out.println("验证码：code="+code);
        return code;
    }

    private static void veriyCode(String code, String phone) {

        if (Objects.isNull(code) || Objects.isNull(phone)) {
            System.out.println("参数校验有误");
            return;
        }
        Jedis jedis = JedisConfig.jedis();
        String phoneKey = "veriy:"+phone+"code";
        String phoneValue = jedis.get(phoneKey);
        if (!Objects.equals(code,phoneValue)){
            System.out.println("手机验证码有误！");
        }else{
            System.out.println("手机验证码成功！");
        }
    }
}
