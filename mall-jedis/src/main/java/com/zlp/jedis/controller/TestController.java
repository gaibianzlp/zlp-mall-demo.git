package com.zlp.jedis.controller;

import com.zlp.jedis.config.JedisConfig;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestController {

    public static void main(String[] args) {

//        setCommand();
//        msetCommand();
//        listCommand();
//        setListCommand();
//        hashCommand();
        zsetCommand();
    }

    /** 
     * set 命令
     * @date: 2022/3/26 18:19
     */
    public static void setCommand(){
        Jedis jedis = JedisConfig.jedis();
        jedis.set("k1", "v1");
        jedis.set("k2", "v2");
        jedis.set("k3", "v3");
        // 获取所有的key
        Set<String> keys = jedis.keys("*");
        System.out.println(keys.size());
        for (String key : keys) {
            System.out.println(key);
        }

        System.out.println(jedis.exists("k1"));
        System.out.println(jedis.ttl("k1"));
        System.out.println(jedis.get("k1"));
        JedisConfig.close();
    }


    /**
     * mset 命令 (批量设置)
     * @date: 2022/3/26 18:19
     */
    public static void msetCommand(){
        Jedis jedis = JedisConfig.jedis();
        jedis.mset("str1","v1","str2","v2","str3","v3");
        System.out.println(jedis.mget("str1","str2","str3"));
        JedisConfig.close();
    }

    /**
     * List 命令
     * @date: 2022/3/26 18:19
     */
    public static void listCommand(){
        Jedis jedis = JedisConfig.jedis();
        jedis.lpush("mylist","my1","my2");
        List<String> list = jedis.lrange("mylist",0,-1);
        for (String element : list) {
            System.out.println(element);
        }
        JedisConfig.close();
    }

    /**
     * setList 命令
     * @date: 2022/3/26 18:19
     */
    public static void setListCommand(){
        Jedis jedis = JedisConfig.jedis();
        jedis.sadd("orders", "order01");
        jedis.sadd("orders", "order02");
        jedis.sadd("orders", "order03");
        jedis.sadd("orders", "order04");
        Set<String> smembers = jedis.smembers("orders");
        for (String order : smembers) {
            System.out.println(order);
        }
        // 删除
        jedis.srem("orders", "order02");
        JedisConfig.close();
    }

    /**
     * hash 命令
     * @date: 2022/3/26 18:19
     */
    public static void hashCommand(){
        Jedis jedis = JedisConfig.jedis();
        jedis.hset("hash1","userName","lisi");
        System.out.println(jedis.hget("hash1","userName"));
        Map<String,String> map = new HashMap<String,String>();
        map.put("telphone","13810169999");
        map.put("address","atguigu");
        map.put("email","abc@163.com");
        jedis.hmset("hash2",map);
        List<String> result = jedis.hmget("hash2", "telphone","email");
        for (String element : result) {
            System.out.println(element);
        }
        JedisConfig.close();
    }


    /**
     * zset 命令
     * @date: 2022/3/26 18:19
     */
    public static void zsetCommand(){
        Jedis jedis = JedisConfig.jedis();
        jedis.zadd("zset01", 100d, "z3");
        jedis.zadd("zset01", 90d, "l4");
        jedis.zadd("zset01", 80d, "w5");
        jedis.zadd("zset01", 70d, "z6");
        Set<String> zrange = jedis.zrange("zset01", 0, -1);
        for (String e : zrange) {
            System.out.println(e);
        }
        JedisConfig.close();
    }



}
