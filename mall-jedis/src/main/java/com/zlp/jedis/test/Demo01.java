package com.zlp.jedis.test;

import redis.clients.jedis.Jedis;

public class Demo01 {


    public static void main(String[] args) {
        Jedis jedis = new Jedis("47.103.20.21", 6379);
        jedis.auth("zlp123456");
        String pong = jedis.ping();
        System.out.println("连接成功：" + pong);
        jedis.close();
    }
}