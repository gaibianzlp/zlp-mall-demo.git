package com.zlp.queue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueApp {

    public static void main(String[] args) {
        SpringApplication.run(QueueApp.class,args);
    }
}
