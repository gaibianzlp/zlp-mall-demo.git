package com.zlp.queue.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;

public class TestController {


    private ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return 10;//每个线程中局部变量的初始值为10
        }
    };

    @ApiOperation(value = "登陆之后返回token")
    @GetMapping("/thread/test")
    public void testThread(){
        try {
            threadLocal.set(threadLocal.get()+10);
            System.out.println(threadLocal.get());
        }finally {
            threadLocal.remove();
        }

    }
}
