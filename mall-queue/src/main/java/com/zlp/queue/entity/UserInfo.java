package com.zlp.queue.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserInfo {

    /**
     * 请求和响应的唯一标识
     */
    private String serialNo;

    private User user;

    private OrderInfo orderInfo;

    @Data
    public static class User{
        private Long userId;
        private String username;
        private String address;
    }

    @Data
    public static class OrderInfo{
        private Long orderId;
        private String projectName;
        private String orderTime;
    }




}
