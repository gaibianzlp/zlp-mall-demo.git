package com.zlp.queue.service;

import com.zlp.queue.entity.UserInfo;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class RemoteService {


    
    /** 
     * 获取用户信息
     * @param userId
     * @date: 2022/3/2 13:41
     * @return: com.zlp.demo.v1.entity.UserInfo.User 
     */
    @SneakyThrows
    public UserInfo.User getUser(Long userId){
        UserInfo.User user = new UserInfo.User();
        user.setUserId(userId);
        user.setUsername("zouLiPing");
        user.setAddress("江西上上饶市");
        Thread.sleep(150);
        return user;
    }


    /**
     * 获取订单信息
     * @param userId
     * @date: 2022/3/2 13:42
     * @return: com.zlp.demo.v1.entity.UserInfo.OrderInfo
     */
    @SneakyThrows
    public UserInfo.OrderInfo getOrder(Long userId){
        UserInfo.OrderInfo orderInfo = new UserInfo.OrderInfo();
        orderInfo.setOrderId(new Random(10010010).nextLong());
        orderInfo.setProjectName("商品");
        orderInfo.setOrderTime(getDateTime());
        Thread.sleep(120);
        return orderInfo;
    }

    public static String getDateTime() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    /**
     * 获取用户详情信息
     * @param userId
     * @date: 2022/3/2 13:42
     * @return: com.zlp.demo.v1.entity.UserInfo
     */
    public UserInfo getUserInfo(Long userId){
        UserInfo userInfo = new UserInfo();
        userInfo.setUser(getUser(userId));
        userInfo.setOrderInfo(getOrder(userId));
        return userInfo;
    }


}
