package com.zlp.queue;

import com.zlp.queue.entity.UserInfo;
import com.zlp.queue.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试类-控制层
 *
 * @version 2022年3月2日14:44:15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QueueApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BillTest {


    @Autowired
    private UserService userService;

    /**
     * 测试方法--test
     *
     * @version 2022年3月2日14:44:15
     */
    @Test
    public void test() {
        UserInfo userInfo = userService.getUserInfo(2L);
        System.out.println(userInfo.toString());

    }

    /**
     * 测试方法--test02
     *
     * @version 2022年3月2日14:44:15
     */
    @Test
    public void test02() {
        UserInfo userInfo = userService.getUserInfoFutureAsync(2L);
        System.out.println(userInfo.toString());

    }

    /**
     * 测试方法--test03
     *
     * @version 2022年3月2日14:44:15
     */
    @Test
    public void test03() {
        UserInfo userInfo = userService.getUserInfoCompletableFuture(2L);
        System.out.println(userInfo.toString());

    }
}

