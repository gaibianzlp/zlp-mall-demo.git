package com.zlp.queue;

import com.zlp.demo.v1.entity.UserInfo;
import com.zlp.demo.v1.service.UserService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

/**
 * 并发测试类-控制层
 *
 * @version 2022年3月2日14:44:15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QueueApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConcurrentTest {


    @Autowired
    private UserService userService;

    // 并发数量=10000
    private static final Integer NUM = 2;
    // 上课点名人数
    private CountDownLatch countDown = new CountDownLatch(NUM);

    /**
     * 测试方法--testSingle
     *
     * @version 2022年3月2日14:44:15
     */
    @SneakyThrows
    @Test
    public void testSingle() {
        Thread.sleep(500);
        for (int i = 0; i < NUM; i++) {
            Long userId = 100L+i;
            new Thread(() -> {
                try {
                    countDown.countDown(); // 点名
                    countDown.await(); // 等待->知道countDownw==0时，同时10000线程执行userService.getUserOrderInfo(userId)
                    UserInfo userInfo = userService.getUserOrderInfo(userId);
                    System.out.println("查询结果："+userInfo.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    /**
     * 测试方法--testBatch
     *
     * @version 2022年3月2日14:44:15
     */
    @Test
    public void testBatch() {

        for (int i = 0; i < NUM; i++) {
            Long userId = 100L+i;
            new Thread(() -> {
                try {
                    countDown.countDown(); // 点名
                    countDown.await(); // 等待->知道countDownw==0时，同时10000线程执行userService.getUserOrderInfo(userId)
                    UserInfo userInfo = userService.getUserOrderBatch(userId);
                    System.out.println("查询结果："+userInfo.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }



}

