package com.zlp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
// 资源服务配置
@Configuration
@EnableResourceServer
public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter {


    /**
     * 继承( extends ) ResourceServerConfigurerAdapter 类，
     * 并覆写 #configure(HttpSecurity http) 方法，配置对 HTTP 请求中，
     * 匹配 /api/**" 路径，开启认证的验证。
     * @param http 
     * @date: 2020/6/9 16:19
     * @return: void
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // 对 "/api/**" 开启认证
                .anyRequest()
                .authenticated()
                .and()
                .requestMatchers()
                .antMatchers("/api/**");
    }
}
