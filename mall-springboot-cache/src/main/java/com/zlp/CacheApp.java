package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @description: SpirngBoot整合 cache
 * @author: LiPing.Zou
 * @create: 2020-05-25 16:43
 **/
@EnableCaching // 开启缓存
@SpringBootApplication
public class CacheApp {

    public static void main(String[] args) {
        SpringApplication.run(CacheApp.class, args);
    }
}
