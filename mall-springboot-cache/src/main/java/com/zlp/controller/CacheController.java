package com.zlp.controller;
import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.zlp.dto.DeptResp;
import com.zlp.dto.UserResp;
import com.zlp.entity.User;
import com.zlp.service.UserService;
import com.zlp.utils.BeanToUtils;
import com.zlp.utils.CacheConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.bean.validators.plugins.schema.MinMaxAnnotationPlugin;

/**
 * <p>
 * 管理用户表 前端控制器
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@RestController
@RequestMapping("/cache")
@AllArgsConstructor
@Slf4j(topic = "CacheController")
@Api(value = "cache", tags = "缓存模块")
public class CacheController {

    private final UserService userService;

    /**
     * 缓存key = user:#userId <br/> 当根据UserId查询User对象为空也会缓存一条空的记录
     * @param userId
     * @date: 2021/4/15 10:08
     * @return: com.zlp.dto.UserResp
     */
    @GetMapping("getUserInfo00")
    @ApiOperation("获取用户信息-00")
    @Cacheable(value = CacheConstants.USER, key = "#userId")
    public UserResp getUserInfo00(
            @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId
    ){
        log.info("getUserInfo.req userId={}", userId);
        User user = userService.getById(userId);
        return BeanToUtils.doToDto(user, UserResp.class);
    }

    /**
     * 缓存key = user:#userId <br/> 当在 @Cacheable 中条添加条件判断属性时(condition = "#result != null")
     *
     *  <p>当根据UserId查询User对象为空 ((condition = "#result != null") == TRUE)不会缓存一条空的记录 </p>
     * @param userId
     * @date: 2021/4/15 10:08
     * @return: com.zlp.dto.UserResp
     */
    @GetMapping("getUserInfo01")
    @ApiOperation("获取用户信息-01")
    @Cacheable(value = CacheConstants.USER, key = "#userId" , condition = "#result != null")
    public UserResp getUserInfo01(
            @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId
    ){
        log.info("getUserInfo.req userId={}", userId);
        User user = userService.getById(userId);
        return BeanToUtils.doToDto(user, UserResp.class);
    }

    /**
     * 缓存key = userResp::class com.zlp.service.impl.UserServiceImplgetUserByUserUser
     *  (id=1, username=null, password=null, nickname=null, userType=null, status=null, createTime=null, createUser=null, updateTime=null, updateUser=null) <br/>
     * 当在 @Cacheable 中条添加条件判断属性时(condition = "#result != null")
     *
     *  <p>当根据UserId查询User对象为空 ((condition = "#result != null") == TRUE)不会缓存一条空的记录 </p>
     * @param user
     * @date: 2021/4/15 10:08
     * @return: com.zlp.dto.UserResp
     */
    @GetMapping("getUserByUser")
    @ApiOperation("获取用户信息对象")
    @Cacheable(value = "userResp" ,key = "targetClass + methodName +#p0")
    public UserResp getUserByUser(User user){
        Long userId = user.getId();
        User users = userService.getById(userId);
        return BeanToUtils.doToDto(users, UserResp.class);
    }

    /**
     *  更新缓存
     * @date: 2021/4/15 10:08
     * @return: com.zlp.dto.UserResp
     */
    @GetMapping("update")
    @ApiOperation("修改用户信息")
    @CachePut(value = CacheConstants.USER, key = "#userId" , condition = "#result != null" )
    public UserResp update(
            @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId,
            @RequestParam(value="nickname" )  @ApiParam(name="nickname",value="昵称",required = true) String nickname
    ){
        log.info("update.req userId={},nickname={}", userId,nickname);
        LambdaUpdateWrapper<User> wapper = new LambdaUpdateWrapper<>(new User())
                .eq(User::getId,userId)
                .set(User::getNickname,nickname);
        userService.update(null, wapper);
        User user = userService.getById(userId);
        return BeanToUtils.doToDto(user, UserResp.class);
    }

    /**
     * 清除一条缓存，key为要清空的数据
     * @date: 2021/4/15 9:29
     * @return: void
     */
    @GetMapping("deleteById")
    @ApiOperation("根据用户ID删除")
    @CacheEvict(value = CacheConstants.USER, key = "#userId" )
    public void deleteById(int userId) {
        userService.removeById(userId);
    }

    /**
     * 清空所有user:* 缓存 <br/>
     * @date: 2021/4/15 9:29
     * @return: void
     */
    @GetMapping("deleteAll")
    @ApiOperation("删除用户信息")
    @CacheEvict(value = CacheConstants.USER,allEntries=true)
    public void deleteAll(){

        log.info("deleteAll execute ....");
        LambdaUpdateWrapper<User> wapper = new LambdaUpdateWrapper<>(new User())
                .eq(User::getId,11)
                .set(User::getStatus,1);
        userService.update(null,wapper);
    }


    /**
     * 方法调用前清空所有缓存user:* 缓存 <br/>
     * @date: 2021/4/15 9:29
     * @return: void
     */
    @GetMapping("deleteAllBeforeInvocation")
    @ApiOperation("删除用户信息-beforeInvocation")
    @CacheEvict(value = CacheConstants.USER,beforeInvocation=true)
    public void deleteAllBeforeInvocation(){

        log.info("deleteAll execute ....");
        LambdaUpdateWrapper<User> wapper = new LambdaUpdateWrapper<>(new User())
                .eq(User::getId,11)
                .set(User::getStatus,1);
        userService.update(null,wapper);
    }

    /**
     *
     * @param userId
     * @date: 2021/4/15 10:08
     * @return: com.zlp.dto.UserResp
     */
    @GetMapping("saveDept")
    @ApiOperation("添加部门")
    @Cacheable(value = CacheConstants.DEPT, key = "#userId")
    public DeptResp saveDept(
            @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId
    ){
        log.info("saveDept.req userId={}", userId);
        DeptResp deptResp = new DeptResp();
        deptResp.setId(1L);
        deptResp.setUserId(userId);
        deptResp.setDeptName("研发部门");
        return deptResp;
    }


    /**
     *  更新用户和删除部门信息 缓存
     * @date: 2021/4/15 9:29
     * @return: void
     */
    @Caching(
            put = {
                 @CachePut(value = CacheConstants.USER,key = "#userId"),
            },evict = {
                @CacheEvict(value = CacheConstants.DEPT,key = "#userId"),
            })
    @GetMapping("saveMultiCachedUser")
    @ApiOperation("保存用户多缓存")
    public UserResp saveMultiCachedUser(
            @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId
    ){

        UserResp userResp = new UserResp();
        log.info("deleteAll execute ....");
        LambdaUpdateWrapper<User> wapper = new LambdaUpdateWrapper<>(new User())
                .eq(User::getId,userId)
                .set(User::getStatus,1);
        userService.update(null,wapper);
        User user = userService.getById(userId);
        return BeanToUtils.doToDto(user, UserResp.class);
    }

    @ApiOperation("testEhcache")
    @GetMapping("testEhcache")
    @Cacheable(value = CacheConstants.CACHE_NAME ,key = "#userId")
    public User testEhcache( @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId) {
        System.out.println("没有缓存则执行如下代码");
        User user = new User();
        user.setId(userId);
        user.setUsername("testEhcache");
        user.setPassword("testEhcache");
        user.setNickname("testEhcache");
        user.setUserType(0);
        user.setStatus(0);
        user.setCreateTime(new Date());
        return user;
    }




}

