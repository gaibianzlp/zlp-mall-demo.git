package com.zlp.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
//import com.zlp.component.EhcachServiceComponet;
import com.zlp.component.RedisServiceComponet;
import com.zlp.dto.DeptResp;
import com.zlp.dto.UserResp;
import com.zlp.entity.User;
import com.zlp.service.UserService;
import com.zlp.utils.BeanToUtils;
import com.zlp.utils.CacheConstants;
import com.zlp.utils.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Objects;

/**
 * <p>
 * 管理用户表 前端控制器
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@RestController
@RequestMapping("/redis")
@AllArgsConstructor
@Slf4j(topic = "RedisController")
@Api(value = "redis", tags = "redis缓存模块")
public class RedisController {

    private final UserService userService;

//    private final EhcachServiceComponet ehcachServiceComponet;

    private final RedisServiceComponet redisServiceComponet;

    /**
     * 缓存key = user:#userId <br/> 当根据UserId查询User对象为空也会缓存一条空的记录
     * @param userId
     * @date: 2021/4/15 10:08
     * @return: com.zlp.dto.UserResp
     */
    @GetMapping("getUserInfo00")
    @ApiOperation("获取用户信息-00")
    @Cacheable(value = CacheConstants.CACHE_NAME ,key = "#userId")
    public User getUser(Long userId) {
        /*//首先从本地ehcach中取数据
        User user  = (User)ehcachServiceComponet.get(CacheConstants.CACHE_NAME,userId.toString());
        if(Objects.nonNull(user)){
            //如果存在，则直接返回，如果不存在则从redis中取数据
            log.info("我是从ehcache中查出来的:{} ======= >>>",userId);
            return user;
        }*/
        //如果redis有则直接返回，如果redis 没有则查询数据
        String key  = CacheConstants.CACHE_NAME + StrUtil.COLON + userId;
        Object value = redisServiceComponet.get(key);
        //如果数据库查询不为空则，则讲返回结果缓存到本地和redis缓存中
        if(Objects.nonNull(value)){
            log.info("我是从redis中查出来的:{} ======= >>>",userId);
            return (User) value;
        }
        // redis 也为空则从数据库中获取
        User user = userService.getById(userId);
        if(Objects.nonNull(user)){
            //缓存到redis中
            redisServiceComponet.set(key,user,140);
            log.info("我是从mysql中查出来的: ======= >>>" + userId);
            // 缓存到本地ehcache中
//            ehcachServiceComponet.put(CacheConstants.CACHE_NAME,key,user);
            return user;
        }else{
            return user;
        }
    }


}

