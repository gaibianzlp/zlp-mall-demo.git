package com.zlp.service;

import com.zlp.dto.UserResp;
import com.zlp.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zlp.common.api.Pager;

/**
 * <p>
 * 管理用户表 服务类
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
public interface UserService extends IService<User> {



    
    /** 
     * 获取用户信息
     * @param userId
     * @date: 2021/3/11 15:19
     * @return: java.lang.Object 
     */
    UserResp getUserInfo(Long userId);

    UserResp update(Long userId, String nickname);

    /**
     * 获取用户信息
     * @param user 
     * @date: 2021/4/14 21:20
     * @return: java.lang.Object
     */
    UserResp getUserByUser(User user);

    /**
     * 删除用户信息
     * @date: 2021/4/15 9:13
     * @return: void
     */
    void deleteAll();
}
