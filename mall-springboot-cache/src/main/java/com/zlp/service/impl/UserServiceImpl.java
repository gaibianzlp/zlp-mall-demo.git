package com.zlp.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlp.dto.UserResp;
import com.zlp.entity.User;
import com.zlp.exception.CustomException;
import com.zlp.mapper.UserMapper;
import com.zlp.service.UserService;
import com.zlp.utils.BeanToUtils;
import com.zlp.utils.CacheConstants;
import com.zlp.utils.MethodUtil;
import com.zlp.utils.RedisUtils;
import com.zlp.common.api.ResultCode;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * 管理用户表 服务实现类
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@Service
@AllArgsConstructor
@Slf4j(topic = "UserServiceImpl")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final RedisUtils redisUtils;


    @Override
    @Cacheable(value = CacheConstants.USER, key = "#userId" , condition = "#result != null")
    public UserResp getUserInfo(Long userId) {

        log.info("getUserInfo.req userId={}", userId);
        User user = this.getById(userId);
        if (Objects.isNull(user)) {
            throw new CustomException(ResultCode.SYS_10003, MethodUtil.getLineInfo());
        }
        return BeanToUtils.doToDto(user, UserResp.class);
    }

    @Override
    @CachePut(value = CacheConstants.USER, key = "#userId" , condition = "#result != null" )
    public UserResp update(Long userId, String nickname) {

        log.info("getUserInfo.req userId={}", userId);
        User user = this.getById(userId);
        if (Objects.isNull(user)) {
            throw new CustomException(ResultCode.SYS_10003, MethodUtil.getLineInfo());
        }
        user.setNickname(nickname);
        this.updateById(user);
        user.setNickname(nickname);
        return BeanToUtils.doToDto(user, UserResp.class);
    }

    @Override
    @Cacheable(value = "userResp" ,key = "targetClass + methodName +#p0")
    public UserResp getUserByUser(User user) {
        Long userId = user.getId();
        User users = this.getById(userId);
        if (Objects.isNull(users)) {
            throw new CustomException(ResultCode.SYS_10003, MethodUtil.getLineInfo());
        }
        return BeanToUtils.doToDto(users, UserResp.class);
    }
    
    /** 
     * 清空所有user:* 缓存
     * @date: 2021/4/15 9:29
     * @return: void 
     */
    @Override
    @CacheEvict(value = CacheConstants.USER,allEntries=true)
    public void deleteAll() {

        log.info("deleteAll execute ....");
        LambdaUpdateWrapper<User> wapper = new LambdaUpdateWrapper<>(new User())
                .eq(User::getId,11)
                .set(User::getStatus,1);
        update(null,wapper);


    }


}
