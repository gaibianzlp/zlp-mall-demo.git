package com.zlp.utils;

public interface CacheConstants {

        /**
         * 用户缓存
         */
        String USER = "user";

        /**
         * 角色缓存
         */
        String ROLE = "role";

        /**
         * 部门缓存
         */
        String DEPT = "dept";

        /**
         * CACHE_NAME
         */
        String CACHE_NAME = "userCache";
}
