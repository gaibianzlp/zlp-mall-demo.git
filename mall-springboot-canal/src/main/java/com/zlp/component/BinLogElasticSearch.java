package com.zlp.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.Message;
import com.zlp.config.CanalDataParser;
import com.zlp.entity.es.Googs;
import com.zlp.repository.GoodsRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class BinLogElasticSearch {

    @Resource
    private CanalConnector canalSimpleConnector;

    @Autowired
    private GoodsRepository coodsRepository;

    @Resource
    private CanalConnector canalHaConnector;

    @SneakyThrows
    public void binLogToElasticSearch(){

        openCanalConnector(canalHaConnector);
        // 轮询拉取数据
        Integer batchSize = 5 * 1024;
        while (true) {
            Message message = canalSimpleConnector.getWithoutAck(batchSize);
            long id = message.getId();
            int size = message.getEntries().size();
            log.info("当前监控到binLog消息数量size={}", size);
            if (id == -1 || size == 0) {
                // 等待10秒
                Thread.sleep(10000);
            } else {
                //1. 解析message对象
                List<CanalEntry.Entry> entries = message.getEntries();
                List<CanalDataParser.TwoTuple<CanalEntry.EventType, Map>> rows = CanalDataParser.printEntry(entries);
                for (CanalDataParser.TwoTuple<CanalEntry.EventType, Map> tuple : rows) {
                    if(tuple.eventType == CanalEntry.EventType.INSERT) {
                        Googs goods = createGoods(tuple);
                        log.info("insert同步es对象goods={}", JSON.toJSONString(goods));
                        // 2. 将解析出的对象同步到elasticSearch中
                        coodsRepository.save(goods);
                        // 3.消息确认已处理
                        canalHaConnector.ack(id);
                    }
                    if(tuple.eventType == CanalEntry.EventType.UPDATE){
                        Googs goods = createGoods(tuple);
                        log.info("UPDATE同步es对象goods={}", JSON.toJSONString(goods));
                        coodsRepository.save(goods);
                        // 3.消息确认已处理
                        canalHaConnector.ack(id);
                    }
                    if(tuple.eventType == CanalEntry.EventType.DELETE){
                        Long goodId = Long.parseLong(tuple.columnMap.get("id").toString());
                        log.info("DELETE同步es中goodId={}", goodId);
                        coodsRepository.deleteById(goodId);
                        canalHaConnector.ack(id);
                    }
                }
            }
        }
    }



    /**
     * 打开canal连接
     * @param canalConnector
     */
    private void openCanalConnector(CanalConnector canalConnector) {
        //连接CanalServer
        canalConnector.connect();
        // 订阅destination
        // 过滤数据
        canalConnector.subscribe("mall.goods");
    }

    /**
     * 封装数据至goods
     * @param tuple
     * @return
     */
    private Googs createGoods(CanalDataParser.TwoTuple<CanalEntry.EventType, Map> tuple){
        Googs googs = new Googs();
        googs.setId(Long.parseLong(tuple.columnMap.get("id").toString()));
        googs.setTitle((tuple.columnMap.get("title").toString()));
        googs.setCategory(tuple.columnMap.get("category").toString());
        googs.setBrand(tuple.columnMap.get("brand").toString());
        googs.setPrice(Long.parseLong(tuple.columnMap.get("price").toString()));
        googs.setImages(tuple.columnMap.get("images").toString());
        return googs;
    }

    /**
     * 关闭canal连接
     * @param canalConnector
     */
    private void closeCanalConnector(CanalConnector canalConnector) {
        //关闭连接CanalServer
        canalConnector.disconnect();
        // 注销订阅destination
        canalConnector.unsubscribe();
    }
}
