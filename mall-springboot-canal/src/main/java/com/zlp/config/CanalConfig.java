package com.zlp.config;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.net.InetSocketAddress;

@Configuration
public class CanalConfig {

    public final static String CANAL_ADDRESS = "47.103.20.21";

    public final static int PORT = 11111;

    public final static String DESTINATION = "example";

    /**
     * 这个可以根据需要修改过滤，默认是直接监听所有
     */
    public static String FILTER = ".*\\..*";

    /**
     * canal服务连接IP
     */
    @Value("${canal.server.ip}")
    private String canalIp;
    /**
     * canal服务端口
     */
    @Value("${canal.server.port}")
    private Integer canalPort;
    /**
     * canal 存放目的地
     */
    @Value("${canal.destination}")
    private String destination;
    /**
     * 监听表过滤器
     */
    @Value("${canal.filter}")
    private String filter;


    /**
     * 获取canal-server连接
     * @date: 2020/7/30 14:34
     * @return: com.alibaba.otter.canal.client.CanalConnector 
     */
    @Bean
    public CanalConnector canalSimpleConnector() {
        CanalConnector canalConnector = CanalConnectors.newSingleConnector(new InetSocketAddress(CANAL_ADDRESS, PORT),
                DESTINATION, "", "");
        return canalConnector;
    }
}
