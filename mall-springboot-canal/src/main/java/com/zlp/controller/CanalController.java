package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.component.BinLogElasticSearch;
import com.zlp.entity.es.EsContent;
import com.zlp.entity.es.EsProduct;
import com.zlp.entity.es.EsProductCategory;
import com.zlp.entity.es.Googs;
import com.zlp.repository.EsContentRepository;
import com.zlp.repository.EsProductRepository;
import com.zlp.repository.GoodsRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.applet.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
public class CanalController {

    private BinLogElasticSearch binLogElasticSearch;
    private ElasticsearchTemplate elasticsearchTemplate;
    private EsProductRepository esProductRepository;
    private GoodsRepository goodsRepository;
    private EsContentRepository esContentRepository;


    @GetMapping("syncEs")
    @SneakyThrows
    public String syncEsDemo() {
        binLogElasticSearch.binLogToElasticSearch();
        return "sync es";
    }

    /**
     * 创建 Goods 文档对象
     */
    @GetMapping("createIndex")
    public void createIndex() {
        elasticsearchTemplate.createIndex(Googs.class);
    }

    /**
     * 创建 Goods 文档对象
     */
    @GetMapping("createProductIndex")
    public void createProductIndex() {
        elasticsearchTemplate.createIndex(EsProduct.class);
    }


    /**
     * @desc 定义批量新增方法
     */
    @GetMapping("insertProductsList")
    public String insertProductsList() {

        List<EsProduct> list = new ArrayList<>();
        EsProduct product = new EsProduct();
        product.setProductId(3L);
        product.setProductCommonName("儿童感冒咳嗽理疗贴");
        product.setProductBrand("好将来贴贴佳");
        product.setApprovalNumber("黔械注准20152260004");
        product.setProductBrandKeyWord("好将来贴贴佳");
        EsProductCategory esProductCategory = new EsProductCategory();
        esProductCategory.setCateId(4L);
        esProductCategory.setCateId(54L);
        esProductCategory.setCateId2(164L);
        List<EsProductCategory> categorys = Arrays.asList(esProductCategory);
        product.setProductCategorys(categorys);
        product.setFreightTemplateId(null);
        product.setProductSku("400002");
        product.setProductImage("https://product-img-bucket.oss-cn-shanghai.aliyuncs.com/15365/f5797649c4524a7f685b5eb79a4c1f8a.jpg;" +
                "https://product-img-bucket.oss-cn-shanghai.aliyuncs.com/15365/8c0855b5c8359852ab44171968616920.jpg;" +
                "https://product-img-bucket.oss-cn-shanghai.aliyuncs.com/15365/78fc9bd4d610bb23e9eb74561b1a7442.jpg;" +
                "https://product-img-bucket.oss-cn-shanghai.aliyuncs.com/15365/f840dec8d313d0570891a08ffff22b47.jpg;" +
                "https://product-img-bucket.oss-cn-shanghai.aliyuncs.com/15365/d8a109d88715284ed06cc13f46fe988c.jpg");
        product.setIsMp(3);
        product.setPrice((double) 100);
        product.setDiscountPrice(null);
        product.setStock(200);
        product.setSales(0);
        product.setRecommandStatus(0);
        product.setProductDesc("儿童感冒咳嗽理疗贴");
        product.setProductSpecif("<p>【产品名称】儿童感冒咳嗽理疗贴</p><p> " +
                "【产品结构及组成】本品由远红外纳米粉、磁性材料、基质和医用透气胶带组成。远红外波长7μm~14μm，" +
                "发射率不低于70%，磁性材料产生的磁场度强度不低于2.5Gs。</p><p> 【产品性能】利用远红外线具有的热效应，" +
                "磁场材料所产生的磁场效应，远红外线的热疗和磁场效应的磁疗双重作用，改善局部血液循环和人体微循环，" +
                "具有加强代谢，促进炎症消散、止咳的作用。</p><p> 【适用范围】适用于缓解儿童感冒引起的咳嗽。" +
                "</p><p> 【使用方法】外用。将贴片的隔离纸掲除，用贴片对准肚脐贴实，每片贴12小时，一日一贴。" +
                "</p><p> 【不良反应】皮肤粘贴处偶见过敏反应。</p><p> 【禁忌】禁止内服，肚脐皮肤破损或肚脐部位溃疡者禁用。" +
                "</p><p> 【注意事项】过敏体质禁用；使用过程中出现有皮肤红肿、水泡等应停止使用本品。" +
                "</p><p> 【规格】65mm*65mm，贴片直径：35mm</p><p> 【包装】复合膜，3贴装/盒。" +
                "</p><p> 【贮藏】密封，置干燥处。</p><p> 【有效期】999</p><p> " +
                "【批准文号】黔械注准20152260005</p><p> 【生产企业】贵州苗通生物医药开发有限公司</p>");
        product.setProductModel("片挤");
        product.setEnglishName(null);
        product.setPinyin("ertongganmaokesouliliaotie");
        product.setProductExpire("24个月");
        product.setManufacturer("贵州苗通生物医药开发有限公司");
        product.setProductSpec("65mm*65mm*3贴");
        product.setPromotionType(2);
        product.setServiceIds("1");
        product.setIsShelf(0);
        product.setCreateTime(new Date());
        list.add(product);
        // 接收对象集合，实现批量新增
        String strlist = JSON.toJSONString(list);
        log.info("esProductRepository.saveAll.req list={}",strlist);
        esProductRepository.saveAll(list);
        return strlist;

    }

    /**
     * @desc 定义批量新增方法
     */
    @GetMapping("insertList")
    public void insertList() {
        List<Googs> list = new ArrayList<>();
        list.add(new Googs(24L, "小米R2", "33", "锤子1", 369900L, "http://image.baidu.com/13123.jpg"));
        list.add(new Googs(35L, "vivo10", "67", "华为v1", 280000L, "http://image.baidu.com/13123.jpg"));
        list.add(new Googs(16L, "华为META20", " 23", "华为", 144993L, "http://image.baidu.com/13123.jpg"));
        list.add(new Googs(17L, "华为META30", " 33", "华为", 154993L, "http://image.baidu.com/13123.jpg"));
        list.add(new Googs(18L, "华为META40", " 31", "华为", 164993L, "http://image.baidu.com/13123.jpg"));
        list.add(new Googs(19L, "华为META50", " 54", "华为", 143993L, "http://image.baidu.com/13123.jpg"));
        // 接收对象集合，实现批量新增
        goodsRepository.saveAll(list);
    }

    /**
     * @description:matchQuery底层采用的是词条匹配查询
     */
    @GetMapping("testMatchQuery")
    public Page<Googs> testMatchQuery() {
        // 构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 添加基本分词查询
        queryBuilder.withQuery(QueryBuilders.matchQuery("title", "小米手机"));
        // 精确查找
//        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
//        boolQueryBuilder.must(QueryBuilders.termsQuery("category", "33"));
//        queryBuilder.withQuery(boolQueryBuilder);
        // 设置分页参数
        queryBuilder.withPageable(PageRequest.of(0, 25));

        // 排序
        queryBuilder.withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC));
        NativeSearchQuery searchQuery = queryBuilder.build();
        System.out.println("DSL===" + JSON.toJSONString(searchQuery));
        // 搜索，获取结果
        Page<Googs> items = this.goodsRepository.search(searchQuery);
        // 总条数
        long total = items.getTotalElements();
        // 总页数
        long totalPages = items.getTotalPages();
        System.out.println("total = " + total);
        System.out.println("totalPages = " + totalPages);
        for (Googs goods : items) {
            System.out.println(goods.toString());
        }
        return items;
    }


    /**
     * @description:matchQuery底层采用的是词条匹配查询
     */
    @GetMapping("testProductMatchQuery")
    public Page<EsProduct> testProductMatchQuery() {
        // 构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 添加基本分词查询
//        queryBuilder.withQuery(QueryBuilders.matchQuery("title", "小米手机"));
        // 精确查找
//        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
//        boolQueryBuilder.must(QueryBuilders.termsQuery("category", "33"));
//        queryBuilder.withQuery(boolQueryBuilder);
        // 设置分页参数
        queryBuilder.withPageable(PageRequest.of(0, 25));
        // 权重查询
        List<FunctionScoreQueryBuilder.FilterFunctionBuilder> filterFunctionBuilders = new ArrayList<>();
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.matchQuery("productCommonName", "好将来"),
                ScoreFunctionBuilders.weightFactorFunction(10)));
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.matchQuery("productBrand", "好将来"),
                ScoreFunctionBuilders.weightFactorFunction(10)));
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.matchQuery("approvalNumber", "好将来"),
                ScoreFunctionBuilders.weightFactorFunction(2)));
        FunctionScoreQueryBuilder.FilterFunctionBuilder[] builders = new FunctionScoreQueryBuilder.FilterFunctionBuilder[filterFunctionBuilders.size()];
        filterFunctionBuilders.toArray(builders);
        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(builders)
                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                .setMinScore(2);
        queryBuilder.withQuery(functionScoreQueryBuilder);
        // 排序
        queryBuilder.withSort(SortBuilders.fieldSort("productId").order(SortOrder.DESC));
        NativeSearchQuery searchQuery = queryBuilder.build();
        System.out.println("DSL===" + JSON.toJSONString(searchQuery));
        // 搜索，获取结果
        Page<EsProduct> items = this.esProductRepository.search(searchQuery);
        // 总条数
        long total = items.getTotalElements();
        // 总页数
        long totalPages = items.getTotalPages();
        System.out.println("total = " + total);
        System.out.println("totalPages = " + totalPages);
        for (EsProduct product : items) {
            System.out.println(product.toString());
        }
        return items;


    }


    /**
     * @description:matchQuery底层采用的是词条匹配查询
     */
    @GetMapping("testContentMatchQuery")
    public Page<EsContent> testContentMatchQuery() {
        // 构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 添加基本分词查询
//        queryBuilder.withQuery(QueryBuilders.matchQuery("title", "小米手机"));
        // 精确查找
//        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
//        boolQueryBuilder.must(QueryBuilders.termsQuery("category", "33"));
//        queryBuilder.withQuery(boolQueryBuilder);

        // 精确查找
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //帖子状态
        boolQueryBuilder.must(QueryBuilders.termQuery("postState", 1));
        // 内容发布状态
        List<Integer> statusList = Arrays.asList(2, 4);
        // 下面会带一个以上的条件，至少满足一个条件，这个文档就符合should
        boolQueryBuilder.should(QueryBuilders.termQuery("status", statusList));
        queryBuilder.withFilter(boolQueryBuilder);
        // 设置分页参数
        queryBuilder.withPageable(PageRequest.of(0, 5));
        // 权重查询
        List<FunctionScoreQueryBuilder.FilterFunctionBuilder> filterFunctionBuilders = new ArrayList<>();
        filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.matchQuery("title", "titleTest1598864690055"),
                ScoreFunctionBuilders.weightFactorFunction(10)));
        /*filterFunctionBuilders.add(new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.matchQuery("contentText", "contentTest1598864721949"),
                ScoreFunctionBuilders.weightFactorFunction(5)));*/
        FunctionScoreQueryBuilder.FilterFunctionBuilder[] builders = new FunctionScoreQueryBuilder.FilterFunctionBuilder[filterFunctionBuilders.size()];
        filterFunctionBuilders.toArray(builders);
        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(builders)
                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                .setMinScore(2);
        queryBuilder.withQuery(functionScoreQueryBuilder);
        // 排序
        queryBuilder.withSort(SortBuilders.fieldSort("contentId").order(SortOrder.DESC));
        NativeSearchQuery searchQuery = queryBuilder.build();
        System.out.println("DSL===" + JSON.toJSONString(searchQuery));
        // 搜索，获取结果
        Page<EsContent> items = this.esContentRepository.search(searchQuery);
        // 总条数
        long total = items.getTotalElements();
        // 总页数
        long totalPages = items.getTotalPages();
        System.out.println("total = " + total);
        System.out.println("totalPages = " + totalPages);
        for (EsContent product : items) {
            System.out.println(product.toString());
        }
        return items;


    }


}
