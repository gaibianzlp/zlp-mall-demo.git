package com.zlp.entity;

import lombok.Data;

@Data
public class Student {

    private Integer id;
    private Integer age;
    private String name;
    private String sex;
    private String city;
}
