package com.zlp.entity.es;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

/**
 * 内容文档
 * @date 2020-9-14 10:54:41
 * @author zouLiPing
 */
@Data
@Document(indexName = "es_content", type = "content", shards = 1, replicas = 0)
public class EsContent implements Serializable {

	private static final long serialVersionUID = -7572216500617983721L;

	@Id
	private Long contentId;
	/**
	 * 文章标题
	 */
	@Field(analyzer = "ik_max_word", type = FieldType.Text)
	private String title;

	/**
	 * 内容
	 */
	@Field(analyzer = "ik_max_word", type = FieldType.Text)
	private String content;


	/**
	 * 内容文字
	 */
	@Field(analyzer = "ik_max_word", type = FieldType.Text)
	private String contentText;

	/**
	 * 用户ID
	 */
	@Field(type = FieldType.Keyword)
	private Long userId;

	/**
	 * 	发布时间
	 */
	@Field(type = FieldType.Date)
	private Date postTime;

	/**
	 * 帖子状态 （1-开放, 2-关闭）
	 */
	@Field(type = FieldType.Keyword)
	private Integer postState;

	/**
	 * 帖子显示顺序(1正常、2-置顶)
	 */
	@Field(type = FieldType.Keyword)
	private Integer postOrder;

	/**
	 * 基础热度分
	 */
	private Long basePopularityScore;

	/**
	 * 内容发布图片
	 */
	private String postImages;

	/**
	 * 状态（1-保存草稿箱; 2-发布; 3-拒绝；4-审核成功 ;5-拒绝从新编辑发布）
	 */
	@Field(type = FieldType.Keyword)
	private Integer status;

	/**
	 * 创建时间
	 */
	@Field(type = FieldType.Date)
	private Date createTime;

	/**
	 * 创建用户
	 */
	@Field(type = FieldType.Keyword)
	private String createUser;
















}
