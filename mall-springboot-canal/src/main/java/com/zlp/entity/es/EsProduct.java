package com.zlp.entity.es;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * es文档实体
 * 
 * @author admin
 *
 */
@Data
@Document(indexName = "es_product", type = "product", shards = 1, replicas = 0)
public class EsProduct implements Serializable {
	private static final long serialVersionUID = -7572216500617983721L;

	@Id
	private Long productId;
	/**
	 * 通用名称
	 */
	@Field(analyzer = "ik_max_word", type = FieldType.Text)
	private String productCommonName;
	/**
	 * 商品品牌
	 */
	@Field(analyzer = "ik_max_word", type = FieldType.Text)
	private String productBrand;
	/**
	 * 批准文号
	 */
	@Field(analyzer = "ik_max_word", type = FieldType.Text)
	private String approvalNumber;

	@Field(type = FieldType.Keyword)
	private String productBrandKeyWord;
	/**
	 * 分类编号
	 */
	@Field(type = FieldType.Nested)
	private List<EsProductCategory> productCategorys;
	/**
	 * 模板编号
	 */
	private Long freightTemplateId;
	/**
	 * sku
	 */
	private String productSku;
	/**
	 * 商品图片
	 */
	private String productImage;
	/**
	 * 是否处方药(0=OTC 1=红色OTC 2=双规RX 3=其他)
	 */
	private Integer isMp;
	/**
	 * 产品价格
	 */
	private Double price;
	/**
	 * 折扣价
	 */
	private Double discountPrice;
	/**
	 * 库存
	 */
	private Integer stock;
	/**
	 * 销量
	 */
	private Integer sales;
	/**
	 * 推荐状态；0->不推荐；1->推荐
	 */
	@Field(type = FieldType.Keyword)
	private Integer recommandStatus;
	/**
	 * 商品说明
	 */
	private String productDesc;
	/**
	 * 产品规格
	 */
	private String productSpecif;
	/**
	 * 剂型/型号
	 */
	private String productModel;
	/**
	 * 英文名
	 */
	private String englishName;
	/**
	 * 拼音
	 */
	private String pinyin;
	/**
	 * 产品有效期
	 */
	private String productExpire;
	/**
	 * 生产厂家
	 */
	private String manufacturer;
	/**
	 * 产品说明书
	 */
	private String productSpec;
	/**
	 * 促销类型
	 */
	private Integer promotionType;

	/**
	 * 服务类型
	 */
	private String serviceIds;
	/**
	 * 是否上架(0：未上架，1：上架)
	 */
	@Field(type = FieldType.Keyword)
	private Integer isShelf;
	
	@Field(type = FieldType.Date)
	private Date createTime;


}
