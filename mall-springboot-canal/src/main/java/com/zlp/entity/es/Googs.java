package com.zlp.entity.es;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @description：商品entity
 * @author ：ZouLiPing
 * @date ： 2019/9/2 16:15
 */
@Data
@Document(indexName = "es_good",type = "goods", shards = 1, replicas = 0)
@AllArgsConstructor
@NoArgsConstructor
public class Googs {

    /**
     * 作用在成员变量，标记一个字段作为id主键
     */
    @Id
    private Long id;

    /**
     * 标题
     */
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String title;
    /**
     * 分类
     */
    @Field(type = FieldType.Keyword)
    private String category;
    /**
     * 品牌
     */
    @Field(type = FieldType.Keyword)
    private String brand;
    /**
     * 价格
     */
    @Field(type = FieldType.Long)
    private Long price;
    /**
     * 图片地址
     */
    private String images;



}
