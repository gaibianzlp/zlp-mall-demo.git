package com.zlp.repository;

import com.zlp.entity.es.EsContent;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EsContentRepository extends ElasticsearchRepository<EsContent, Long>
{



}
