package com.zlp.repository;

import com.zlp.entity.es.EsProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * @author ：ZouLiPing
 * @description：定义ItemRepository 接口
 * @date ： 2019/9/2 18:06
 */
public interface EsProductRepository extends ElasticsearchRepository<EsProduct,Long> {





}
