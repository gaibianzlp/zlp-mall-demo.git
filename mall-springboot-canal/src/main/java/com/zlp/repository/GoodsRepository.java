package com.zlp.repository;

import com.zlp.entity.es.Googs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import java.util.List;


/**
 * @author ：ZouLiPing
 * @description：定义ItemRepository 接口
 * @date ： 2019/9/2 18:06
 */
public interface GoodsRepository extends ElasticsearchRepository<Googs,Long> {


    /**
     * @Description:根据价格区间查询
     * @Param price1
     * @Param price2
     */
    List<Googs> findByPriceBetween(Long price1, Long price2);


}
