package com.zlp.test;

import java.util.*;

public class MapToString {

    public static String convertToString(Map<String, Object> properties) {

        List<String> parts = new ArrayList<>();
        for (String key : properties.keySet()) {
            Object value = properties.get(key);
            if (value instanceof String) {
                parts.add(encode('=', new String[] { key, (String) value }));
            } else {
                throw new RuntimeException("Can't encode " + value);
            }
        }
        return encode(';', parts);
    }
    public static String encode(char delimiter, String[] parts) {
        java.util.List<String> partList = new java.util.ArrayList<String>();
        for (String part : parts) {
            partList.add(part);
        }
        return encode(delimiter, partList);
    }
    public static String encode(char delimiter, Iterable<String> parts) {
        StringBuilder result = new StringBuilder();
        for (String part : parts) {
            String encodedPart = part.replace("\\", "\\\\");
            encodedPart = encodedPart.replace("" + delimiter, "\\"
                    + delimiter);
            result.append(encodedPart);
            result.append(delimiter);
        }
        return result.toString();
    }

    //参数类型是Map<String,String> 因为支付只能用string的参数。如果诸君还需要修改的话，那也可以适当的做调整
    /**
     *
     * map转str
     * @param map
     * @return
     */
    public static String getMapToString(Map<String,String> map){
        Set<String> keySet = map.keySet();
        //将set集合转换为数组
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        //给数组排序(升序)
        Arrays.sort(keyArray);
        //因为String拼接效率会很低的，所以转用StringBuilder。博主会在这篇博文发后不久，会更新一篇String与StringBuilder开发时的抉择的博文。
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keyArray.length; i++) {
            // 参数值为空，则不参与签名 这个方法trim()是去空格
            if (map.get(keyArray[i]).length() > 0) {
                sb.append(keyArray[i]).append("=").append(map.get(keyArray[i]).trim());
            }
            if(i != keyArray.length-1){
                sb.append("&");
            }
        }
        return sb.toString();
    }


    public static void main(String[] args) {
        Map<String, String> properties = new HashMap<>();
        properties.put("id","1");
        properties.put("name","张三");
        properties.put("sex","男");
        System.out.println(getMapToString(properties));
    }

}
