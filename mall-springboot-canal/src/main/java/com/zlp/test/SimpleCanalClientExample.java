package com.zlp.test;

import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.protocol.CanalEntry.*;
import com.alibaba.otter.canal.protocol.Message;
import com.zlp.config.CanalConfig;
import com.zlp.config.CanalDataParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class SimpleCanalClientExample {

    @Autowired
    private CanalConnector canalSimpleConnector;

    @Autowired
    @Qualifier("canalHaConnector")
    private CanalConnector canalHaConnector;


    public static void main(String args[]) {
        // 创建链接
        //192.168.35.254为canal-deployer启动的ip地址
        //canal-deployer如果你没有修改过的话，默认端口是11111
        CanalConnector connector = CanalConnectors.newSingleConnector(new InetSocketAddress(CanalConfig.CANAL_ADDRESS, CanalConfig.PORT), "example", "", "");
        //底层默认是1000
        int batchSize = 1;
        try {
            connector.connect();
            connector.subscribe(".*\\..*");
            // connector.subscribe("canal_tsdb.test_table1");
            //把上次停止后未提交的数据回滚，因为不确定是否已处理
            connector.rollback();
            while (true) {
                log.info("当前时间为：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                //每次获取多少条数据
                Message message = connector.getWithoutAck(batchSize);
                //System.out.println("内容为："+ JSON.toJSONString(message));
                long batchId = message.getId();
                int size = message.getEntries().size();
                if (batchId == -1 || size == 0) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                    }
                } else {
                    try {
                        System.out.println(batchId);
//                        printEntry(message.getEntries());
                        List<CanalDataParser.TwoTuple<EventType, Map>> twoTuples = CanalDataParser.printEntry(message.getEntries());
                        for (CanalDataParser.TwoTuple<EventType, Map> twoTuple : twoTuples) {
                            System.out.println(twoTuple.eventType);
                            Map columnMap = twoTuple.columnMap;
                            System.out.println(columnMap);
                        }

                        // int i=1/0;
                        connector.ack(batchId); // 提交确认
                    } catch (Exception e) {
                        connector.rollback(batchId); // 处理失败, 回滚数据
                    }
                }
            }
        } finally {
            connector.disconnect();
        }
    }
    private static void printEntry(List<Entry> entrys) {
        log.info("读取长度为：" + entrys.size());
        for (Entry entry : entrys) {
            if (entry.getEntryType() == EntryType.TRANSACTIONBEGIN || entry.getEntryType() == EntryType.TRANSACTIONEND) {
                continue;
            }
            RowChange rowChage = null;
            try {
                rowChage = RowChange.parseFrom(entry.getStoreValue());
            } catch (Exception e) {
                throw new RuntimeException("ERROR ## parser of eromanga-event has an error , data:" + entry.toString(),
                        e);
            }
            EventType eventType = rowChage.getEventType();
            log.info(String.format("================&gt; binlog[%s:%s] , name[%s,%s] , eventType : %s",
                    entry.getHeader().getLogfileName(), entry.getHeader().getLogfileOffset(),
                    entry.getHeader().getSchemaName(), entry.getHeader().getTableName(),
                    eventType));
            for (RowData rowData : rowChage.getRowDatasList()) {
                // 删除
                if (eventType == EventType.DELETE) {
                    printColumn(rowData.getBeforeColumnsList());
                // 插入
                } else if (eventType == EventType.INSERT) {
                    printColumn(rowData.getAfterColumnsList());
                // 修改
                } else {
                    System.out.println("-------&gt; before");
                    printColumn(rowData.getBeforeColumnsList());
                    System.out.println("-------&gt; after");
                    printColumn(rowData.getAfterColumnsList());
                }
            }
        }
    }
    private static void printColumn(List<Column> columns) {
        for (Column column : columns) {
            System.out.println(column.getName() + " : " + column.getValue() + "    update=" + column.getUpdated());
        }
    }
}
