package com.zlp.test;

import lombok.SneakyThrows;

public class ThreadSate {

    public static void main(String[] args) {
        testStateNew();
//        thread01();
    }
    @SneakyThrows
    private static void testStateNew() {
        Thread thread = new Thread(() -> {});
        System.out.println(thread.getState()); // 输出 NEW
        Thread.sleep(1000L);
        System.out.println(thread.getState()); // 输出 NEW
    }

    private static void thread01(){

        Thread thread1 = new Thread(() -> {
            System.out.println("thread1 执行");
        });
        thread1.start();
        thread1.start();
    }
}
