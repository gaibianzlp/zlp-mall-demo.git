package com.zlp.event;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy
@SpringBootApplication
public class EventApp {

    public static void main(String[] args) {
        SpringApplication.run(EventApp.class,args);
    }
}
