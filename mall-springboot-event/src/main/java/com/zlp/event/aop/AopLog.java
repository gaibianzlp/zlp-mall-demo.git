package com.zlp.event.aop;


import com.alibaba.fastjson.JSON;
import com.zlp.event.dto.RedisParamDTO;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Aop 日志切面
 *
 * @date: 2022/2/25 11:11
 */
@Aspect
@Component
@Slf4j(topic = "AopLog")
public class AopLog {

    @Pointcut("@annotation(com.zlp.event.aop.MyAnnotation )")
    public void pointCut() {}





    //    环绕通知
    @Around("pointCut()")
    public Object aroundLog(ProceedingJoinPoint joinpoint) throws Exception {

        // 获取被增强的目标对象，然后获取目标对象的class
//        Class<?> targetClass = joinpoint.getTarget().getClass();
//        System.out.println("执行Around，被增强的目标类为：" + targetClass);
        // 方法名称
        String methodName = joinpoint.getSignature().getName();
        System.out.println("执行Around，目标方法名称为：" + methodName);
        // 目标方法的参数类型
        Class[] parameterTypes = ((MethodSignature) joinpoint.getSignature()).getParameterTypes();
        System.out.println("parameterTypes：" + Arrays.toString(parameterTypes));
        // 目标方法的入参
        ParamData params = new ParamData();
        Object[] args = joinpoint.getArgs();// 获得目标方法的参数
        if (args != null && args.length > 0) {

            Object object = args[0];
            String toString = object.toString();
            if (object != null) {
                if (args[0].getClass() == DefaultMultipartHttpServletRequest.class) {
                    DefaultMultipartHttpServletRequest request5 = (DefaultMultipartHttpServletRequest) joinpoint.getArgs()[0];
                    Map<String, String> resultInfo = getResultInfo(request5);
                    params.putAll(resultInfo);
                }
            }
        }
        log.info("params={}",JSON.toJSONString(params));
            log.info(JSON.toJSONString(args));
        /*for (Object arg : args) {
            System.out.println("参数：" + arg);
        }*/

        System.out.println("执行Around，方法入参为：" + Arrays.toString(args));
        MyAnnotation redisParamDTO = getServiceAnnotation(joinpoint);
        log.info("modelType={},seelpTime={}",redisParamDTO.modelType(),redisParamDTO.seelpTime());
        Object result = null;
        try {
            System.out.println("环绕通知 Before 日志记录");
            long start = System.currentTimeMillis();
            //有返回参数 则需返回值
            result = joinpoint.proceed();
            System.out.println(JSON.toJSONString(result));

//            long end = System.currentTimeMillis();
//            System.out.println("总共执行时长" + (end - start) + " 毫秒");
            System.out.println("环绕通知 After 日志记录");
        } catch (Throwable t) {
//            System.out.println("出现错误"+t.getMessage());
            throw new Exception("出现错误" + t.getMessage());
        }
        return result;
    }

    /**
     * 获取DefaultMultipartHttpServletRequest信息
     *
     * @param request
     * @return
     */
    public static Map<String, String> getResultInfo(DefaultMultipartHttpServletRequest request) {
        Map requestParams = request.getParameterMap();
        Map<String, String> params = new HashMap<String, String>();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        return params;
    }

    /**
     * 获取注解中对方法的描述信息 用于service层注解
     * @param joinPoint 切点
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public static MyAnnotation getServiceAnnotation(JoinPoint joinPoint) throws Exception {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        MyAnnotation redisParamDTO = null;
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    redisParamDTO = method.getAnnotation(MyAnnotation.class);
                    break;
                }
            }
        }
        return redisParamDTO;
    }
}



