package com.zlp.event.aop;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wangyuan02
 * @desc
 * @date 2019-05-25 09:37
 */
@Data
public class DubboResult<T> implements Serializable {
    private boolean success = false;
    private T data = null;
    private String msg = "";
    private String code = "200";

    public DubboResult() {

    }

    public static <T> DubboResult<T> success(T data) {
        DubboResult<T> r = new DubboResult<>();
        r.setData(data);
        r.setSuccess(true);
        r.setMsg("success");
        r.setCode("200");
        return r;
    }

    public static <T> DubboResult<T> success() {
        DubboResult<T> r = new DubboResult<>();
        r.setSuccess(true);
        r.setMsg("success");
        r.setCode("200");
        return r;
    }

    public static <T> DubboResult<T> fail(String msg) {
        return fail("500", msg);
    }

    public static <T> DubboResult<T> fail(String code, String msg) {
        DubboResult<T> r = new DubboResult<>();
        r.setSuccess(false);
        r.setMsg(msg);
        r.setCode(code);
        return r;
    }


    public boolean isSuccess() {
        return success;
    }

    public DubboResult<T> setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public T getData() {
        return data;
    }

    public DubboResult<T> setData(T data) {
        this.data = data;
        return this;
    }

    public String getMsg() {
        return msg;
    }


    public DubboResult<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public String getCode() {
        return code;
    }


    public DubboResult<T> setCode(String code) {
        this.code = code;
        return this;
    }

    @Override
    public String toString() {
        return "HallBaseResult{" + "success=" + success +
                ", data=" + data +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
