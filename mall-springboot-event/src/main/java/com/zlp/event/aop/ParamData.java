package com.zlp.event.aop;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.*;

/**
 * 
 * @author:ZouLiPing
 * @Description:封装请求参数
 * @time:2018年4月16日 下午4:40:22
 */
@SuppressWarnings("rawtypes")
public class ParamData extends HashMap implements Map, Serializable {


	/**参数封装*/
	Map<Object, Object> map ;
	HttpServletRequest request;

	public ParamData(Map<Object, Object> map) {
		this.map = map;
	}

	public ParamData() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (Objects.isNull(requestAttributes)) return;
		this.request = ((ServletRequestAttributes) requestAttributes).getRequest();
		Map<String, String[]> properties = request.getParameterMap();
		Map<Object, Object> returnMap = new HashMap<>();
		Iterator entries = properties.entrySet().iterator();
		Entry entry;
		String name = "";
		String value = "";
		while (entries.hasNext()) {
			entry = (Entry) entries.next();
			name = (String) entry.getKey();
			Object valueObj = entry.getValue();
			if (null == valueObj) {
				value = "";
			} else if (valueObj instanceof String[]) {
				String[] values = (String[]) valueObj;
				for (int i = 0; i < values.length; i++) {
					value = values[i] + ",";
				}
				value = value.substring(0, value.length() - 1);
			} else {
				value = valueObj.toString();
			}
			returnMap.put(name, value);
		}
		// 登录IP
		map = returnMap;
	}

	@Override
	public Object get(Object key) {
		Object obj = null;
		if (map.get(key) instanceof Object[]) {
			Object[] arr = (Object[]) map.get(key);
			obj = getObject((String) key, arr);
		} else {
			obj = map.get(key);
		}
		return obj;
	}

	private Object getObject(String key, Object[] arr) {
		Object obj;
		obj = request == null ? arr : (request.getParameter(key) == null ? arr : arr[0]);
		return obj;
	}

	public String getString(Object key) {
		Object value = get(key);
		String str = null;
		if (null != value) {
			str = String.valueOf(get(key));
		}
		return str;
	}

	public int getInt(Object key) {
		int num = 0;
		String str = getString(key);
		if (null != str) {
			num = Integer.parseInt(str);
		}
		return num;
	}

	public double getDouble(Object key) {
		double num = 0;
		String str = getString(key);
		if (null != str) {
			num = Double.parseDouble(str);
		}
		return num;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		ParamData paramData = (ParamData) o;
		return map.equals(paramData.map) && request.equals(paramData.request);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), map, request);
	}

	@Override
	public Object put(Object key, Object value) {
		return map.put(key, value);
	}

	@Override
	public Object remove(Object key) {
		return map.remove(key);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public Set entrySet() {
		return map.entrySet();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public Set keySet() {
		return map.keySet();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void putAll(Map t) {
		map.putAll(t);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Collection values() {
		return map.values();
	}

}
