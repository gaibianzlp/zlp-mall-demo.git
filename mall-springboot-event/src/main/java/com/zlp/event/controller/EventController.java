package com.zlp.event.controller;


import com.zlp.event.aop.DubboResult;
import com.zlp.event.aop.MyAnnotation;
import com.zlp.event.aop.ParamData;
import com.zlp.event.dto.RedisParamDTO;
import com.zlp.event.listener.event.CouponEvent;
import com.zlp.event.listener.support.TransactionalEventHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EventController {

    private final TransactionalEventHelper transactionalEventHelper;


    /**
     *   http://127.0.0.1:8085/hello
     * @date: 2022/6/19 20:52
     * @return: java.lang.String
     */
    @MyAnnotation(seelpTime = 10,modelType = "spu")
    @GetMapping("hello")
    public String hello(String userId,Integer age) {

        long timeMillis = System.currentTimeMillis();
        long time = System.currentTimeMillis()/1000;
        log.info("第一步：transactionalEventHelper.publish.req time={},timeMillis={},type={}",time,timeMillis, CouponEvent.Type.VERIFY);
        transactionalEventHelper.publish(
                new CouponEvent((int) time,String.valueOf(timeMillis), CouponEvent.Type.VERIFY));
        return "success";
    }

    /**
     *   http://127.0.0.1:8085/hello
     * @date: 2022/6/19 20:52
     * @return: java.lang.String
     */
    @MyAnnotation(seelpTime = 10,modelType = "spu")
    @GetMapping("hello2")
    public String hello2(RedisParamDTO redisParamDTO) {

        return "success";
    }

    @MyAnnotation(seelpTime = 10,modelType = "spu")
    @PostMapping(value = "/hello3")
    public DubboResult<RedisParamDTO> update(Integer id, Integer status) {
        System.out.println("testsfs");
        RedisParamDTO paramData = new RedisParamDTO();
        paramData.setSeelpTime(10);
        paramData.setModelType("20");

        return DubboResult.success(paramData);
    }


}
