package com.zlp.event.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class RedisParamDTO implements Serializable {

    /**
     * 延迟双删休眠时间
     */
    int seelpTime = 10;

    /**
     * 模块名称
     */
    String modelType = "";
}
