package com.zlp.event.listener.event;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author hbj
 * @date 2020/6/29 4:05 下午
 */
@Getter
@Setter
@Builder
public class CouponEvent extends ApplicationEvent {
    private Integer couponId;
    private String orderCode;
    private Type type;

    public CouponEvent(Integer couponId, String orderCode, Type type) {
        super(couponId);
        this.couponId = couponId;
        this.orderCode = orderCode;
        this.type = type;
    }

    public enum Type {
        /**
         * 核销
         */
        VERIFY
    }
}
