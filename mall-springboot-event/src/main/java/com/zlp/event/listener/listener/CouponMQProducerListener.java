package com.zlp.event.listener.listener;

import com.alibaba.fastjson.JSON;
import com.zlp.event.listener.event.CouponEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author hbj
 * @date 2020/6/29 4:05 下午
 */
@Slf4j
@Component
public class CouponMQProducerListener {


    @Async
    @EventListener
    public void onEvent(CouponEvent event) {
        log.info("第三步：onEvent.req event={}", JSON.toJSONString(event));
        Integer couponId = event.getCouponId();
        String orderCode = event.getOrderCode();
        CouponEvent.Type type = event.getType();
        if (CouponEvent.Type.VERIFY.equals(type)) {
            sendMemberTraceMsg(couponId, orderCode);
        }
    }

    private void sendMemberTraceMsg(Integer couponId, String orderCode) {

        log.info("sendMemberTraceMsg.req couponId={},orderCode={}",couponId,orderCode);
    }
}
