package com.zlp.event.listener.support;


import com.alibaba.fastjson.JSON;
import com.zlp.event.listener.event.CouponEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.Assert;
@Slf4j
@Component
public class TransactionalEventHelper {

    @Autowired
    private ApplicationContext applicationContext;



    /**
     * 事件发布
     * @param couponEvent
     * @date: 2022/6/19 20:44
     * @return: void
     */
    public void publish(CouponEvent couponEvent) {
        log.info("第二步：publish.req event={}", JSON.toJSONString(couponEvent));
        Assert.notNull(couponEvent, "couponEvent must not null");
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            log.info("TransactionSynchronizationManager.isSynchronizationActive()={}",TransactionSynchronizationManager.isSynchronizationActive());
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                @Override
                public void afterCommit() {
                    applicationContext.publishEvent(couponEvent);
                }
            });
        } else {
            applicationContext.publishEvent(couponEvent);
        }
    }


}
