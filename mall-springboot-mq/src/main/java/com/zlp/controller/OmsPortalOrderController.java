package com.zlp.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zlp.dto.OrderParamDTO;
import com.zlp.service.OmsPortalOrderService;

/**
 * 订单管理Controller
 * @author ZouLiPing
 * @since 2022-4-21 17:01:02
 */
@RestController
@RequestMapping("/order")
@Api(tags = "OmsPortalOrderController", value = "订单管理")
public class OmsPortalOrderController {

    @Autowired
    private OmsPortalOrderService portalOrderService;

    @ApiOperation("根据购物车信息生成订单")
    @PostMapping(value = "/generateOrder")
    public Object generateOrder(@RequestBody OrderParamDTO orderParam) {
        return portalOrderService.generateOrder(orderParam);
    }
}
