package com.zlp.service;

import org.springframework.transaction.annotation.Transactional;
import com.zlp.common.api.CommonResult;
import com.zlp.dto.OrderParamDTO;

/**
 * 前台订单管理Service
 * Created by macro on 2018/8/30.
 */
public interface OmsPortalOrderService {

    /**
     * 根据提交信息生成订单
     */
    @Transactional
    CommonResult generateOrder(OrderParamDTO orderParam);

    /**
     * 取消单个超时订单
     */
    @Transactional
    void cancelOrder(Long orderId);
}
