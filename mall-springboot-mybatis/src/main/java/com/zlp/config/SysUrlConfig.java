package com.zlp.config;


import lombok.Data;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(SysUrlProperties.class)
@Data
public class SysUrlConfig {

}
