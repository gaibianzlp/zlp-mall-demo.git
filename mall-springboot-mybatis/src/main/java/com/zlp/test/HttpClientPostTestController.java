package com.zlp.test;

import com.alibaba.fastjson.JSON;
import com.zlp.common.api.CommonResult;
import com.zlp.config.SysUrlProperties;
import com.zlp.domian.entity.PmsBrand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Slf4j
@RestController
public class HttpClientPostTestController {

    @Resource
    private RestTemplate restTemplate;

/*    @Value("${framework.org-sync-url}")
    private String orgSyncUrl;*/

    @Autowired
    private SysUrlProperties sysUrlProperties;

    private final static String testKey = "LSadsfdslfm";
    private final static String testSecret = "LSerkfdksvdsf";
    private final static int intent = 101;
    private final static String code = "2asfd5f69699532325686";
    private final static String number = "665533255555";
    private final static String salt = "10";
//    @GetMapping("test")
    public CommonResult test() {

//        Map body = new HashMap();
////        body.put("1","1");
////        body.put("2",2);
////        String jsonData = JSON.toJSONString(body);
////        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();        map.add("1", 1);        map.add("2", "2");        map.add("3", "3");        HttpHeaders headers = new HttpHeaders();        headers.setContentType(MediaType.APPLICATION_JSON);        HttpEntity<String> request = new HttpEntity<>(jsonData, headers);        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(Url + "/rack").queryParams(map);        ResponseEntity<JSONObject> responseEntity = template.postForEntity(builder.toUriString(), request, JSONObject.class);        System.out.println(builder.toUriString());
////————————————————
////        版权声明：本文为CSDN博主「如果丶可以坑」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
////        原文链接：https://blog.csdn.net/qq_35642849/article/details/103821900
////        return commonResult;
        return null;
    }
    @RequestMapping(value = "/create1", method = RequestMethod.POST)
    public CommonResult getObj(@RequestBody PmsBrand pmsBrand){
        String jsonData = JSON.toJSONString(pmsBrand);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE));
//        Map<String, Object> param = new HashMap<String, Object>();
//        param.put("参数1", "值1");
//        param.put("参数2", "值2");
        HttpEntity<String> httpEntity = new HttpEntity<>(jsonData,httpHeaders);

        final String url = "http://127.0.0.1:8081/brand/create";
        ResponseEntity<CommonResult> commonResultResponseEntity = restTemplate.postForEntity(url, httpEntity, CommonResult.class);
        return commonResultResponseEntity.getBody();
    }


    public static void main(String[] args) {
        String url = "http://127.0.0.1:8081/brand/list";
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("pageNum","1");
        reqMap.put("pageSize","10");
        System.out.println(getUrlAppendParm(url, reqMap));

    }

    private static String getUrlAppendParm(String url, Object object) {
        StringBuffer stringBuffer = new StringBuffer(url);
        if (object instanceof Map) {
            Iterator iterator = ((Map) object).entrySet().iterator();
            if (iterator.hasNext()) {
                stringBuffer.append("?");
                Object element;
                while (iterator.hasNext()) {
                    element = iterator.next();
                    Map.Entry<String, Object> entry = (Map.Entry) element;
                    //过滤value为null，value为null时进行拼接字符串会变成 "null"字符串
                    if (entry.getValue() != null) {
                        stringBuffer.append(element).append("&");
                    }
                    url = stringBuffer.substring(0, stringBuffer.length() - 1);
                }
            }
        } else {
            throw new RuntimeException("url请求:" + url + "请求参数有误不是map类型");
        }
        log.info("url请求:" + url);
        return url;
    }


}
