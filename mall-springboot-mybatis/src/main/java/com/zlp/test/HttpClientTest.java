package com.zlp.test;

import com.zlp.utils.HttpClientResult;
import com.zlp.utils.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;

public class HttpClientTest {
    public static void main(String[] args) throws Exception {
//        127.0.0.1:8081/brand/list?pageNum=1&pageSize=10
        String url = "http://127.0.0.1:8081/brand/list";
//        URL url = new URL(url);
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("pageNum","1");
        reqMap.put("pageSize","10");
//        String var = doPost(url, reqMap);
        String var = doPut(url, reqMap);
    }

    public static String doPost(String url, Map<String, Object> reqMap) throws Exception {

        Map<String, String> headerMap = new HashMap<>(7);
        headerMap.put("Content-Type", "application/json;charset=UTF-8");
        HttpClientResult httpClientResult = HttpClientUtil.doPost(url, headerMap, reqMap);
        int code = httpClientResult.getCode();
        if(code == 200){
            String content = httpClientResult.getContent();
            return content;
        }
        return null;
    }

    public static String doPut(String url, Map<String, Object> reqMap) throws Exception {

        Map<String, String> headerMap = new HashMap<>(7);
        headerMap.put("Content-Type", "application/json;charset=UTF-8");
        HttpClientResult httpClientResult = HttpClientUtil.doPut(url,reqMap);
        int code = httpClientResult.getCode();
        if(code == 200){
            String content = httpClientResult.getContent();
            return content;
        }
        return null;
    }


}
