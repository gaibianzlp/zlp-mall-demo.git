package com.zlp.test;

import com.alibaba.fastjson.JSON;
import com.zlp.common.api.CommonResult;
import com.zlp.config.SysUrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Slf4j
@RestController
public class HttpClientTestController {

    @Resource
    private RestTemplate restTemplate;

/*    @Value("${framework.org-sync-url}")
    private String orgSyncUrl;*/

    @Autowired
    private SysUrlProperties sysUrlProperties;

    private final static String testKey = "LSadsfdslfm";
    private final static String testSecret = "LSerkfdksvdsf";
    private final static int intent = 101;
    private final static String code = "2asfd5f69699532325686";
    private final static String number = "665533255555";
    private final static String salt = "10";
    @GetMapping("test")
    public CommonResult test() {

        log.info("call test.");
//        final String url = "http://127.0.0.1:8081/brand/list";
//        final String url = orgSyncUrl;
        final String url = sysUrlProperties.getOrgSyncUrl();
        System.out.println(sysUrlProperties.getOrgSyncUrl());

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("key", testKey);
        requestHeaders.add("intent", String.valueOf(intent));
        requestHeaders.add("number", number);
        requestHeaders.add("code", code);
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("pageNum","1");
        reqMap.put("pageSize","10");
        HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(reqMap, requestHeaders);
        ResponseEntity<String > response = restTemplate.exchange(url+"?pageSize={pageSize}", HttpMethod.GET, requestEntity, String.class,reqMap);
        System.out.println(response.getStatusCode());




//        Object data = response.getBody().getData();
        String body = response.getBody();
        CommonResult commonResult = JSON.parseObject(body, CommonResult.class);
        Object data = commonResult.getData();


//        System.out.println(body.getData());
//        String body = response.getBody();
//        String sttr = body;
//        CommonResult<CommonPage<PmsBrand>>
//        PmsBrand pmsBrand = JSON.parseObject(body, PmsBrand.class);
//        JSONObject jsonObject = new JSONObject(sttr);
//        JSONObject json = JSONObject.fromObject(str);
//        sttr
        String s;
//        JSONObject jsonObject = JSONObject.parseObject(sttr);
//        jsonObject.get("data");
//        System.out.println(jsonObject.get("data"));
//        log.info("sttr="+sttr);
//        return sttr;
        return commonResult;
    }

    public static void main(String[] args) {
        String url = "http://127.0.0.1:8081/brand/list";
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("pageNum","1");
        reqMap.put("pageSize","10");
        System.out.println(getUrlAppendParm(url, reqMap));

    }

    private static String getUrlAppendParm(String url, Object object) {
        StringBuffer stringBuffer = new StringBuffer(url);
        if (object instanceof Map) {
            Iterator iterator = ((Map) object).entrySet().iterator();
            if (iterator.hasNext()) {
                stringBuffer.append("?");
                Object element;
                while (iterator.hasNext()) {
                    element = iterator.next();
                    Map.Entry<String, Object> entry = (Map.Entry) element;
                    //过滤value为null，value为null时进行拼接字符串会变成 "null"字符串
                    if (entry.getValue() != null) {
                        stringBuffer.append(element).append("&");
                    }
                    url = stringBuffer.substring(0, stringBuffer.length() - 1);
                }
            }
        } else {
            throw new RuntimeException("url请求:" + url + "请求参数有误不是map类型");
        }
        log.info("url请求:" + url);
        return url;
    }


}
