package com.zlp.test.weixin;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public  class WXShareUtil {
//    private static Props props = new Props("wei-xin.properties", "utf-8");
//    private static String appId = props.getStr("app_id");

    private final static String APPID = "wxfc6b50892370584a";
    private final static String APPSECRET = "96ef177a7fd27b37a5d7651a1116583f";

    /** 
     * 根据 ticket 和 要分享的页面url地址获取签名
     * @param jsapi_ticket
     * @param url
     * @date: 2020/7/24 13:41
     * @return: java.util.Map<java.lang.String,java.lang.Object> 
     */
    public static Map<String, Object> sign(String jsapi_ticket,String url) {
        Map<String, Object> ret = new HashMap<String, Object>();
        //生成签名的随机串
        String nonce_str = create_nonce_str();
        // 生成签名的时间戳 
        String timestamp = create_timestamp();
        String string1;
        // 签名
        String signature = "";
        // 注意这里参数名必须全部小写，且必须有序
        string1 = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + nonce_str
                + "&timestamp=" + timestamp + "&url=" + url;
        //System.out.println(string1+"==================================");
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(string1.getBytes("UTF-8"));
            signature = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ret.put("url", url);
        ret.put("jsapi_ticket", jsapi_ticket);
        ret.put("nonceStr", nonce_str);
        ret.put("timestamp", timestamp);
        ret.put("signature", signature);
        ret.put("appid",APPID);
        return ret;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    private static String create_nonce_str() {
        return UUID.randomUUID().toString();
    }

    private static String create_timestamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }

    /**
     * 获access_token
     *
     * @return
     */
    public static String getAccessToken(String appId,String appSecret) {

        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
                + appId + "&secret=" + appSecret;

        log.info("request user info from url: {" + url + "}");
        JsonObject accessTokenInfo = null;
        try {

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            String response = EntityUtils.toString(httpEntity, "utf-8");
            //LOGGER.error("获取Token-------------------"+response);
            Gson token_gson = new Gson();
            accessTokenInfo = token_gson.fromJson(response, JsonObject.class);
//            return accessTokenInfo.get("access_token").toString();
            return accessTokenInfo.get("access_token").toString().replaceAll("\"", "");

        } catch (Exception ex) {
            log.error("fail to request wechat user info. [error={" + ex + "}]");
        }
        return "";
    }


    /**
     * 获取jsapi_ticket
     *
     * @return
     */
    public static Map<String, String> getJsApiTicket(String accessToken) {
        Map<String, String> data = new HashMap();
        String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + accessToken + "&type=jsapi";
        log.info("request user info from url: {" + url + "}");
        JsonObject jsApiTicket = null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            String response = EntityUtils.toString(httpEntity, "utf-8");
            Gson token_gson = new Gson();
            jsApiTicket = token_gson.fromJson(response, JsonObject.class);
            log.info("get accessTokenInfo success. [result={" + jsApiTicket + "}]");
            data.put("ticket", jsApiTicket.get("ticket").toString().replaceAll("\"", ""));
            data.put("expires_in", jsApiTicket.get("expires_in").toString().replaceAll("\"", ""));
        } catch (Exception ex) {
            log.error("fail to request wechat user info. [error={" + ex + "}]");
        }
        return data;
    }

    public static void main(String[] args) {
        String accessToken = getAccessToken(APPID, APPSECRET);
        System.out.println("access_token ==>"+getAccessToken(APPID,APPSECRET));
        System.out.println("===========getJsApiTicket===========>");
        for (Map.Entry<String, String> stringStringEntry : getJsApiTicket(accessToken).entrySet()) {
            System.out.println(stringStringEntry);
        }

        
    }

}
