package com.zlp.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ClassName MyBatisPlusConfig
 * @Description TODO
 * @Author Engineer_Song
 * @Date 2020/4/23
 * @Version 1.0
 */
@MapperScan("com.zlp.mapper")
@Configuration
@EnableTransactionManagement
public class MyBatisPlusConfig {
    /**
     * 注册乐观锁组件
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor(){
        return new OptimisticLockerInterceptor();
    }

    /**
     * 注册分页组件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    // 新版本逻辑删除直接在yml中配置
    // 新版本性能插件还不会使用
}
