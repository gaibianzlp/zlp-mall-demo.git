package com.zlp.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @description: 实现元对象处理器接口,用于实现时间的自动填充
 * @date: 2020/9/7 16:44
 **/
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        // 这里的 createTime 填写需要生成的字段名称
        setFieldValByName("createTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        // 这里的 updateTime 填写需要生成的字段名称
        setFieldValByName("updateTime", new Date(), metaObject);
    }
}
