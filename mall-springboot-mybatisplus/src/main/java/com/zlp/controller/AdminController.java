package com.zlp.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlp.entity.Admin;
import com.zlp.mapper.AdminMapper;
import com.zlp.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 后台用户表 前端控制器
 * </p>
 *  // https://blog.csdn.net/j1231230/article/details/108022859
 * @author SurRen
 * @since 2021-02-02
 */
@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("select")
    public List<Admin> select(){

        List<Admin> adminList ;
        String columns = "id, username, password, icon, email, nick_name," +
                "create_time, login_time, status";
        Wrapper<Admin> queryWrapper = new QueryWrapper<>(new Admin()).
//                eq("status",1). // stats = 1
                select(columns).
                between("id",1,100).
//                like("nick_name","系统"). // nick_name like "%系统%"
                ne("username","test"); // 不等于 <>
        adminList = adminService.list(queryWrapper);
        return adminList;

    }

    @GetMapping("insert")
    public Admin insert(){

        Admin admin = new Admin();
        admin.setUsername("change");
        admin.setPassword("123456");
        admin.setIcon("小图片");
        admin.setEmail("865391093@qq.com");
        admin.setNickName("changes");
        admin.setNote("this is notes");
        admin.setLoginTime(new Date());
        admin.setStatus(1);
        adminService.save(admin);
        return admin;

    }

}

