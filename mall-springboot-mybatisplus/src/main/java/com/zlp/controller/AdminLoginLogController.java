package com.zlp.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 后台用户登录日志表 前端控制器
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-05
 */
@RestController
@RequestMapping("/adminLoginLog")
public class AdminLoginLogController {

}

