package com.zlp.controller;


import com.zlp.config.api.Pager;
import com.zlp.config.api.Result;
import com.zlp.dto.AdminResp;
import com.zlp.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 自定义分页查询
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-05
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/page")
@Api(value = "PageController", tags = "分页模块")
public class PageController {

    private final AdminService adminService;

    @GetMapping("getAdminPage")
    @ApiOperation(value = "获取用户分页列表")
    public Result<Pager<AdminResp>> getAdminPage(
            @RequestParam(value="pageNumber",defaultValue = "1") @ApiParam(name="pageNumber",value="当前第几页(从第一页开始)",required = true) Integer pageNumber,
            @RequestParam(value="pageSize",defaultValue = "10") @ApiParam(name="pageSize",value="每页多少条",required = true) Integer pageSize,
            @RequestParam(value="roleName",required = false) @ApiParam(name="roleName",value="角色名称") String roleName
    ){

        return Result.success(adminService.getAdminPage(pageNumber,pageSize,roleName));
    }

    @GetMapping("getAdminPageByWraaper")
    @ApiOperation(value = "获取用户分页列表ByWraaper")
    public Result<Pager<AdminResp>> getAdminPageByWraaper(
            @RequestParam(value="pageNumber",defaultValue = "1") @ApiParam(name="pageNumber",value="当前第几页(从第一页开始)",required = true) Integer pageNumber,
            @RequestParam(value="pageSize",defaultValue = "10") @ApiParam(name="pageSize",value="每页多少条",required = true) Integer pageSize,
            @RequestParam(value="roleName",required = false) @ApiParam(name="roleName",value="角色名称") String roleName
    ){

        return Result.success(adminService.getAdminPageByWraaper(pageNumber,pageSize,roleName));
    }

}

