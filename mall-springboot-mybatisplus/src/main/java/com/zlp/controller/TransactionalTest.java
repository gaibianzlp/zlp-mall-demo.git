package com.zlp.controller;

import com.zlp.entity.Admin;
import com.zlp.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transactional")
public class TransactionalTest {

    @Autowired
    private AdminService adminService;

    /** 
     * 获取用户列表信息
     * @date: 2021/2/3 10:20
     * @return: java.util.List<com.zlp.entity.Admin> 
     */
    @GetMapping("/getUserList")
    public List<Admin> getUserList() {
        
        return adminService.getUserList();
    }

    /**
     * 添加用户
     * @date: 2021/2/3 10:20
     * @return: java.util.List<com.zlp.entity.Admin>
     */
    @GetMapping("/insertUser")
    public int insertUser() {
        int counter = 1;
        adminService.insertUser();
        return counter;
    }
}
