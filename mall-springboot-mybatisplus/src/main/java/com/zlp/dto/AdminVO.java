package com.zlp.dto;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class AdminVO implements Serializable{


    private static final long serialVersionUID = 1L;

    private Long userId;

    private String username;

    private String password;

    /**
     * 头像
     */
    private String icon;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 备注信息
     */
    private String note;


    private Date createTime;

    /**
     * 最后登录时间
     */
    private Date loginTime;

    /**
     * 帐号启用状态：0->禁用；1->启用
     */
    private Integer status;

}
