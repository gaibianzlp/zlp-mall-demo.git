package com.zlp.mapper;

import com.zlp.entity.AdminLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户登录日志表 Mapper 接口
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-05
 */
public interface AdminLoginLogMapper extends BaseMapper<AdminLoginLog> {

}
