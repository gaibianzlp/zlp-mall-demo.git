package com.zlp.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.zlp.dto.AdminResp;
import com.zlp.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 *
 * @author SurRen
 * @since 2021-02-02
 */
public interface AdminMapper extends BaseMapper<Admin> {


    /** 
     * 自定义分页查询
     * @param page
     * @param roleName
     * @date: 2021/12/10 15:04
     * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.zlp.dto.AdminResp> 
     */
    IPage<AdminResp> pageData(IPage<AdminResp> page, String roleName);

    /** 
     * Wrapper 包装类查询条件
     * @param page
     * @param queryWrapper
     * @date: 2021/12/10 15:03
     * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.zlp.dto.AdminResp>
     */
    IPage<AdminResp> getAdminPageByWraaper(IPage<AdminResp> page, @Param(Constants.WRAPPER) QueryWrapper<AdminResp> queryWrapper);
}
