package com.zlp.mapper;

import com.zlp.entity.MemberLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员等级表 Mapper 接口
 * </p>
 *
 * @author liping.zou
 * @since 2021-02-03
 */
public interface MemberLevelMapper extends BaseMapper<MemberLevel> {

}
