package com.zlp.mapper;

import com.zlp.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户角色表 Mapper 接口
 * </p>
 *
 * @author liping.zou
 * @since 2021-12-10
 */
public interface RoleMapper extends BaseMapper<Role> {

}
