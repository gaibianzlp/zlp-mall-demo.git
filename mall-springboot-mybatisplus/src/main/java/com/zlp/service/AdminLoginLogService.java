package com.zlp.service;

import com.zlp.entity.AdminLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户登录日志表 服务类
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-05
 */
public interface AdminLoginLogService extends IService<AdminLoginLog> {

}
