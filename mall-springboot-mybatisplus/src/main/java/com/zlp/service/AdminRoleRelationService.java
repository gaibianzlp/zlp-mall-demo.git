package com.zlp.service;

import com.zlp.entity.AdminRoleRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户和角色关系表 服务类
 * </p>
 *
 * @author liping.zou
 * @since 2021-12-10
 */
public interface AdminRoleRelationService extends IService<AdminRoleRelation> {

}
