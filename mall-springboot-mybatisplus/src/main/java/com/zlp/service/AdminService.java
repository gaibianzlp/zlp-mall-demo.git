package com.zlp.service;

import com.zlp.config.api.Pager;
import com.zlp.dto.AdminResp;
import com.zlp.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author SurRen
 * @since 2021-02-02
 */
public interface AdminService extends IService<Admin> {

    /**
     * 获取用户列表信息
     * @date: 2021/2/3 10:20
     * @return: java.util.List<com.zlp.entity.Admin>
     */
    List<Admin> getUserList();

    
    /**
     * 添加用户
     * @date: 2021/2/3 11:30
     * @return: void 
     */
    void insertUser();


    /**
     * 获取用户分页列表
     * @param pageNumber
     * @param pageSize
     * @param roleName
     * @date: 2021/12/10 11:14
     * @return: java.lang.Object
     */
    Pager<AdminResp> getAdminPage(Integer pageNumber, Integer pageSize, String roleName);

    /**
     * 获取用户分页列表ByWraaper
     * @param pageNumber
     * @param pageSize
     * @param roleName
     * @date: 2021/12/10 14:40
     * @return: com.zlp.config.api.Pager<com.zlp.dto.AdminResp>
     */
    Pager<AdminResp> getAdminPageByWraaper(Integer pageNumber, Integer pageSize, String roleName);
}
