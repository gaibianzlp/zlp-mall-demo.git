package com.zlp.service;

import com.zlp.entity.MemberLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员等级表 服务类
 * </p>
 *
 * @author liping.zou
 * @since 2021-02-03
 */
public interface MemberLevelService extends IService<MemberLevel> {

}
