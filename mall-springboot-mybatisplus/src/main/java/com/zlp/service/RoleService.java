package com.zlp.service;

import com.zlp.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户角色表 服务类
 * </p>
 *
 * @author liping.zou
 * @since 2021-12-10
 */
public interface RoleService extends IService<Role> {

}
