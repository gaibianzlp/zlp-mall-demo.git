package com.zlp.service.impl;

import com.zlp.entity.AdminLoginLog;
import com.zlp.mapper.AdminLoginLogMapper;
import com.zlp.service.AdminLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户登录日志表 服务实现类
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-05
 */
@Service
public class AdminLoginLogServiceImpl extends ServiceImpl<AdminLoginLogMapper, AdminLoginLog> implements AdminLoginLogService {

}
