package com.zlp.service.impl;

import com.zlp.entity.AdminRoleRelation;
import com.zlp.mapper.AdminRoleRelationMapper;
import com.zlp.service.AdminRoleRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户和角色关系表 服务实现类
 * </p>
 *
 * @author liping.zou
 * @since 2021-12-10
 */
@Service
public class AdminRoleRelationServiceImpl extends ServiceImpl<AdminRoleRelationMapper, AdminRoleRelation> implements AdminRoleRelationService {

}
