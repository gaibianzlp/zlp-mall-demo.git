package com.zlp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlp.config.api.Pager;
import com.zlp.dto.AdminResp;
import com.zlp.entity.Admin;
import com.zlp.entity.AdminLoginLog;
import com.zlp.mapper.AdminMapper;
import com.zlp.service.AdminLoginLogService;
import com.zlp.service.AdminService;
import com.zlp.utils.MyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 后台用户表 服务实现类
 * </p>
 *
 * @author SurRen
 * @since 2021-02-02
 */
@Service
@Slf4j(topic = "AdminServiceImpl")
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Resource
    private AdminLoginLogService adminLoginLogService;

    @Resource
    private AdminMapper adminMapper;


    @Override
    public List<Admin> getUserList() {

        return this.list(new QueryWrapper<>(new Admin()).eq("status",1));
    }
    /**
     * TransactionDefinition.PROPAGATION_SUPPORTS：
     *      如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。
     * TransactionDefinition.PROPAGATION_NOT_SUPPORTED：
     *      以非事务方式运行，如果当前存在事务，则把当前事务挂起。
     * TransactionDefinition.PROPAGATION_NEVER：
     *      以非事务方式运行，如果当前存在事务，则抛出异常。
     */
    @Override
//    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    @Transactional(rollbackFor = MyException.class)
    public void insertUser() {
        /*try {
            Admin admin = new Admin();
            admin.setUsername("smile");
            admin.setPassword("123456567");
            admin.setIcon("icon090807");
            admin.setEmail("865391094@qq.com");
            admin.setNickName("liping.zou");
            admin.setNote("this is my life !!!");
            admin.setLoginTime(new Date());
            admin.setStatus(1);
            this.save(admin);
            insertLoginLog(admin.getId());
            int count = 1 / 0;
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        Admin admin = new Admin();
        admin.setUsername("smile");
        admin.setPassword("123456567");
        admin.setIcon("icon090807");
        admin.setEmail("865391094@qq.com");
        admin.setNickName("liping.zou");
        admin.setNote("this is my life !!!");
        admin.setLoginTime(new Date());
        admin.setStatus(1);
        this.save(admin);
        insertLoginLog(admin.getId());
        int count = 1 / 0;

    }

    @Override
    public Pager<AdminResp> getAdminPage(Integer pageNumber, Integer pageSize, String roleName) {

        log.info("getAdminPage.req pageNumber={},pageSize={},roleName={}",pageNumber,pageSize,roleName);
        IPage<AdminResp> page = new Page<>(pageNumber, pageSize);

        IPage<AdminResp> pageData = adminMapper.pageData(page,roleName);
        if (CollectionUtils.isNotEmpty(pageData.getRecords())) {
            return new Pager<>(pageNumber,pageSize,(int)pageData.getPages(),pageData.getTotal(),pageData.getRecords());
        }
        return new Pager<>();
    }

    @Override
    public Pager<AdminResp> getAdminPageByWraaper(Integer pageNumber, Integer pageSize, String roleName) {

        log.info("getAdminPage.req pageNumber={},pageSize={},roleName={}",pageNumber,pageSize,roleName);
        IPage<AdminResp> page = new Page<>(pageNumber, pageSize);

        QueryWrapper<AdminResp> queryWrapper = Wrappers.query(new AdminResp());
        if (StringUtils.isNotBlank(roleName)) {
            queryWrapper.like("ua.nick_name",roleName).or()
                    .like("ur.`name`",roleName);
        }
        IPage<AdminResp> pageData = adminMapper.getAdminPageByWraaper(page,queryWrapper);
        if (CollectionUtils.isNotEmpty(pageData.getRecords())) {
            return new Pager<>(pageNumber,pageSize,(int)pageData.getPages(),pageData.getTotal(),pageData.getRecords());
        }
        return new Pager<>();
    }

    @Transactional(rollbackFor = Exception.class)
    public void insertLoginLog(Long id) {

        AdminLoginLog adminLoginLog = new AdminLoginLog();
        adminLoginLog.setAdminId(id);
        adminLoginLog.setIp("127.0.0.1");
        adminLoginLog.setAddress("浦东新区 天河机场");
        adminLoginLog.setUserAgent("谷歌Agent");
        adminLoginLogService.save(adminLoginLog);
    }
}
