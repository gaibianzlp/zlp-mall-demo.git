package com.zlp.service.impl;

import com.zlp.entity.MemberLevel;
import com.zlp.mapper.MemberLevelMapper;
import com.zlp.service.MemberLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员等级表 服务实现类
 * </p>
 *
 * @author liping.zou
 * @since 2021-02-03
 */
@Service
public class MemberLevelServiceImpl extends ServiceImpl<MemberLevelMapper, MemberLevel> implements MemberLevelService {

}
