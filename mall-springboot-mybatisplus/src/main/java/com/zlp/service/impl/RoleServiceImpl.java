package com.zlp.service.impl;

import com.zlp.entity.Role;
import com.zlp.mapper.RoleMapper;
import com.zlp.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户角色表 服务实现类
 * </p>
 *
 * @author liping.zou
 * @since 2021-12-10
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
