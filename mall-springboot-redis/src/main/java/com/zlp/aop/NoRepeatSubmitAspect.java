package com.zlp.aop;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.zlp.common.CommonResult;
import com.zlp.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Map;

@Slf4j
@Aspect
@Component
public class NoRepeatSubmitAspect {

    private static Gson gson = new Gson();

    private static final String SUFFIX = "SUFFIX";

    private static final String NO_REPEAT_LOCK_PREFIX = "REPEAT_LOCK";

    @Autowired
    RedisService redisService;

    /**
     * 横切点
     */
    @Pointcut("@annotation(noRepeatSubmit)")
    public void repeatPoint(NoRepeatSubmit noRepeatSubmit) {
    }

    /**
     *  接收请求，并记录数据
     */
    @Around(value = "repeatPoint(noRepeatSubmit)")
    public Object doBefore(ProceedingJoinPoint joinPoint, NoRepeatSubmit noRepeatSubmit) {

        String key = NO_REPEAT_LOCK_PREFIX +":"+ noRepeatSubmit.location();
        // 获取所有的请求参数
        Object[] args = joinPoint.getArgs();
        String name = noRepeatSubmit.name();
        int argIndex = noRepeatSubmit.argIndex();
        String suffix;
        if (StringUtils.isEmpty(name)) {
            suffix = String.valueOf(args[argIndex]);
        } else {
            Map<String, Object> keyAndValue = getKeyAndValue(args[argIndex]);
            Object valueObj = keyAndValue.get(name);
            if (valueObj == null) {
                suffix = SUFFIX;
            } else {
                suffix = String.valueOf(valueObj);
            }
        }
        key = key + ":" + suffix;
        // 打印请求参数
        for (Object arg : args) {
            log.info(gson.toJson(arg));
        }
        int seconds = noRepeatSubmit.seconds();
        String value = redisService.getLock(key, seconds);
        log.info("lock key={} value={} ",key,value);
        if (StringUtils.isEmpty(value)) {
            return CommonResult.failed("操作过于频繁，请稍后重试");
        }
        try {
            Object proceed = joinPoint.proceed();
            return proceed;
        } catch (Throwable throwable) {
            log.error("运行业务代码出错", throwable);
            throw new RuntimeException(throwable.getMessage());
        } finally {
            redisService.unLock(key,value);
        }
    }

    public static Map<String, Object> getKeyAndValue(Object obj) {
        Map<String, Object> map = Maps.newHashMap();
        // 得到类对象
        Class userCla = (Class) obj.getClass();
        /* 得到类中的所有属性集合 */
        Field[] fs = userCla.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            Field f = fs[i];
            // 设置些属性是可以访问的
            f.setAccessible(true);
            Object val;
            try {
                val = f.get(obj);
                // 得到此属性的值
                // 设置键值
                map.put(f.getName(), val);
            } catch (IllegalArgumentException e) {
                log.error("getKeyAndValue IllegalArgumentException", e);
            } catch (IllegalAccessException e) {
                log.error("getKeyAndValue IllegalAccessException", e);
            }

        }
        log.info("扫描结果：" + gson.toJson(map));
        return map;
    }
}
