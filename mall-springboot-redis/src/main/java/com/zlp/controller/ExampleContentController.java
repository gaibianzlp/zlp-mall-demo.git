package com.zlp.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.zlp.dto.ContentRecommendCache;
import com.zlp.service.RedisService;
import com.zlp.util.RedisConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("example/content")
@Slf4j(topic = "ExampleContentController")
@Api(value = "content", tags = "案例-文章内容模块-(V1.2)")
public class ExampleContentController {


    @Autowired
    private RedisService redisService;


    /**
     * 取消文章点赞
     *
     * @param contentId 文章ID
     * @param userId    用户ID
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long
     */
    @GetMapping("commentUnUp")
    @ApiOperation(value = "取消文章点赞")
    public Boolean commentUnUp(@RequestParam("contentId") Long contentId,
                             @RequestParam("userId") Long userId) {

        log.info("commentUnUp.req contentId={},userId={}", contentId, userId);
        String key = RedisConstants.CONTENT + ":" + RedisConstants.UP + ":" + userId;
        String lockKey = "commentUnUp";
        String lockValue = redisService.getLock(lockKey, 10);
        try {
            if (StringUtils.isNotBlank(lockValue)) {
                // 1. 判断当前用户是否对这篇文章已经点过赞
                Long rank = redisService.zRank(key, contentId.toString());
                if (Objects.isNull(rank)) {
                    return Boolean.FALSE;
                }
                redisService.zRemove(key, contentId.toString());
                // 2. 文章统计点赞数量
                String keyStatic = RedisConstants.CONTENT + ":" + RedisConstants.STATISTICS + ":" + contentId;
                redisService.hIncrBy(keyStatic,RedisConstants.UP,-1);
                // todo 异步同步数据库操作
                return Boolean.TRUE;
            }else {
                // 睡眠100s
                Thread.sleep(100L);
                commentUp(contentId,userId);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            redisService.unLock(lockKey,lockValue);
        }
        return Boolean.FALSE;
    }


    /**
     * 文章点赞
     *
     * @param contentId 文章ID
     * @param userId    用户ID
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long
     */
    @GetMapping("commentUp")
    @ApiOperation(value = "文章点赞")
    public Boolean commentUp(@RequestParam("contentId") Long contentId,
                             @RequestParam("userId") Long userId) {

        log.info("commentUp.req contentId={},userId={}", contentId, userId);
        String key = RedisConstants.CONTENT + ":" + RedisConstants.UP + ":" + userId;
        String lockKey = "commentUp";
        String lockValue = redisService.getLock(lockKey, 10);
        try {
            if (StringUtils.isNotBlank(lockValue)) {
                // 1. 判断当前用户是否对这篇文章已经点过赞
                Long rank = redisService.zRank(key, contentId.toString());
                if (Objects.nonNull(rank)) {
                    return Boolean.FALSE;
                }
                Boolean result = redisService.zAdd(key, contentId.toString(), System.currentTimeMillis());
                // 2. 文章统计点赞数量
                String keyStatic = RedisConstants.CONTENT + ":" + RedisConstants.STATISTICS + ":" + contentId;
                redisService.hIncrBy(keyStatic,RedisConstants.UP,1);
                // todo 异步同步数据库操作
                // 更新文章推荐值
                return result;
            }else {
                // 睡眠100s
                Thread.sleep(100L);
                commentUp(contentId,userId);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            redisService.unLock(lockKey,lockValue);
        }
        return Boolean.FALSE;
    }


    /**
     * 获取文章点赞列表
     *
     * @param userId    用户ID
     * @param pageIndex 第几页:默认从第1开始
     * @param pageSize  每页显示多少条
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long
     */
    @GetMapping("getUpList")
    @ApiOperation(value = "获取文章点赞列表")
    public Set<String> getUpList(
            @RequestParam("userId") Long userId,
            @RequestParam("pageIndex") Integer pageIndex,
            @RequestParam("pageSize") Integer pageSize
    ) {

        log.info("commentUp.req userId={}", userId);
        String key = RedisConstants.CONTENT + ":" + RedisConstants.UP + ":" + userId;
        // 1. 判断zSet集合里面是否存在当前这篇文章ID
        if (Objects.isNull(pageSize)) pageSize = 10;
        if (Objects.isNull(pageIndex) || pageIndex == 1 || pageIndex == 0)
            pageIndex = 0;
        else
            pageIndex = pageSize * (pageIndex-1);

        if (Objects.nonNull(pageSize))
            pageSize = (pageIndex + pageSize)-1;

        // 获取集合的元素, 根据日期从大到小排序
        Set<String> contentIds = redisService.zReverseRange(key, pageIndex, pageSize);
        if (CollectionUtil.isNotEmpty(contentIds)) {
            return contentIds;
        }
        return new LinkedHashSet<>();
    }



    @ApiOperationSupport(order = 1)
    @PostMapping("/addContent")
    @ApiOperation(value = "添加文章内容", notes = "添加文章内容")
    public boolean addContent(@Valid @RequestBody ContentRecommendCache contentRecommendCache) {

        log.info("addContent.req addContent={}", JSON.toJSONString(contentRecommendCache));
        String key = RedisConstants.COMMENT+":"+RedisConstants.RECOMMEND;
        redisService.zAdd(key,JSON.toJSONString(contentRecommendCache),contentRecommendCache.getScore()+100);
        return Boolean.TRUE;
    }


    /**
     * 推荐文章内容列表
     *
     * @param contentId 文章ID 双写数据不一致问题
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long
     */
    @GetMapping("getRecommendContentPape")
    @ApiOperation(value = "文章点赞或者取消List")
    public List<ContentRecommendCache> getRecommendContentPape(@RequestParam("contentIds") List<Long> contentIds,
                                                               @RequestParam(value="pageNumber",defaultValue = "0") Integer pageNumber,
                                                               @RequestParam(value="pageSize",defaultValue = "10")  Integer pageSize,
                                                               @RequestParam("userId") Long userId) {

        List<ContentRecommendCache> contentRecommendCaches = new ArrayList<>();
        String key = RedisConstants.COMMENT+":"+RedisConstants.RECOMMEND;
        // 查询屏蔽用户 todo userId
        // 查询感兴趣的文章 todo userId
        Set<String> conSetList = redisService.zReverseRange(key, pageNumber, pageSize);
        if (CollectionUtils.isNotEmpty(contentRecommendCaches)) {
            contentRecommendCaches = conSetList.stream().map(cache -> JSON.parseObject(cache,ContentRecommendCache.class)).collect(Collectors.toList());
        }
        return contentRecommendCaches;
    }



}
