package com.zlp.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@Slf4j
@RestController
public class GcController {

    @SneakyThrows
    @GetMapping("loop")
    public void loop(){
        while (true) {
            Thread.sleep(100L);
            log.info("loop...");
        }
    }
}
