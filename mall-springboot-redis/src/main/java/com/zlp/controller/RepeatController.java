package com.zlp.controller;

import com.zlp.aop.NoRepeatSubmit;
import com.zlp.common.CommonResult;
import com.zlp.dto.RequestVo;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RepeatController {

    /**
     *  根据请求参数里的 PathVariable 里获取的变量进行接口级别防重复点击
     *
     * @param testId 测试id
     * @param requestVo 请求参数
     * @return
     * @author daleyzou
     */
    @SneakyThrows
    @PostMapping("/test/{testId}")
    @NoRepeatSubmit(location = "thisIsTestLocation", seconds = 10,name = "userId")
    public CommonResult thisIsTestLocation(@PathVariable Integer testId, @RequestBody RequestVo requestVo) {
        // 睡眠 5 秒，模拟业务逻辑
        Thread.sleep(5);
        return CommonResult.success("test is return success");
    }
}
