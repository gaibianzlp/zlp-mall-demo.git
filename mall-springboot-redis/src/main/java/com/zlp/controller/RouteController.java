package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.service.RedisService;
import com.zlp.util.RedisUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
public class RouteController {

    private final RedisService redisService;


    /**
     * 动态路由入口
     *
     * @return
     */
    @GetMapping("getRouteDefinitions")
    public Object getRouteDefinitions() {

        Map<Object, Object> gateway_route_key = redisService.hGetAll("gateway_route_key");
        String jsonString = JSON.toJSONString(gateway_route_key);
        log.info("gateway_route_key ={}， ", jsonString);
        return jsonString;
    }
}
