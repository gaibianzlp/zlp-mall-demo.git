package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.zlp.dto.ContentCommentReq;
import com.zlp.dto.ContentRecommendCache;
import com.zlp.service.RedisService;
import com.zlp.util.DateUtil;
import com.zlp.util.RedisConstants;
import com.zlp.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("content")
@Slf4j(topic = "TestContentController")
@Api(value = "content", tags = "文章内容模块-(V1.1)")
public class TestContentController {

    // https://zhuanlan.zhihu.com/p/102585022
//    https://zhuanlan.zhihu.com/p/102585022

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisService redisService;

    @GetMapping("test")
    public String test() {
        redisUtils.set("hello", "hello world");
        return "success";
    }

    @GetMapping("test2")
    public String test2() {
        redisService.set("hello2", "hello world");
        return "success";
    }

    @GetMapping("append")
    public String append() {
        redisService.append("hello2", "hello world");
        return "success";
    }

    /**
     * 推荐文章内容列表
     * @param contentId 文章ID
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long
     */
    @GetMapping("getRecommendContentPape")
    @ApiOperation(value = "文章点赞或者取消List")
    public List<ContentRecommendCache> getRecommendContentPape(@RequestParam("contentId") Long contentId,
                                            @RequestParam(value="pageNumber",defaultValue = "0") Integer pageNumber,
                                            @RequestParam(value="pageSize",defaultValue = "10")  Integer pageSize,
                                            @RequestParam("userId") Long userId) {

        List<ContentRecommendCache> contentRecommendCaches = new ArrayList<>();
        String key = RedisConstants.COMMENT+":"+RedisConstants.RECOMMEND+":"+contentId;
        // 查询屏蔽用户 todo userId
        // 查询感兴趣的文章 todo userId
        Set<String> conSetList = redisService.zReverseRange(key, pageNumber, pageSize);
        if (CollectionUtils.isNotEmpty(contentRecommendCaches)) {
            contentRecommendCaches = conSetList.stream().map(cache -> JSON.parseObject(cache,ContentRecommendCache.class)).collect(Collectors.toList());
        }
        return contentRecommendCaches;
    }

    /**
     * 文章点赞或者取消List
     * @param contentId 文章ID
     * @param operType 操作类型 1:添加 2:取消
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long
     */
    @GetMapping("commentUpList")
    @ApiOperation(value = "文章点赞或者取消List")
    public Boolean commentUpList(@RequestParam("contentId") Long contentId,
                                 @RequestParam("operType") int operType,
                                 @RequestParam("userId") Long userId) {

        log.info("commentUp.req contentId={},operType={}",contentId,operType);
        String key = RedisConstants.CONTENT+":"+RedisConstants.UPLIST +":"+ contentId;
        // todo 添加数据库一份数据
        if (1 == operType) {
            redisService.zAdd(key,userId.toString(),System.currentTimeMillis());
            return Boolean.TRUE;
        }
        redisService.zRemove(key,userId.toString());

        // 总条数
        Long count = redisService.zSize(key);
        log.info("count={}",count);
        return Boolean.TRUE;
    }

    /** 
     * 文章点赞或者取消
     * @param contentId 文章ID
     * @param operType 操作类型 1:添加 2:取消
     * @date: 2021/1/4 15:25
     * @return: java.lang.Long 
     */
    @GetMapping("commentUp")
    @ApiOperation(value = "文章点赞或者取消")
    public Long commentUp(@RequestParam("contentId") Long contentId,
                          @RequestParam("operType") int operType ) {

        log.info("commentUp.req contentId={},operType={}",contentId,operType);
        String key = RedisConstants.CONTENT+":"+RedisConstants.UP +":"+ contentId;
        // todo 添加数据库一份数据
        if (1 == operType) {
            return redisService.incrBy(key,1);
        }
        return redisService.decrby(key,1);
    }

    /**
     * 根据文章ID获取评论详情
     * @param contentId 文章ID
     * @date: 2021/1/4 14:03
     * @return: String
     */
    @GetMapping("getCommentDetails")
    @ApiOperation(value = "获取评论详情-(V1.1)", notes = "获取评论详情-(V1.1)")
    public List<ContentCommentReq> getCommentDetails(@RequestParam("contentId") Long contentId,
                                    @RequestParam(value="pageNumber",defaultValue = "0") Integer pageNumber,
                                    @RequestParam(value="pageSize",defaultValue = "5")  Integer pageSize) {

        log.info("getCommentDetails.req contentId={},pageNumber={},pageSize={}",contentId,pageNumber,pageSize);
        List<ContentCommentReq> contentCommentReqs;
        String key = RedisConstants.COMMENT+":"+ contentId;
        List<String> commentList = redisService.lGet(key, pageNumber, pageSize);
        if (CollectionUtils.isNotEmpty(commentList)) {
            contentCommentReqs = commentList.stream().map(comment -> JSON.parseObject(comment,ContentCommentReq.class)).
                    collect(Collectors.toList());
            return contentCommentReqs;
        }
        return null;
    }

    @ApiOperationSupport(order = 1)
    @PostMapping("/commentOrReply")
    @ApiOperation(value = "添加评论/回复-(V1.1)", notes = "评论/回复-(V1.1)")
    public boolean commentOrReply(@Valid @RequestBody ContentCommentReq contentCommentReq) {

        log.info("commentOrReply.req contentCommentReq={}",JSON.toJSONString(contentCommentReq));
        String key = RedisConstants.COMMENT+":"+ contentCommentReq.getContentId();
        redisService.lSet(key,JSON.toJSONString(contentCommentReq));
        return Boolean.TRUE;
    }


    @ApiOperationSupport(order = 1)
    @PostMapping("/addContent")
    @ApiOperation(value = "添加文章内容", notes = "添加文章内容")
    public boolean addContent(@Valid @RequestBody ContentRecommendCache contentRecommendCache) {

        log.info("addContent.req addContent={}",JSON.toJSONString(contentRecommendCache));
        String key = RedisConstants.COMMENT+":"+RedisConstants.RECOMMEND+":"+contentRecommendCache.getContentId();
        redisService.zAdd(key,JSON.toJSONString(contentRecommendCache),contentRecommendCache.getScore()+100);
        return Boolean.TRUE;
    }


    /** 
     * 获取订单流水号
     * @date: 2021/1/5 10:13
     * @return: java.lang.String 
     */
    @GetMapping("/getserialNumber")
    @ApiOperation(value = "获取订单流水号", notes = "获取订单流水号")
    public String getserialNumber() {

        String key = RedisConstants.SERIAL;
        Long serial = redisService.incrBy(key, 1);
        String format = String.format("%06d", serial);
        String stringDate = DateUtil.getStringDate(new Date(), DateUtil.yyyyMMddHHmm);
        StringBuilder sb = new StringBuilder("OD");
        sb.append(stringDate).append(format);
        return sb.toString();
    }



}
