package com.zlp.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.zlp.entity.User;
import com.zlp.service.RedisService;
import com.zlp.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class TestController {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisService redisService;

    final static String PRE = "bum ";

    @GetMapping("test")
    public String test() {
        redisUtils.set("hello", "hello world");
        return "success";
    }

    @GetMapping("test2")
    public String test2() {
        redisService.set("hello2", "hello world");
        return "success";
    }

    @GetMapping("append")
    public String append() {
        redisService.append("hello2", "hello world");
        return "success";
    }

    @GetMapping("lSet")
    public String lSet() {
        User user = new User();
        user.setUserName("张三22");
        user.setAddress("浦东新区22");
        redisService.lSet("helloLset", JSON.toJSONString(user));
        return "success";
    }

    @GetMapping("lGet")
    public String lGet() {
        User user = new User();
        user.setUserName("张三22");
        user.setAddress("浦东新区22");
        List<String> helloLset = redisService.lGet("helloLset", 0, -1);
        return JSON.toJSONString(helloLset);
    }

    @GetMapping("lRemove")
    public String lRemove() {
        User user = new User();
        user.setUserName("张三22");
        user.setAddress("浦东新区22");
        redisService.lRemove("helloLset", 1, JSON.toJSONString(user));
        return "success";
    }

    @GetMapping("deleteByPrex")
    public String deleteByPrex() {
//        redisService.deleteByPrex("REPORT:*");
        redisService.deleteByPrex(PRE + StrUtil.COLON+"2"+StrUtil.COLON+"*");
        return "success";
    }

    @GetMapping("col")
    public String col() {

        String key = PRE + StrUtil.COLON+"1"+StrUtil.COLON+ UUID.randomUUID();
        String key1 = "bum "+ StrUtil.COLON+"2"+StrUtil.COLON+ UUID.randomUUID();
//        redisService.deleteByPrex("REPORT:*");
        redisService.set(key, UUID.randomUUID().toString());
        redisService.set(key1, UUID.randomUUID().toString());
        return "success";
    }


}
