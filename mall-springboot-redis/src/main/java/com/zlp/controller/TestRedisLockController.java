package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.entity.Content;
import com.zlp.service.BloomFilterStrategy;
import com.zlp.service.RedisService;
import com.zlp.util.RedisConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * redis分布式锁
 *
 * @date: 2020/12/2 14:10
 * @return: java.lang.String
 */
@RestController
@Slf4j(topic = "TestRedisLockController")
public class TestRedisLockController {

    private final static String DETAIL = "DETAIL";
    private final static String CONTENT = "CONTENT";
    private final static String LOCK = "LOCK";
    private final static String CONTENT_PREFIX = CONTENT + ":" + DETAIL;


    private AtomicInteger count = new AtomicInteger(0);

    @Resource
    private RedisService redisService;

    @Resource
    private RedissonClient redissonClient;

    @Resource(name = "contentBloomFilterStrategy")
    private BloomFilterStrategy contentBloomFilterStrategy;

   /**
    * 构建一个contentList数据容器
    */
    private static List<Content> contentList = new ArrayList<>();


    static {
        contentList.add(new Content(1L, "说好不哭", "没有联络，后来的生活！", 1));
        contentList.add(new Content(2L, "告别气球", "塞拉河畔，左岸的咖啡", 1));
        contentList.add(new Content(3L, "等你下课", "你住的学校旁，我租了一间公寓", 1));
    }


    /**
     * 获取文章详情 01-- 防止缓存击穿
     *
     * @date: 2020/12/2 14:10
     * @return: Content
     */
    @GetMapping("getContentDetail")
    public Content getContentDetail(@RequestParam(value = "contentId") Long contentId) {

        log.info("getContentDetail.req contentId={}", contentId);
        Content content;
        String detail = CONTENT + ":" + DETAIL + ":" + contentId;
        String value = redisService.get(detail);
        if (StringUtils.isNotEmpty(value)) {
            log.info("从缓存获取数据.....");
            return JSON.parseObject(value, Content.class);
        }
        content = getData(contentId);
        // 查询文章内容不空设置缓存为10min
        if (Objects.nonNull(content)) {
            redisService.setKeyByMINUTES(detail, JSON.toJSONString(content), 10);
        }
        return content;

    }


    /**
     * 模拟数据库查询数据
     *
     * @param contentId 文章ID
     * @date: 2020/12/30 10:50
     * @return: com.zlp.entity.Content
     */
    private Content getData(Long contentId) {

        log.info("getData查询数据库获取数据count={}.....", count.incrementAndGet());
        try {
            Thread.sleep(40);
            Optional<Content> con = contentList.stream().filter(content ->
                    content.getContentId().equals(contentId)).findFirst();
            if (con.isPresent()) {
                return con.get();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取文章详情 -- 防止缓存击穿
     *
     * @date: 2020/12/2 14:10
     * @return: Content
     */
    @GetMapping("getContentDetail02")
    public Content getContentDetail02(@RequestParam(value = "contentId") Long contentId) {


        log.info("getContentDetail02.req contentId={}", contentId);
        Content content;
        String detail = CONTENT + ":" + DETAIL + ":" + contentId;
        String conLock = CONTENT + ":" + LOCK + ":" + contentId;
        String lock = "";
        String value = redisService.get(detail);
        if (StringUtils.isNotEmpty(value)) {
            log.info("从缓存获取数据.....");
            return JSON.parseObject(value, Content.class);
        }
        try {
            lock = redisService.getLock(conLock, 10);
            if (StringUtils.isNotEmpty(lock)) {
                // 获取数据库数据
                content = getData(contentId);
                // 查询文章内容不为空设置缓存为10min
                if (Objects.nonNull(content)) {
                    redisService.setKeyByMINUTES(detail, JSON.toJSONString(content), 10);
                }
                // 查询文章内容为空设置缓存为1min,避免缓存穿透
                redisService.setKeyByMINUTES(detail, JSON.toJSONString(content), 1);
                return content;
            }
            // 休眠重新尝试调用方法
            Thread.sleep(300);
            getContentDetail(contentId);
        } catch (Exception e) {
            e.printStackTrace();
            redisService.unLock(detail, lock);
        } finally {
            redisService.unLock(detail, lock);
        }
        return null;
    }

    /**
     * 获取文章详情03 -- 防止缓存穿透
     *
     * @date: 2020/12/2 14:10
     * @return: Content
     */
    @GetMapping("getContentDetail03")
    public Content getContentDetail03(@RequestParam(value = "contentId") Long contentId) {


        log.info("getContentDetail03.req contentId={}", contentId);
        Content content;
        String detail = CONTENT + ":" + DETAIL + ":" + contentId;
        String conLock = CONTENT + ":" + LOCK + ":" + contentId;
        RBloomFilter<Object> bloomFilter = redissonClient.getBloomFilter(RedisConstants.CONTENT_PREFIX);
        if (Boolean.FALSE.equals(bloomFilter.contains(contentId))) {
            log.info("bloomFilter.contentId={},非法的文章ID", detail);
            return null;
        }
        String lock = "";
        String value = redisService.get(detail);
        if (StringUtils.isNotEmpty(value)) {
            log.info("从缓存获取数据.....");
            return JSON.parseObject(value, Content.class);
        }
        try {
            lock = redisService.getLock(conLock, 10);
            if (StringUtils.isNotEmpty(lock)) {
                content = getData(contentId);
                // 查询文章内容不空设置缓存为10min
                if (Objects.nonNull(content)) {
                    redisService.setKeyByMINUTES(detail, JSON.toJSONString(content), 10);
                }
                // 查询文章内容为空设置缓存为1min,避免缓存穿透
                redisService.setKeyByMINUTES(detail, JSON.toJSONString(content), 1);
                return content;
            }
            // 休眠重新尝试调用方法
            Thread.sleep(300);
            getContentDetail(contentId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            redisService.unLock(detail, lock);
        }
        return null;
    }

    /**
     * 初始化布隆过滤器数据
     *
     * @date: 2020/12/30 13:34
     * @return: void
     */
    @GetMapping("initContentBloomData")
    public void initContentBloomData() {

        log.info("初始化布隆过滤器数据==>initContentBloomData...");
        RBloomFilter<Object> bloomFilter = redissonClient.getBloomFilter(RedisConstants.CONTENT_PREFIX);
        //初始化布隆过滤器，var1表示容量大小，var3表示容错率
        bloomFilter.tryInit(1000L, 0.0001);
        for (long i = 1L; i < 1000; i++) {
            bloomFilter.add(i);
        }
        log.info("CONTENT_PREFIX:1 是否存在：" + bloomFilter.contains(1));
        log.info("CONTENT_PREFIX:2 是否存在：" + bloomFilter.contains(1002));
        log.info("预计插入数量：" + bloomFilter.getExpectedInsertions());
        log.info("容错率：" + bloomFilter.getFalseProbability());
        log.info("hash函数的个数：" + bloomFilter.getHashIterations());
        log.info("插入对象的个数：" + bloomFilter.count());
    }


    /**
     * 添加文章内容
     *
     * @param content
     * @date: 2020/12/30 16:19
     * @return: void
     */
    @GetMapping("addContent")
    public void addContent(Content content) {

        log.info("addContent.req content={}", JSON.toJSONString(content));
        contentList.add(content);
        contentBloomFilterStrategy.addContentBloomData(content.getContentId());
        log.info("addContent.resp contentList={}", JSON.toJSONString(contentList));
    }


}
