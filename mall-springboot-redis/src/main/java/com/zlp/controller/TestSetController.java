package com.zlp.controller;

import com.alibaba.fastjson.JSON;
import com.zlp.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@Slf4j(topic = "TestSetController")
public class TestSetController {

    private final static String UP = "UP";
    private final static String CONTENT = "CONTENT";

    @Autowired
    private RedisService redisService;



    /**
     * set添加元素
     * @date: 2020/12/2 14:10
     * @return: java.lang.String
     */
    @GetMapping("sAdd")
    public String sAdd() {
        String up = CONTENT+":"+UP+":"+1;
        String value1 = "1";
        String value2 = "2";
        String value3 = "3";
        redisService.sAdd(up,value1,value2,value3);
        return "success";
    }

    /**
     * 随机set添加元素
     * @date: 2020/12/2 14:10
     * @return: java.lang.String
     */
    @GetMapping("Radd")
    public String Radd() {
        String up = CONTENT+":"+UP+":"+1;
        String value4 = "4";
        redisService.sAdd(up,value4);
        return "success";
    }

    /**
     * set移除元素
     * @date: 2020/12/2 14:11
     * @return: java.lang.String
     */
    @GetMapping("sRemove")
    public String sRemove() {
        String up = CONTENT+":"+UP+":"+1;
        String value1 = "1";
        redisService.sRemove(up,value1);
        return "success";
    }

    /**
     * 获取set长度
     * @date: 2020/12/2 14:11
     * @return: java.lang.String
     */
    @GetMapping("sSize")
    public String sSize() {
        String up = CONTENT+":"+UP+":"+1;
        Long count = redisService.sSize(up);
        log.info("sSize.count={}",count);
        return "success";
    }


    /**
     * 获取集合所有元素
     * @date: 2020/12/2 14:11
     * @return: java.lang.String
     */
    @GetMapping("setMembers")
    public String setMembers() {
        String up = CONTENT+":"+UP+":"+1;
        Set<String> strings = redisService.setMembers(up);
        log.info("sSize.strings={}", JSON.toJSONString(strings));
        return "success";
    }




}
