package com.zlp.controller.lock;


import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class IndexController {

    private final static String PRODUCT = "product:";


    private AtomicInteger count = new AtomicInteger(0);

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;


    /**
     * 扣减库存
     *
     * @date: 2022/3/29 14:18
     * @return: com.zlp.entity.Content
     */
    @GetMapping("reduceStock2")
    public String reduceStock2(@RequestParam(value = "productId") Long productId) {

        String lockKey = PRODUCT + "lock:" + productId;
        String productKey = PRODUCT + "stock:" + productId;

        RLock lock = redissonClient.getLock(lockKey);
        try {
            /**
             *  尝试获取锁
             * waitTimeout 尝试获取锁的最大等待时间，超过这个值，则认为获取锁失败
             * leaseTime   锁的持有时间,超过这个时间锁会自动失效（值应设置为大于业务处理的时间，确保在锁有效期内业务能处理完）
             */
            boolean result = lock.tryLock(30L, 60L, TimeUnit.SECONDS);
            if (result){
                String stock = redisTemplate.opsForValue().get(productKey);
                if (Integer.parseInt(stock) > 0) {
                    redisTemplate.opsForValue().decrement(productKey);
                } else {
                    String print = String.format("该商品[%d]库存不足", productId);
                    System.out.println(print);
                    return print;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return "success";

    }


    /**
     * 扣减库存
     *
     * @date: 2022/3/29 14:18
     * @return: com.zlp.entity.Content
     */
    @GetMapping("reduceStock")
    public String reduceStock(@RequestParam(value = "productId") Long productId) {

        String lockKey = PRODUCT + "lock:" + productId;
        String productKey = PRODUCT + "stock:" + productId;
        // ======  此段代码不能保证原子性 ======
//        Boolean result = redisTemplate.opsForValue().setIfAbsent(lockKey, productId.toString());
//        redisTemplate.expire(lockKey,15, TimeUnit.MILLISECONDS);

        // 加锁和设置过期时间是原子性
        String clientId = UUID.randomUUID().toString();
        Boolean result = redisTemplate.opsForValue().setIfAbsent(lockKey, clientId, 15, TimeUnit.MILLISECONDS);
        String currentDate = getCurrentDate();
        System.out.println(String.format("执行时间：%s,执行次数%d", currentDate, count.incrementAndGet()));
        if (!result) {
            return "error";
        }
        try {
            String stock = redisTemplate.opsForValue().get(productKey);
            if (Integer.parseInt(stock) > 0) {
                redisTemplate.opsForValue().decrement(productKey);
            } else {
                String print = String.format("该商品[%d]库存不足", productId);
                System.out.println(print);
                return print;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 保证删除是自己的锁
            if (Objects.equals(redisTemplate.opsForValue().get(lockKey), clientId)) {
                redisTemplate.delete(lockKey);
            }
        }
        return "success";

    }

    private String getCurrentDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date());
    }
}
