package com.zlp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 内容评论/回复
 * @author zouLiPing
 * @date 2020年8月18日10:53:22
 */
@Data
@ApiModel(value = "内容评论/回复")
public class ContentCommentReq implements Serializable{
	

	@ApiModelProperty(value = "回复父评论ID ")
	private Long parentId;

	@ApiModelProperty(value = "用户ID ")
	private Long userId;

	@NotNull(message = "文章内容发布ID为空!")
	@ApiModelProperty(value = "文章内容发布ID ")
	private Long contentId;

	@NotNull(message = "用户评论文章标识为空!")
	@ApiModelProperty(value = "用户评论文章标识 （1-作者;2-普通用户评论）")
	private Integer contentFlag;

	@ApiModelProperty(value = "回复评论ID")
	private Long replyCommentId;

	@NotNull(message = "评价的内容为空!")
	@ApiModelProperty(value = "评价的内容")
	private String commentDetails;

	@ApiModelProperty(value = "回复人用户ID")
	private Long replyUserId;




}
