package com.zlp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 内容发布返回信息
 * @author zouLiPing
 * @date 2020年8月18日10:53:22
 *
 */
@Data
@ApiModel(value = "内容发布返回信息")
public class ContentRecommendCache implements Serializable{

	@ApiModelProperty(value = "分数")
	private Long score = 0L;
	
	@ApiModelProperty(value = "文章标题Id")
	private Long contentId;

	@ApiModelProperty(value = "用户ID")
	private Long userId;

	@ApiModelProperty(value = "文章排序")
	private Integer postOrder;


}
