package com.zlp.dto;

import lombok.Data;

@Data
public class RequestVo {

    private String userName;
    private String address;
    private String telephone;
}
