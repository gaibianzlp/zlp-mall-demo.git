package com.zlp.entity;

import lombok.Data;

@Data
public class Content {

    private Long contentId;
    private String title;
    private String content;
    private Integer postState;

    public Content(){}

    public Content(Long contentId, String title, String content,Integer postState) {
        this.contentId = contentId;
        this.title = title;
        this.content = content;
        this.postState = postState;
    }
}
