package com.zlp.service;


public interface BloomFilterStrategy {

    /** 
     * 添加布隆过滤器数据
     * @param obj 添加布隆容器对象ID
     * @date: 2020/12/30 15:44
     * @return: void 
     */
    Boolean addContentBloomData(Object obj);
}
