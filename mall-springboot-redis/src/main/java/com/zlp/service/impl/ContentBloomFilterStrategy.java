package com.zlp.service.impl;

import com.zlp.service.BloomFilterStrategy;
import com.zlp.util.RedisConstants;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j(topic = "ContentBloomFilterStrategy")
@Component("contentBloomFilterStrategy")
public class ContentBloomFilterStrategy implements BloomFilterStrategy {



    @Resource
    private RedissonClient redissonClient;

    @Override
    public Boolean addContentBloomData(Object obj) {

        log.info("addContentBloomData.req obj={}",obj);
        RBloomFilter<Object> bloomFilter = redissonClient.getBloomFilter(RedisConstants.CONTENT_PREFIX);
        bloomFilter.tryInit(1000L, 0.0001);
        log.info("size={}", bloomFilter.getSize());
        //初始化布隆过滤器，var1表示大小，var3表示容错率
        bloomFilter.add(obj);
        return Boolean.TRUE;
    }
}
