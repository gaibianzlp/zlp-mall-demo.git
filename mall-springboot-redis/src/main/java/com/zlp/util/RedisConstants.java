package com.zlp.util;

/** 
 * 布隆过滤器常量
 * @date: 2020/12/30 16:13
 *
 */
public interface RedisConstants {


    /**
     * 文章详情
     */
    String CONTENT_PREFIX = "CONTENT_PREFIX";

    /**
     * 文章评论
     */
    String COMMENT = "comment";

    /**
     * 内容
     */
    String CONTENT = "content";

    /**
     * 推荐
     */
    String RECOMMEND = "reco";

    /**
     * 点赞
     */
    String UP = "UP";



    /**
     * 统计
     */
    String STATISTICS = "statistics";
    /**
     * 点赞
     */
    String UPCOUNT = "upcount";

    /**
     * 点赞集合
     */
    String UPLIST = "uplist";

    /**
     * 流水号
     */
    String SERIAL = "SERIAL_NUMBER";


}
