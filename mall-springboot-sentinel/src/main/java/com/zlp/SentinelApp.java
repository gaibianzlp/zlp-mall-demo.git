package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @description: 启动类
 * @author: LiPing.Zou
 * @create: 2020-05-25 12:55
 **/
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SentinelApp {

    public static void main(String[] args) {
        SpringApplication.run(SentinelApp.class, args);
    }
}
