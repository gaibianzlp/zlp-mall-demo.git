package com.zlp.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class SentinelController {

    @GetMapping(value = "/hello")
    @SentinelResource(value = "hello", blockHandler = "getQpsblockHandler")
    public String hello() {

        return "Hello,Sentinel";
    }

    /**
     *
     *  信号量
     * @date: 2020/9/8 11:00
     * @return: java.lang.String
     */
    @SneakyThrows
    @GetMapping(value = "/getSemaPhone")
    @SentinelResource(value = "getSemaPhone",blockHandler = "getQpsblockHandler")
    public String getSemaPhone() {

        Thread.sleep(500L);
        return "getSemaPhone,Sentinel";
    }

    /**
     *
     *  信号量
     * @date: 2020/9/8 11:00
     * @return: java.lang.String
     */
    @SneakyThrows
    @GetMapping(value = "/getRT")
    @SentinelResource(value = "getRT",blockHandler = "getQpsblockHandler")
    public String getRT() {

        Thread.sleep(300L);
        return "getRT,Sentinel";
    }



    /**
     * 接口限流返回提示
     * @param e
     * @date: 2020/9/8 11:07
     * @return: java.lang.String
     */
    public String getQpsblockHandler (BlockException e) {

        log.error ("error:",e);

        return "当请求前流量过大,请稍后再试...";
    }
}
