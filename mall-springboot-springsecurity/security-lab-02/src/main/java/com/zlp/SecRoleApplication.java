package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecRoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecRoleApplication.class,args);
    }


}
