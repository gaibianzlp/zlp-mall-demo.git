package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: LiPing.Zou
 * @create: 2020-05-25 16:43
 **/
@SpringBootApplication
public class Swagger2App {

    public static void main(String[] args) {
        SpringApplication.run(Swagger2App.class, args);
    }
}
