package com.zlp.controller;


import com.alibaba.fastjson.JSON;
import com.carrotsearch.sizeof.RamUsageEstimator;
import com.zlp.dto.CacheInfo;
import com.zlp.dto.LoginUserReq;
import com.zlp.dto.UserQueryReq;
import com.zlp.dto.UserResp;
import com.zlp.service.UserService;
import com.zlp.common.api.Pager;
import com.zlp.common.api.Result;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

/**
 * <p>
 * 管理用户表 前端控制器
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
@Api(value = "user", tags = "用户模块")
public class UserController {

    private final UserService userService;


    @ApiOperation( "登录")
    @PostMapping(value = "login")
    public Result<CacheInfo> login(@Valid @RequestBody LoginUserReq loginReq){

        return Result.success(userService.login(loginReq));
    }


    @PostMapping("getUserPage")
    @ApiOperation("获取用户列表")
    public Result<Pager<UserResp>> getUserPage(@Valid @RequestBody UserQueryReq userQueryReq) throws UnsupportedEncodingException {
        Pager<UserResp> userPage = userService.getUserPage(userQueryReq);
        // https://blog.csdn.net/banji5552/article/details/101359238
        System.out.println(JSON.toJSONString(userPage).getBytes("utf-8").length);
        System.out.println(RamUsageEstimator.sizeOf(userPage));
        return Result.success(userPage);
    }

    @GetMapping("getUserInfo")
    @ApiOperation("获取用户信息")
    public Result<UserResp> getUserInfo(
            @RequestParam(value="userId" )  @ApiParam(name="userId",value="用户ID",required = true) Long userId
    ){

        return Result.success(userService.getUserInfo(userId));
    }


}

