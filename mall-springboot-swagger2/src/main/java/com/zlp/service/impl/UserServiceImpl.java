package com.zlp.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlp.dto.*;
import com.zlp.entity.User;
import com.zlp.enums.UserTypeEnum;
import com.zlp.exception.CustomException;
import com.zlp.mapper.UserMapper;
import com.zlp.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlp.utils.BeanToUtils;
import com.zlp.utils.MethodUtil;
import com.zlp.utils.RedisConstants;
import com.zlp.utils.RedisUtils;
import com.zlp.common.api.Pager;
import com.zlp.common.api.ResultCode;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <p>
 * 管理用户表 服务实现类
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@Service
@AllArgsConstructor
@Slf4j(topic = "UserServiceImpl")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {



    private static ExecutorService executor = Executors.newFixedThreadPool(16);

    private final RedisUtils redisUtils;

    @Override
    public CacheInfo login(LoginUserReq loginReq) {

        log.info("login.req loginReq={}", JSON.toJSONString(loginReq));
        CacheInfo cacheInfo = new CacheInfo();

        Wrapper<User> wapper = new QueryWrapper<>(new User())
                .eq("username",loginReq.getUsername())
                .eq("password",loginReq.getPassword());
        User user = this.getOne(wapper);
        if (Objects.isNull(user)){
            throw new CustomException(ResultCode.SYS_10002, MethodUtil.getLineInfo());
        }
        bulidCache(cacheInfo, user);
        String key = RedisConstants.CACHE+ StrUtil.COLON+cacheInfo.getToken();
        // 设置登入用户缓存信息
        redisUtils.setKeyByHOURS(key,cacheInfo.getId().toString(),1);
        return cacheInfo;
    }

    @Override
    public Pager<UserResp> getUserPage(UserQueryReq userQueryReq) {

        log.info("getUserPage.req userQueryReq={}", JSON.toJSONString(userQueryReq));
        IPage<User> page = new Page<>(userQueryReq.getPageNumber(),userQueryReq.getPageSize());
        QueryWrapper<User> queryWapper = new QueryWrapper<>();
        IPage<User> pager = this.page(page, queryWapper);
        if (Objects.nonNull(pager) && CollectionUtil.isNotEmpty(pager.getRecords())) {
            List<User> records = pager.getRecords();
            List<UserResp> userRespList = BeanToUtils.entityToList(records, UserResp.class);
            return new Pager<>(userQueryReq.getPageNumber(),userQueryReq.getPageSize(),(int)pager.getPages(),pager.getTotal(),userRespList);
        }
        return new Pager<>();
    }

    @SneakyThrows
    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserResp getUserInfo(Long userId) {

        ThreadLocal local = new ThreadLocal();
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        // 计算CompletableFuture
        CompletableFuture<Void> test = CompletableFuture.runAsync(() -> {
            try {
                User user = new User();
                user.setUserType(0);
                user.setPassword("1223123");
                user.setUsername("fsdfd" + System.currentTimeMillis());
                user.setCreateTime(new Date());

                this.save(user);
            } catch (Exception e) {
                local.set(true);
                atomicBoolean.set(true);
            }
        });


       new Thread(()->{

           User user = new User();
           user.setUserType(0);
           user.setPassword("1223123");
           user.setUsername("fsdfd"+System.currentTimeMillis());
           user.setCreateTime(new Date());

           this.save(user);

       }).start();
        int a  = 1/ 0;
        System.out.println("atomicBoolean="+atomicBoolean.get());
        System.out.println("local="+local.get());
        CompletableFuture.allOf(test).get();

        log.info("getUserInfo.req userId={}", userId);
        User user = this.getById(userId);
        if (Objects.isNull(user)){
            throw new CustomException(ResultCode.SYS_10003, MethodUtil.getLineInfo());
        }
        return BeanToUtils.doToDto(user,UserResp.class);
    }

    private void bulidCache(CacheInfo cacheInfo, User user) {

        cacheInfo.setId(user.getId());
        cacheInfo.setUsername(user.getUsername());
        cacheInfo.setNickName(user.getNickname());
        cacheInfo.setToken(UUID.randomUUID().toString());
        cacheInfo.setUserType(UserTypeEnum.ADMINISTRATOR.getValue());
    }
}
