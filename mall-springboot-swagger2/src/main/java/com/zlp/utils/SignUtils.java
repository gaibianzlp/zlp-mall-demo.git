package com.zlp.utils;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

/**
 * 签名工具类
 * @date: 2021/5/13 17:01
 */
public class SignUtils {

    /** 
     * 获取时间戳
     * @date: 2021/5/13 17:03
     */
    public static long getTimestamp() {

        long timestamp = System.currentTimeMillis() / 1000;
        return timestamp;
    }



    /** 
     * 获取签名
     * @param key
     * @param secret
     * @param timestamp
     * @date: 2021/5/13 17:05
     * @return: long 
     */
    public static String getSign(String key,String secret,String timestamp) {

        String sign = "";
        Map<String,String> mapParams = new HashMap<>(3);
        return sign;
    }


    /**
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param params 需要排序并参与字符拼接的参数组
     * @return 拼接后字符串
     * @throws UnsupportedEncodingException
     *
     */
    public static String getParamsCoverString(Map<String, String> params) throws UnsupportedEncodingException {

        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = "";
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            value = URLEncoder.encode(value, "UTF-8");
            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                prestr = prestr + key + "=" + value;
            } else {
                prestr = prestr + key + "=" + value + "&";
            }
        }
        return prestr;
    }

    
}
