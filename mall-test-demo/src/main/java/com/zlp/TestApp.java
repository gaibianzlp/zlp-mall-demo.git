package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import java.util.concurrent.locks.AbstractQueuedLongSynchronizer;
import java.util.concurrent.locks.Lock;

@SpringBootApplication
//@ConditionalOnBean(ThreadLocal.class)
public class TestApp {



    public static void main(String[] args) {
        SpringApplication.run(TestApp.class,args);
    }
}
