package com.zlp.algorithm;


import lombok.extern.slf4j.Slf4j;

/**
 * 冒泡排序算法
 *
 * @date: 2021/1/5 16:58
 */
@Slf4j(topic = "BubblingDemo")
public class BubblingDemo {

    public static void main(String[] args) {

        int[] nums = {6, 5, 7, 3, 9, 2, 10, 4};

        bublingAsc(nums);
        for (int num : nums) {
            log.info(num + "\t");
        }
    }

    private static void bublingAsc(int[] nums) {

        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = 0; j < nums.length - i - 1; j++) {
               //临时变量
                int temp;
                if (nums[j] > nums[j + 1]) {
                    temp = nums[j + 1];
                    nums[j + 1] = nums[j];
                    nums[j] = temp;
                }
            }
        }
    }


}
