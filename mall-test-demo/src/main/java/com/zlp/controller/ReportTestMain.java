package com.zlp.controller;
 
import com.zlp.dto.HousekeeperReport;
import com.zlp.dto.User;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReportTestMain {
	public static void main(String[] args) {

		HousekeeperReport report1 = new HousekeeperReport(null,null,"帕金森病", 1L, 5L, new BigDecimal(56));
		HousekeeperReport report2 = new HousekeeperReport(null,null,"帕金森病", 2L, 6L, new BigDecimal(76));
		HousekeeperReport report3 = new HousekeeperReport(null,null,"帕金森病", 2L, 6L, new BigDecimal(80));

		List<HousekeeperReport> reportList = new ArrayList<>();
		reportList.add(report1);
		reportList.add(report2);
		reportList.add(report3);

		if (CollectionUtils.isNotEmpty(reportList)) {
			reportList = loadSumReport(reportList);
		}
		
		for (HousekeeperReport report : reportList) {
			System.out.println("统计:"+report.getRegion()+"\t 数量:"+report.getCounter()+"\t 疾病:"+report.getIllnessName()+"\t 方案ID:"+report.getCaseId()+
					"\t 汇总："+report.getReportScore());
		}

	}
 
	private static List<HousekeeperReport> loadSumReport(List<HousekeeperReport> reportList) {
		
		List<HousekeeperReport> sumList = new ArrayList<>();
		HousekeeperReport report2 = new HousekeeperReport();
		HousekeeperReport report3 = new HousekeeperReport();
		String illnessName = reportList.get(0).getIllnessName();
		String groupA = illnessName + reportList.get(0).getCaseId();
		int groupACount = 0;
		int groupBCount = 0;
		for (HousekeeperReport report : reportList) {
			// 求(第三层)小计
			String groupLeA = report.getIllnessName()+ report.getCaseId();
			if (groupA.equals(groupLeA)) {
				groupACount = groupACount+ 1;
				report3.setCounter(groupACount);
				laodData(report3, report, 3);
			} else {
				report3.setRegion("小计");
				report3.setReportScore(report3.getReportScore().divide(new BigDecimal(report3.getCounter())).setScale(2,BigDecimal.ROUND_HALF_UP));
				sumList.add(report3);
				groupA = report.getIllnessName()+ report.getCaseId();
				report3 = new HousekeeperReport();
				laodData(report3, report, 3);
			}
		}
		// 求最后一个汇总（）
		report3.setRegion("小计");
		report3.setReportScore(report3.getReportScore().divide(new BigDecimal(report3.getCounter())).setScale(2,BigDecimal.ROUND_HALF_UP));
		sumList.add(report3);

		// 求总计
		for (HousekeeperReport user : sumList) {
			if ("小计".equals(user.getRegion())) {
				groupBCount = groupBCount +1;
				report2.setCounter(groupBCount);
				laodData(report2, user, 1);
			}
		}

		report2.setReportScore(report2.getReportScore().divide(new BigDecimal(report2.getCounter())).setScale(2,BigDecimal.ROUND_HALF_UP));
		report2.setRegion("总计");
		sumList.add(report2);
		sumList.removeAll(reportList);
		if (sumList.size() <= 2){
			sumList.remove(0);
		}
		return sumList;
	}
    // 求小计和总计
	public static void laodData(HousekeeperReport report2, HousekeeperReport report, Integer type) {

		BigDecimal reportScore = Objects.isNull(report2.getReportScore())?BigDecimal.ZERO:report2.getReportScore();
		report2.setReportScore(reportScore.add(report.getReportScore()));
		report2.setCaseId(report.getCaseId());
	}
 
}