package com.zlp.controller;
 
import com.zlp.dto.User;
import org.w3c.dom.css.Counter;

import java.util.ArrayList;
import java.util.List;
 
public class TestMain {
	public static void main(String[] args) {
	  Integer counter = 0;

		User user = new User("江苏", "东部", "江西春秋", 1);
		User user1 = new User("江苏", "东部", "江西春秋", 2);
		User user2 = new User("江苏", "东部", "江西春秋", 3);
		User user3 = new User("江苏", "西部", "江西春秋", 4);
		User user4 = new User("江苏", "西部", "江西春秋", 5);
		
		/*User user5 = new User("上海", "东部", "江西春秋", 1);
		User user6 = new User("上海", "东部", "江西春秋", 6);
		User user7 = new User("上海", "西部", "江西春秋", 7);
		User user8 = new User("上海", "西部", "江西春秋", 8);
		
		User user9 = new User("北京", "南部", "江西春秋", 5);
		User user10 = new User("北京", "南部", "江西春秋", 6);
		User user11 = new User("北京", "北部", "江西春秋", 7);
		User user12 = new User("北京", "北部", "江西春秋", 8);*/
		List<User> userList = new ArrayList<User>();
		userList.add(user);
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
		userList.add(user4);
	/*	userList.add(user5);
		userList.add(user6);
		userList.add(user7);
		userList.add(user8);
		userList.add(user9);
		userList.add(user10);
		userList.add(user11);
		userList.add(user12);*/
		if (userList != null && userList.size()>0) {
			userList = loadSumUser(userList);
		}
		
		for (User users : userList) {
			System.out.println(users.getCorp()+"\t"+users.getRegion()+
					"\t"+users.getDbCode()+"\t"+users.getAmt());
		}
	}
 
	private static List<User> loadSumUser(List<User> userList) {
		
		List<User> sumList = new ArrayList<>();
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		String corp = userList.get(0).getCorp();
		String groupA = userList.get(0).getCorp()+ userList.get(0).getRegion();
		for (User users : userList) {
			// 求(第三层)小计
		    String groupLeA = users.getCorp()+ users.getRegion();
			if (groupA.equals(groupLeA)) {
				laodData(user3, users, "3");
			} else {
				user3.setCorp("");
				user3.setDbCode("");
				user3.setRegion("小计");
				sumList.add(user3);
				groupA = users.getCorp()+ users.getRegion();
				user3 = new User();
				laodData(user3, users, "3");
			}
			// 求(第二层)小计
		    String corpB = users.getCorp();
			if (corp.equals(corpB)) {
				laodData(user2, users, "2");
			} else {
				user2.setCorp("");
				user2.setDbCode("");
				user2.setRegion("总计");
				sumList.add(user2);
				System.out.println("------------------------------");
				corp = users.getCorp();
				user2 = new User();
				laodData(user2, users, "2");
			}
			sumList.add(users);
		}
		// 求最后一个汇总（）
		user3.setCorp("");
		user3.setDbCode("");
		user3.setRegion("小计");
		sumList.add(user3);
		user2.setCorp("");
		user2.setDbCode("");
		user2.setRegion("总计");
		sumList.add(user2);
		
		// 求总计
		for (User user : sumList) {
			if ("总计".equals(user.getRegion())) {
				laodData(user1, user, "1");
			}
		}
		user1.setCorp("");
		user1.setDbCode("");
		user1.setRegion("汇总");
		sumList.add(user1);
		
		
		return sumList;
	}
    // 求小计和总计
	public static void laodData(User user3, User users, String type) {
		Integer amt= user3.getAmt()==null?0:user3.getAmt();
		//System.out.println(amt+users.getAmt());
		user3.setAmt(amt+users.getAmt());
		
	}
 
}