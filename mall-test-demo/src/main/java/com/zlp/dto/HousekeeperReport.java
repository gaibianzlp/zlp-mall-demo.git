package com.zlp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 管家报告统计表
 * </p>
 *
 * @author SuiRen
 * @since 2021-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class HousekeeperReport  {

    private static final long serialVersionUID=1L;

    private Integer counter;


    private String region;
    /**
     * 疾病名称
     */
    private String illnessName;

    /**
     * 方案ID
     */
    private Long caseId;

    /**
     * 药物ID
     */
    private Long drugId;


    /**
     * 报告平均得分
     */
    private BigDecimal reportScore;






}
