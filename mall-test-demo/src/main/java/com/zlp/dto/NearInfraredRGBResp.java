package com.zlp.dto;

import lombok.Data;
import java.util.List;


/**
 * 近红外数据
 */
@Data
public class NearInfraredRGBResp {

    private List<List<String>> nearInfrareds;


}
