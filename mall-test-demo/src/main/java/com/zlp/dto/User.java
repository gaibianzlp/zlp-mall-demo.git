package com.zlp.dto;
 
public class User {
	
	private String corp;
	
	private String region;
	
	private String dbCode;
	
	private String sign; //标记
	
	private Integer amt;
	
	
	
	
 
	public String getSign() {
		return sign;
	}
 
	public void setSign(String sign) {
		this.sign = sign;
	}
 
	public String getCorp() {
		return corp;
	}
 
	public void setCorp(String corp) {
		this.corp = corp;
	}
 
	public String getRegion() {
		return region;
	}
 
	public void setRegion(String region) {
		this.region = region;
	}
 
	public String getDbCode() {
		return dbCode;
	}
 
	public void setDbCode(String dbCode) {
		this.dbCode = dbCode;
	}
 
	public Integer getAmt() {
		return amt;
	}
 
	public void setAmt(Integer amt) {
		this.amt = amt;
	}
 
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
 
	public User(String corp, String region, String dbCode, Integer amt) {
		super();
		this.corp = corp;
		this.region = region;
		this.dbCode = dbCode;
		this.amt = amt;
	}
	
	
 
}