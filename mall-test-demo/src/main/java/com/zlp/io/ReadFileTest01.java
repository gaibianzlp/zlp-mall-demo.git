package com.zlp.io;

import com.alibaba.fastjson.JSON;
import com.zlp.dto.NearInfraredRGBResp;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Objects;

@Slf4j(topic = "ReadFileTest01")
public class ReadFileTest01 {

    public static void main(String[] args) {

        String filePath = "D:"+File.separator + "file" + File.separator + "rgb.txt";
        String text = readRgbFile(filePath);
    }

    private static String readRgbFile(String filePath) {

        String lineTxt = "";
        InputStreamReader read = null;
        try {
            String encoding = "utf-8";
            File file = new File(filePath);
            if (file.isFile() && file.exists()) { //判断文件是否存在
                read = new InputStreamReader(
                        new FileInputStream(file), encoding);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                lineTxt = bufferedReader.readLine();
                read.close();
                log.info("readFile 文件读取完毕");
            } else {
                throw new Exception(String.format("该目录[%s]下文件不存在！", filePath));
            }
        } catch (Exception e) {
            log.error(" readFile 读取文件内容出错!", e);
        }finally {
            if (Objects.nonNull(read)) {
                try {
                    read.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return lineTxt;
    }
}