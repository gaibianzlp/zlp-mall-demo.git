package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 启动类
 * @author: LiPing.Zou
 * @create: 2020-05-25 12:55
 **/
@SpringBootApplication
public class JmeterApp {

    public static void main(String[] args) {
        SpringApplication.run(JmeterApp.class, args);
    }
}
