package com.zlp.aop;




import java.lang.annotation.*;
/**
 * 自定义服务限流注解
 * @date 2020-9-6 15:36:44
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface ExtRateLimiter {

    /**
     *  以每秒为单位固定速率
     */
    double rate() default 10d;

    /**
     *  在规定的毫秒数没有获取到令牌，大于超时时间，走服务降级
     */
    long timeOut() default 500L;


}
