package com.zlp.aop;

import com.google.common.util.concurrent.RateLimiter;
import com.zlp.api.CommonResult;
import com.zlp.domian.entity.PmsBrand;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 功能说明 ：使用环绕通知判断该方法是否添加限流注解 <br>
 * @date 2020-9-6 15:36:44
 */
@Aspect
@Component
public class RateLimiterAspect {

    private Map<String,RateLimiter> limitHashMap = new ConcurrentHashMap();

    @Pointcut("@annotation(com.zlp.aop.ExtRateLimiter)")
    public void RateAspect(){

    }


    /**
     * 使用AOP 中before 判断该方法是否添加该ExtRateLimiter注解
     */
    @Around("RateAspect()")
    public Object doBefore(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{

        // 使用java反射机制获取注解中的参数
        MethodSignature signature = (MethodSignature)proceedingJoinPoint.getSignature();
        Method method = signature.getMethod();
        ExtRateLimiter declaredAnnotation = method.getDeclaredAnnotation(ExtRateLimiter.class);
        RateLimiter rateLimiter = null;
        double rate = declaredAnnotation.rate();
        long timeOut = declaredAnnotation.timeOut();
        if (limitHashMap.containsKey(getHttpRequestUrl())) {
            // 如果limitHashMap存在，则获取rateLimiter对象
            rateLimiter = limitHashMap.get(getHttpRequestUrl());
        } else {
            // 如果limitHashMap不存在，则创建添加新的URL
            rateLimiter = RateLimiter.create(rate);
            limitHashMap.put(getHttpRequestUrl(),rateLimiter);
        }
        // 从RateLimiter 获取令牌，如果没有获取到令牌，走服务降级方法
        boolean tryAcquire = rateLimiter.tryAcquire(timeOut, TimeUnit.MICROSECONDS);
        if (Boolean.FALSE.equals(tryAcquire)) {
//            fallback();
            return CommonResult.failed("服务器请求繁忙，请稍后再试...");
        }
        // 获取到令牌,走实际代码
        return proceedingJoinPoint.proceed();

    }

    /**
     *  服务降级
     */
    @SneakyThrows
    private void fallback() {

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = attributes.getResponse();
        // 设置浏览器以何种方式编码输入流
        response.setHeader("content-type", "text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.println("服务器请求繁忙，请稍后再试...");


    }



    /**
     *  获取request对象
     */
    private String getHttpRequestUrl() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        return request.getRequestURI();
    }

}
