package com.zlp.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("framework")
@Data
public class SysUrlProperties {

    private String orgSyncUrl;
    private String orgSystemUrl;


}
