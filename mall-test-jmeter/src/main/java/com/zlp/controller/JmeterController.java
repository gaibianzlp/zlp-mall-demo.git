package com.zlp.controller;

import com.google.common.util.concurrent.RateLimiter;
import com.zlp.api.CommonResult;
import com.zlp.domian.entity.PmsBrand;
import com.zlp.service.PmsBrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
public class JmeterController {

    private static volatile Integer count = 1;


    /**
     *   create方法参数
     *  以每秒为单位固定速率1r/s ，每秒中存入桶中存入一个令牌桶
     **/
    RateLimiter rateLimiter = RateLimiter.create(1); // 独立线程


    @Autowired
    private PmsBrandService demoService;


    @PostMapping("inserBrand")
    public CommonResult<Integer> inserBrand(){

        PmsBrand brand = new PmsBrand();
        brand.setName("T品牌_"+System.currentTimeMillis());
        brand.setLogo("log_"+(Math.random()*100+1));
        brand.setBrandStory("story_A");
        brand.setFirstLetter("B");
        brand.setBigPic("PIC_A");
        brand.setShowStatus(1);
        brand.setProductCount(100);
        brand.setProductCommentCount(1);
        demoService.createBrand(brand);
        return CommonResult.success(++count);
    }

    /**
     * 浏览器访问
     * http://127.0.0.1:8080/brand/delete/3
     * standalone
     * @param id
     * @date: 2020-05-25 16:16
     * @return: com.com.zlp.common.api.CommonResult<com.com.zlp.domian.entity.PmsBrand>
     */
    @GetMapping(value = "/getBrand/{id}")
    public CommonResult<PmsBrand> brand(@PathVariable("id") Long id) {
        return CommonResult.success(demoService.getBrand(id,null));
    }

    @GetMapping(value = "/getNoBrand")
    public CommonResult<PmsBrand> getNoBrand() {
        // 1.客户端从桶中获取对应的令牌，为什么是double值，这个结果表示从桶中拿到令牌等待时间
        // 2.如果获取不到token,实现服务降级，（如果在规定时间内获取不到令牌，直接走服务降级）
//        double acquire = rateLimiter.acquire();
//        System.out.println("从桶中获取令牌等待时间："+acquire);

        boolean tryAcquire = rateLimiter.tryAcquire(500, TimeUnit.MICROSECONDS);
        if (Boolean.FALSE.equals(tryAcquire)) {
            System.out.println("亲 ！实现服务降级！");
            return CommonResult.success(new PmsBrand());
        }


        return CommonResult.success(new PmsBrand());
    }

//    @ExtRateLimiter(rate = 20,timeOut = 500)
    @GetMapping(value = "/findBrand")
    public CommonResult<PmsBrand> findBrand() {
        PmsBrand pmsBrand = new PmsBrand();
        pmsBrand.setProductCount(1);
        pmsBrand.setProductCommentCount(2);
        pmsBrand.setBigPic("test");
        pmsBrand.setBrandStory("32324story");
        pmsBrand.setFactoryStatus(2);
        pmsBrand.setFirstLetter("A");
        return CommonResult.success(pmsBrand);
    }
}
