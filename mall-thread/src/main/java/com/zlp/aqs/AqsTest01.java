package com.zlp.aqs;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedLongSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

@Slf4j(topic = "AqsTest01")
public class AqsTest01 {

    @SneakyThrows
    public static void main(String[] args) {

        MyLock lock = new MyLock();

        new Thread(()-> {
            lock.lock();
            try {
                log.info("locking...");
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                log.info("unlocking...");
                lock.unlock();
            }


        },"t1").start();
        new Thread(()-> {
            lock.lock();
            try {
                log.info("locking...");

            }finally {
                log.info("unlocking...");
                lock.unlock();
            }


        },"t2").start();

    }
}

// 自定义锁 （不可重入锁）
class MyLock implements Lock{

    // 自定义抽象同步锁 aqs
    class MySync extends AbstractQueuedLongSynchronizer{


        /**
         * 尝试获取锁
         *
         * @param arg
         * @date: 2020/11/27 14:57
         */
        @Override
        protected boolean tryAcquire(long arg) {

            if (compareAndSetState(0,1)){
                // 如果加锁成功，设置当前线程
                setExclusiveOwnerThread(Thread.currentThread());
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }

        /**
         * 尝试释放锁
         *
         * @param arg
         * @date: 2020/11/27 14:57
         */
        @Override
        protected boolean tryRelease(long arg) {
            // 设置所属线程为空
            setExclusiveOwnerThread(null);
            // 线程状态为0-未占用
            setState(0);
            return Boolean.TRUE;
        }

        /**
         * 是否持有独占锁
         *
         * @date: 2020/11/27 14:57
         */
        @Override
        protected boolean isHeldExclusively() {
            return getState() == 1;
        }

        public Condition newCondition() {

            return new ConditionObject();
        }
    }

    MySync mySync = new MySync();

    /**
     * 加锁 （不成功会进入等待队列）
     * @date: 2020/11/19 20:24
     */
    @Override
    public void lock() {
        mySync.tryAcquire(1);
    }

    /**
     * 加锁 （可以打断）
     * @date: 2020/11/19 20:24
     */
    @Override
    public void lockInterruptibly() throws InterruptedException {

        mySync.acquireInterruptibly(1);

    }

    /**
     * 尝试获取锁  （一次）
     * @date: 2020/11/19 20:24
     */
    @Override
    public boolean tryLock() {
        return mySync.tryAcquire(1);
    }

    /**
     * 尝试获取锁  （带超时）
     * @date: 2020/11/19 20:24
     */
    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return mySync.tryAcquireNanos(1,TimeUnit.SECONDS.toNanos(time));
    }

    /**
     * 解锁
     * @date: 2020/11/19 20:24
     */
    @Override
    public void unlock() {
        mySync.tryRelease(0);
    }

    /**
     * 自定义条件判断
     * @date: 2020/11/19 20:24
     */
    @Override
    public Condition newCondition() {
        return mySync.newCondition();
    }
}
