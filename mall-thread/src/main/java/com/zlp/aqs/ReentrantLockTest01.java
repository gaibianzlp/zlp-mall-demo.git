package com.zlp.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "ReentrantLockTest01")
public class ReentrantLockTest01 {

    public static void main(String[] args) {

        Lock lock = new ReentrantLock();
        new Thread(() -> {
            try {
                lock.lock();
                log.info("locking....");
                try {
                    log.info(" t1 sellp 1s ....");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } finally {
                log.info("t1 unlock...");
                lock.unlock();
            }
        }, "t1").start();

        try {
            lock.lock();
            new Thread(() -> {
                log.info("locking....");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }, "t2").start();
        } finally {
            log.info("t2 unlock...");
            lock.unlock();
        }
    }

}
