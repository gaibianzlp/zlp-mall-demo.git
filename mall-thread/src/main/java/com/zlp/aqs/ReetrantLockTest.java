package com.zlp.aqs;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "ReetrantLockTest")
public class ReetrantLockTest {

    private static int count = 0;
    static Lock lock = new ReentrantLock();


    @SneakyThrows
    public static void main(String[] args) {

        Runnable runnable = ()->{
            for (int i = 0; i < 100; i++) {
                inc();
            }
        };
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(runnable);
            thread.start();
            threads.add(thread);

        }
        // 线程有序
        for (Thread thread : threads) {
            thread.join();
        }
        log.info("count={}",count);
    }

    

    public static void inc() {
        lock.lock();
        try {
            Thread.sleep(1);
            count++;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
