package com.zlp.aqs;


public class SAbstractQueuedSynchronizer {


    /**
     *
     * 等待队列节点类
     * <p>等待队列是 "CLH" (Craig, Landin, and Hagersten) 思想实现等待队列
     * 相反，我们使用它们来阻止同步器, 但是
     * 使用相同的基本策略，即在其节点的前身中包含有关线程的某些控件信息.
     * 每个节点中的“状态”字段跟踪线程是否应阻塞.
     * 节点的前继释放时会发出信号.  否则，队列的每个节点都充当
     * 特定通知样式的监视器，其中包含单个等待线程
     * 虽然状态字段不控制线程是否被授予锁等.
     * 线程可能会尝试获取它是否在队列中首先。
     * 但是先行并不能保证成功。
     * 它仅授予竞争权。因此，当前发布的竞争者线程可能需要重新等待.
     *
     *
     * <p>排队 CLH 锁, 自动将其新的队列添加尾部。要出队，您只需设置头字段。
     * <pre>
     *      +------+ 上一个 +-----+       +-----+
     *   头 |      | <---- |     | <---- |     |  尾
     *      +------+       +-----+       +-----+
     * </pre>
     *
     * <p>插入到CLH队列中，只需要对“ 尾部”执行单个原子操作,
     * 因此，存在一个从非排队到排队的简单分界点。同样，使出队仅涉及更新“ head”。
     * 但是，节点需要花费更多的精力来确定其继任者，一部分是由于超时和中断而可能导致的取消。.
     *
     * <p>“ prev”链接（在原始的CLH锁中不使用）主要是用于处理取消。如果取消某个节点，
     * 则其后继节点（通常）会重新链接到一个未取消的前任节点。
     * 有关自旋锁情况下类似机制的说明，请参见Scott和Scherer的论文，网址为
     * http://www.cs.rochester.edu/u/scott/synchronization/
     *
     * <p>我们还使用“下一个”链接来实现阻止机制。
     * 每个节点的线程ID保留在其自己的节点中，因此前继通过遍历
     * 下一个链接确定它是哪个线程，从而通知下一个节点唤醒。
     * 确定后继节点必须避免与新排队的节点竞争以设置其前任的“下一个”字段。
     * 必要时，通过在节点的后继者似乎为空时从原子更新的“尾部”向后检查来解决此问题。
     * （或者换句话说，下一个链接是一种优化，因此我们通常不需要向后扫描。）
     *
     * <p>取消将一些保守性引入到基本算法中。
     * 由于我们必须轮询其它节点的取消，因此我们可能会遗漏一个被取消的节点
     * 在我们前面还是后面。这可以通过以下方式来解决：
     * 始终在取消后立即取消后继者，
     * 使他们能够稳定在新的前继节点上，除非我们能够确定一个将取消此后继节点的前继节点.
     *
     * <p>CLH 队列需要一个哑元（空的）头节点节点才能开始.
     * 但是我们不会在构建过程中创建它们，因为如果永不争用，
     * 那将会浪费精力。而是构造节点，并在第一个竞争时设置头和尾指针.
     *
     * <p>等待条件的线程使用相同的节点,
     * 但使用附加链接。条件只需要在简单（非并行）链接队列中链接节点，
     * 因为仅当它们被独占时才可以访问它们。等待时，
     * 将节点插入条件队列。收到信号后，
     * 该节点被转移到主队列。 status字段的特殊值用于标记节点所在的队列.
     *
     */
    static final class Node {

        /** 指示节点正在共享模式下等待的标记 */
        static final SAbstractQueuedSynchronizer.Node SHARED = new SAbstractQueuedSynchronizer.Node();

        /** 指示节点正在以独占模式等待的标记 */
        static final SAbstractQueuedSynchronizer.Node EXCLUSIVE = null;

        /** waitStatus值，指示线程已取消 */
        static final int CANCELLED =  1;
        /** waitStatus值，指示后继节点线程需要释放 */
        static final int SIGNAL    = -1;
        /** waitStatus值，指示线程正在等待条件 */
        static final int CONDITION = -2;

        /**
         * waitStatus值，指示下一个acquireShared应该
         * 无条件传播
         */
        static final int PROPAGATE = -3;

        /**
         * 状态字段:
         *   SIGNAL:     该节点的后继者（或将很快被）（通过停放）被阻止，
         *               因此当前节点在释放或取消时必须*取消停放其后继者。
         *               为了避免种族冲突，acquire方法必须*首先表明它们需要信号，
         *               然后重试原子获取，然后，失败时，阻塞.
         *   CANCELLED:  该节点由于超时或中断而被取消.
         *              节点永远不会离开此状态。尤其是
         *               具有取消节点的线程永远不会再次阻塞.
         *   CONDITION:  该节点当前在条件队列中.
         *               在传输之前，它不会用作同步队列节点，此时状态将设置为0。
         *               （在此使用此值与字段的其他用途无关，但简化了机制。 ）
         *   PROPAGATE:  releaseShared应该传播到其他节点。
         *               在doReleaseShared中设置了此设置（仅适用于头节点），
         *               以确保传播继续进行，即使由于干预而进行了其他操作。.
         *   0:          以上都不是
         *
         * 数值按数字排列以简化使用.
         * 非负值表示节点不需要发送信号。因此，大多数代码不需要检查特定的值，仅需检查符号.
         *
         * 对于普通同步节点，该字段初始化为0, 和CONDITION用于条件节点。
         * 使用CAS （或在可能时进行无条件的易失性写入）进行修改.
         */
        volatile int waitStatus;

        /**
         * 链接到当前节点/线程所依赖的前任节点
         * 用于检查waitStatus。在入队期间分配，
         * 并且仅在出队时将清空（出于GC的考虑）。
         * 同样，在取消前任后，我们会在短路的同时找到一个永远不会存在的不可取消的前者，
         * 因为头节点从未被取消：一个节点仅由于成功获取而成为头。
         * 被取消的线程永远不会成功获取，只有一个*线程会取消自身，而不会取消任何其他节点。.
         */
        volatile SAbstractQueuedSynchronizer.Node prev;

        /**
         * 链接到当前节点/线程释放后将停放的后继节点
         * 入队时分配, 绕过已取消的前任时调整，并在出队时清零（出于的原因）.
         * enq操作在附件之后才分配前身的下一个字段,
         * 因此，看到下一个字段为null并不一定意味着节点位于队列末尾.
         * 但是，如果下一个字段为空，则我们可以从尾到前扫描.
         * 被取消节点的下一个字段设置为*指向节点本身而不是null，以使isOnSyncQueue的工作更轻松.
         */
        volatile SAbstractQueuedSynchronizer.Node next;

        /**
         * 排队此节点的线程.  在结构上初始化，使用后消失.
         */
        volatile Thread thread;

        /**
         * 链接到等待条件的下一个节点, 或特殊的共享值.
         * 因为条件队列仅在独占模式下才被访问,
         * 我们只需要一个简单的链接队列，即可在节点等待条件时保存节点。
         * 然后将它们转移到队列以重新获取。并且由于条件只能是互斥的，
         * 所以我们使用特殊值来表示模式来保存字段.
         */
        SAbstractQueuedSynchronizer.Node nextWaiter;

        /**
         * Re如果节点在共享模式下等待，则返回true。
         */
        final boolean isShared() {
            return nextWaiter == SHARED;
        }

        /**
         * 返回上一个节点，如果为null，则抛出NullPointerException。
         * 在前任不能为null时使用。 可以取消空检查，但它可以帮助虚拟机
         *
         * @return the predecessor of this node
         */
        final SAbstractQueuedSynchronizer.Node predecessor() throws NullPointerException {
            SAbstractQueuedSynchronizer.Node p = prev;
            if (p == null)
                throw new NullPointerException();
            else
                return p;
        }

        // Used to establish initial head or SHARED marker
        Node() {
        }

        Node(Thread thread, SAbstractQueuedSynchronizer.Node mode) {     // Used by addWaiter
            this.nextWaiter = mode;
            this.thread = thread;
        }

        // Used by Condition
        Node(Thread thread, int waitStatus) {
            this.waitStatus = waitStatus;
            this.thread = thread;
        }
    }
}
