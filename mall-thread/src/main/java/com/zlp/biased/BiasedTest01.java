package com.zlp.biased;

import lombok.extern.slf4j.Slf4j;
import org.openjdk.jol.info.ClassLayout;

@Slf4j
public class BiasedTest01 {
    public static void main(String[] args) {
        MyThread thread = new MyThread();
        thread.start();
    }
}

@Slf4j
class MyThread extends Thread{

    private static Object lock = new Object();
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            synchronized (lock){
                log.info(Thread.currentThread().getName());
                System.out.println(ClassLayout.parseInstance(lock).toPrintable());
            }
        }
    }
}

