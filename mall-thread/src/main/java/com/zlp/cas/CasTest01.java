package com.zlp.cas;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j(topic = "CasTest01")
public class CasTest01 {

    static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {

        // 对atomicInteger进行1000次相加
        Runnable runnable = ()->{
            for (int i = 0; i < 1000; i++) {
                atomicInteger.incrementAndGet();
            }
        };

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(runnable);
            thread.start();
            threads.add(thread);

        }

        // 线程有序
        for (Thread thread : threads) {
            thread.join();
        }

        log.info("atomicInteger={}",atomicInteger);
    }
}
