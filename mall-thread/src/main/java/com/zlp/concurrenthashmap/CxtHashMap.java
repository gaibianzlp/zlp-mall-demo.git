package com.zlp.concurrenthashmap;

import java.io.Serializable;

public class CxtHashMap<K,V> implements Serializable {

    private static final long serialVersionUID = 362498820763181265L;

    /**
     * 默认初始容量-必须为2的幂
     */
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16

    /**
     * 最大容量，如果任意一个带有参数的构造函数隐式指定更高的值，
     * 则使用该最大容量。 必须是两个<= 1 << 30的幂。
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * 在构造函数中未指定时使用的负载系数
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * hashMap元素个数
     */
    int size;

     CxtNode<K,V>[] table;

    public static <V, K> void main(String[] args) {

        CxtHashMap cxtHashMap = new CxtHashMap<>();
        cxtHashMap.table = new CxtNode[10];

//        tab[i] = newNode(hash, key, value, null);
        Test test = new Test().invoke();
        String key1 = test.getKey1();
        String value1 = test.getValue1();
        int hash1 = test.getHash1();
        String key = "k2";
        String value = "v2";
        int hash = 2;

        String key3 = "k3";
        String value3 = "v3";
        CxtNode chicherNode3 =  new CxtNode(hash,key3,value3,null);
        CxtNode chicherNode =  new CxtNode(hash,key,value,chicherNode3);
        int i = 0;
        cxtHashMap.table[i] = new CxtNode(hash1,key1,value1,chicherNode);

        for (CxtNode<K,V> e = cxtHashMap.table[i]; e != null; e = e.next) {
            System.out.println("eeee"+cxtHashMap.table[i]);
            System.out.println("e.next="+e.next);
            System.out.println("e="+e);
        }
      /*  int count = 1;
        for (CxtNode<K,V> [] tables = cxtHashMap.table ;;count++) {
            System.out.println(count);
        }*/


    }


    private static class Test {
        private String key1;
        private String value1;
        private int hash1;

        public String getKey1() {
            return key1;
        }

        public String getValue1() {
            return value1;
        }

        public int getHash1() {
            return hash1;
        }

        public Test invoke() {
            key1 = "k1";
            value1 = "v1";
            hash1 = 1;
            return this;
        }
    }
}
