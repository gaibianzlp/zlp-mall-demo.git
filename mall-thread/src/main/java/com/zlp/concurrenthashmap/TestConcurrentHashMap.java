package com.zlp.concurrenthashmap;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j(topic = "TestConcurrentHashMap")
public class TestConcurrentHashMap {

    public static void main(String[] args) {

        ConcurrentHashMap<String, Integer> chm = new ConcurrentHashMap(16, 0.75f);
        chm.put("k1",1);
        chm.get("k1");



        // neuron store
        HashMap hashMap = new HashMap<>(4);
        hashMap.put("k1",1);
        hashMap.put("k2",2);
        hashMap.put("k3",3);

        hashMap.get("k1");

        Hashtable hashtable = new Hashtable(4);
        hashtable.put("h1","h");
        hashtable.get("h1");
    }
}
