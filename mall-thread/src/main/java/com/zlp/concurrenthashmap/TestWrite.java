package com.zlp.concurrenthashmap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Random;

public class TestWrite {

    public static void main(String[] args) {
//        String json= "{\"certNo\":\"510181198809091212\",\"createTime\":1603692617000,\"customerId\":\"15\",\"id\":1,\"inspectorName\":\"刘凤霞\",\"irisId\":\"ndasjhdgd\",\"machineNo\":\"M00001\",\"phone\":\"18616672199\",\"reportChannel\":1,\"reportNo\":\"LB00001\",\"riskValue\":1,\"username\":\"爱谁谁了\"}";
//        String FilePath = "D:\\file\\backtracking.txt";
//        writeFile(json,FilePath);

        Random r = new Random();
        for (int i = 0; i < 200; i++) {
            System.out.println(BigDecimal.valueOf((10 - Math.random() * 20)/100).setScale(4,BigDecimal.ROUND_HALF_UP));
        }

        /*for (int i = 1; i< 68; i++) {
            BigDecimal bigDecimal = BigDecimal.valueOf((1 - Math.random() * 1)).setScale(2, BigDecimal.ROUND_HALF_UP);
            System.out.println(bigDecimal);
        }*/

    }


    public static void writeFile(String json, String FilePath) {

        try {
            File file = new File(FilePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            } else {
                file.delete();
                file.createNewFile();
            }

            // true = append file
            FileWriter fileWritter = new FileWriter(file, false);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(json);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
