package com.zlp.countdownLatch;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j(topic = "TestCountDownLatch")
public class TestCountDownLatch {

    /**
     * 需求
     * 1.创建10个线程同步执行任务
     * 2.每个任务会有一个进度条
     * 3.当10个任务的进度条执行全部完成
     * 4.执行成功
     *
     */
    @SneakyThrows
    public static void main(String[] args) {

        CountDownLatch cdl = new CountDownLatch(10);
        AtomicInteger num = new AtomicInteger(0);
        ExecutorService service = Executors.newFixedThreadPool(10, (r) -> {
            return new Thread(r, "t" + num.getAndIncrement());
        });
        System.out.println("");
        Random random = new Random();
        String [] all = new String[10];
        for (int j = 0; j < 10 ; j++) {
            int x = j;
            service.execute(()->{
                for (int i = 1; i <= 100; i++) {
                    try {
                        Thread.sleep(random.nextInt(100));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    all[x] = Thread.currentThread().getName()+"(" + (i + "%") + ")";
                    System.out.print("\r" + Arrays.toString(all));
                }
                cdl.countDown();
            });
        }
        cdl.await();
        service.shutdown();
        System.out.println("\n游戏开始......");
        System.out.println("");
    }

}
