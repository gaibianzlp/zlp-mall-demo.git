package com.zlp.entity;

import lombok.Data;

@Data
public class ContentStatics {

    private Long clickCount;
    private Long upCount;
    private Long shareCount;
    private Long commentCount;
    private Long collectCount;

    public Long getClickCount() {
        return clickCount;
    }

    public Long getUpCount() {
        return upCount;
    }

    public Long getShareCount() {
        return shareCount;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public Long getCollectCount() {
        return collectCount;
    }
}
