package com.zlp.jvm;

public class TestFrames {

    public static void main(String[] args) {
        int x = 10;
        int i = method1(x);
        System.out.println(i);
    }

    private static int method1(int x) {
        int y = x+1;
        return method2(y);
    }

    private static int method2(int y) {

        return y+2;
    }
}
