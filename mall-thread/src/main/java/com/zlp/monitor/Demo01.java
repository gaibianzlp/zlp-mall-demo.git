package com.zlp.monitor;

public class Demo01 {

    private static Object lock = new Object();

    public static void main(String[] args) {

        synchronized (lock) {
            System.out.println("1");
        }
    }

    public synchronized void test1(){
        System.out.println("a");

        try {

        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
    }
}
