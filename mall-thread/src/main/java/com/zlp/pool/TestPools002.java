package com.zlp.pool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j(topic = "TestPools002")
public class TestPools002 {

    public static void main(String[] args) throws InterruptedException {

//        test01();
        for (int i = 0; i < 500001; i++) {
            
            String format = "%05d";
            System.out.println(startFormat(format));
        }

    }

    private static void test01() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        CountDownLatch cdl = new CountDownLatch(10);
        log.info("执行任务start.....");
        for (int i = 0; i < 10; i++) {
            int j = i;
            executorService.submit(()->{
                try {
                    log.info(Thread.currentThread().getName()+"==>"+j);
                    Thread.sleep(2000);
                    cdl.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        cdl.await();
        log.info("执行任务end.....");
    }

    private final static AtomicInteger atomic = new AtomicInteger(0);
    public static String startFormat(String format) {
        return String.format(format, atomic.incrementAndGet());
    }
}
