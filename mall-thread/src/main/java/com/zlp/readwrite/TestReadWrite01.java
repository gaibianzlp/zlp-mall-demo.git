package com.zlp.readwrite;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j(topic = "TestReadWrite01")
public class TestReadWrite01 {

    public static void main(String[] args) throws InterruptedException {

        DataContainer dataContainer = new DataContainer();
        new Thread(dataContainer::write, "t2").start();

        Thread.sleep(1000);
        new Thread(() -> {
            Object data = dataContainer.read();
            log.info("data={}", data);
        }, "t1").start();

    }
}


@Slf4j(topic = "DataContainer")
class DataContainer {


    private Object data;
    private ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();    //读锁
    private final Lock w = rwl.writeLock();    //写锁


    @SneakyThrows
    protected Object read() {

        log.info("读取数据...");
        try {
            log.info("正在读取....");
            Thread.sleep(1000);
            r.lock();
        } finally {
            r.unlock();
            log.info("读取释放数据...");
        }
        return data;
    }


    @SneakyThrows
    protected Object write() {

        log.info("写入数据...");
        try {
            w.lock();
            log.info("正在写入....");
            Thread.sleep(1000);
            data = "我们都是好孩子！";
        } finally {
            w.unlock();
            log.info("写入释放数据...");
        }
        return data;
    }

}

   /* 单词 说明
        exclusive 独占，排它锁   exclusive exclusive
        acquire 获得   acquire  qcquire
        interrupt 打断  interrupt  interrupt
        special 特殊的   special  special
        waiter 等待着，服务员 waiter
        shared 共享   shared  shared
        propagate 传播  propagation
        release 释放   release
        park n.推迟 v.推迟  park
        signal 信号量  signal  signal signal
        condition 条件 condition  condition
        cancelled 取消    cancelled  cancelled */
