package com.zlp.rennertant;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "Rennertant001")
public class Rennertant001 {

    private static  Lock lock = new ReentrantLock();
    public static void main(String[] args) {
        method01();
    }

    private static void method01() {

        try {
            lock.lock();
            log.info("enter method01....");
            method02();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private static void method02() {

        try {
            lock.lock();
            log.info("enter method02....");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }
}
