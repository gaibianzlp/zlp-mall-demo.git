package com.zlp.semaphore;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Semaphore;


@Slf4j(topic = "TestPoolSemaphore")
public class TestPoolSemaphore {

    public static void main(String[] args) {

        // 1.创建一个Semaphore 对象
        Semaphore semaphore = new Semaphore(3);

        // 创建10个线程执行
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();
                    log.info("runnig......");
                    Thread.sleep(1000);
                    log.info("end......");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            }).start();
        }
    }



}
