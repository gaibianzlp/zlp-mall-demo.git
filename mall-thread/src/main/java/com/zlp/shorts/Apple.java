package com.zlp.shorts;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Apple{
    private String color;
    private String name;
    private Integer size;
    private Integer order;
}
