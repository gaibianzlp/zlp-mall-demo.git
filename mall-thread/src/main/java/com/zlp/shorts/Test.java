package com.zlp.shorts;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {

        List<Apple> apples = new ArrayList<>();
        Apple apple = new Apple("black","apple",20,3);
        Apple orange = new Apple("orange","orange",30,0);
        Apple blue = new Apple("blue","blue",50,1);
        Apple yellow = new Apple("yellow","yellow",55,1);
        Apple red = new Apple("red","red",60,1);
        apples.add(apple);
        apples.add(orange);
        apples.add(blue);
        apples.add(red);
        apples.add(yellow);
//        Comparator<Apple> getOrder = Comparator.comparing(Apple::getOrder).reversed();
//        Comparator<Apple> bySizeDesc = Comparator.comparing(Apple::getSize).reversed();
//        apples.sort(getOrder.thenComparing(bySizeDesc)); // 先以名称升序排列，再按照size倒叙排列
//        apples.sort(getOrder); // 先以名称升序排列，再按照size倒叙排列 热度分  BASE_POPULARITY_SCORE
        // --------------------------------------------//
        apples = apples.stream().sorted(Comparator.comparing(Apple::getSize).reversed()).collect(Collectors.toList());
        apples = apples.stream().sorted(Comparator.comparing(Apple::getOrder).reversed()).collect(Collectors.toList());
        for (Apple apple1 : apples) {
            System.out.println(apple1.toString());

        }

//        Apple(color=black, name=apple, size=20, order=3)
//        Apple(color=blue, name=blue, size=50, order=1)
//        Apple(color=red, name=red, size=60, order=1)
//        Apple(color=orange, name=orange, size=30, order=0)
    }

}
