package com.zlp.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TimeClient {

    public static void main(String[] args){
        int port = 8090;
        if(args != null && args.length > 0){
            try{
                port = Integer.valueOf(args[0]);
            }catch(NumberFormatException e){

            }
        }
        Socket socket = null;
        BufferedReader in = null;
        PrintWriter out = null;
        try{
            socket = new Socket("127.0.0.1",port);
//            输入流，获取服务端输出流信息
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            输出流，放到服务端输入流中
            out = new PrintWriter(socket.getOutputStream(),true);
//            发送信息到服务端
            out.println("QUERY TIME ORDER");
            System.out.println("Send order 2 server succeed.");
//            读取输入流的信息
            String resp = in.readLine();
            System.out.println("Now is : " + resp);
        }catch(Exception e){

        }finally{
            if(out != null){
                out.close();
                out = null;
            }
            if(in != null){
                try{
                    in.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
                in = null;
            }
            if(socket != null){
                try{
                    socket.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
                socket = null;
            }
        }
    }
}
