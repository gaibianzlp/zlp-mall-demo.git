package com.zlp.socket;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

@Slf4j(topic = "TimeServer")
public class TimeServer {

    public static void main(MysqlxDatatypes.Scalar.String[] args) throws IOException {
        int port = 8090;
        ServerSocket server = null;
        try {
            // 如果端口合法且没有被占用，服务端监听成功
            server = new ServerSocket(port);
            log.info("The time server is start in port:" + port);
            Socket socket;
            while (true) {
                // 如果没有客户端接入，则主线程阻塞在ServerSocket的accept操作上
                socket = server.accept();
                new Thread(new TimeServerHandler(socket)).start();
            }
        } finally {
            if (server != null) {
                System.out.println("The time server close");
                server.close();
            }
        }
    }
}


@Slf4j(topic = "TimeServerHandler")
class TimeServerHandler implements Runnable {

    private Socket socket;


    public TimeServerHandler(Socket socket) {
        super();
        this.socket = socket;
    }

    @Override
    public void run() {

        BufferedReader in = null;
        PrintWriter out = null;
        try {
//            输入流，获取客户端输出流信息
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
//            输出流，放到客户端输入流中
            out = new PrintWriter(this.socket.getOutputStream(), true);
            String currentTime;
            String body;
            while (true) {
//                获取客户端输出的信息
                body = in.readLine();
                if (body == null) {
                    break;
                }
                System.out.println("The time server receive order : " + body);
                currentTime = "QUERY TIME ORDER".equalsIgnoreCase(body) ? "currentTime:"+new Date(System.currentTimeMillis()).toString() : "BAD ORDER";
//                发送信息到客户端输入流中
                out.println(currentTime);
            }
        } catch (Exception e) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (out != null) {
                out.close();
            }
            if (this.socket != null) {
                try {
                    this.socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                this.socket = null;
            }
        }
    }
}
