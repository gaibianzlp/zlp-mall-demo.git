package com.zlp.steam;

import lombok.Data;

import java.io.Serializable;

/**
 * 内容统计信息
 * @author zouLiPing
 * @date 2020年8月18日10:53:22
 *
 */
@Data
public class ContentStatisticsResp implements Serializable{

	private Long clickCount = 0L;

	private Long favorCount = 0L;

	private Long shareCount = 0L;

	private Long commentCount = 0L;

	private Long upCount =0L;

	private Long fansCount = 0L;

}
