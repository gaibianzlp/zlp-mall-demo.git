package com.zlp.steam;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

@Data
public class Emp {

    private Integer id;
    private String userName;
    private String address;
    private Integer age;
    private Date birthday;


    public Emp(Integer id, String userName, String address, Integer age, Date birthday) {
        this.id = id;
        this.userName = userName;
        this.address = address;
        this.age = age;
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;//地址相等
        }

        if(obj == null){
            return false;//非空性：对于任意非空引用x，x.equals(null)应该返回false。
        }

        if(obj instanceof Emp){
            Emp other = (Emp) obj;
            //需要比较的字段相等，则这两个对象相等
            if(equalsStr(this.userName, other.userName)
                    && equalsStr(String.valueOf(this.age), String.valueOf(other.age))){
                return true;
            }
        }

        return false;
    }

    private boolean equalsStr(String str1, String str2){
        if(StringUtils.isEmpty(str1) && StringUtils.isEmpty(str2)){
            return true;
        }
        if(!StringUtils.isEmpty(str1) && str1.equals(str2)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (userName == null ? 0 : userName.hashCode());
        result = 31 * result + (age == null ? 0 : age.hashCode());
        return result;
    }
}
