package com.zlp.steam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author cx.hao
 * @date 2019/6/26 15:29
 */
public class ListOrder {
    public static void main(String[] args) {

        List<String> orderRegulation = Arrays.asList("G105","G102", "G103", "G108", "G109");
        List<Posts> targetList = new ArrayList<Posts>();

        Posts post3 = new Posts();
        post3.setId("G103");
        post3.setName("xiaoli3");
        post3.setAge("23");

        Posts post2 = new Posts();
        post2.setId("G102");
        post2.setName("xiaoli2");
        post2.setAge("22");

        Posts post4 = new Posts();
        post4.setId("G108");
        post4.setName("xiaoli4");
        post4.setAge("25");

        Posts post7 = new Posts();
        post7.setId("G109");
        post7.setName("xiaoli7");
        post7.setAge("21");



        Posts post5 = new Posts();
        post5.setId("G105");
        post5.setName("xiaoli5");
        post5.setAge("27");


        targetList.add(post2);
        targetList.add(post3);
        targetList.add(post4);
        targetList.add(post5);
        targetList.add(post7);
      /*  System.out.println("排列前的数据：");
        targetList.forEach(t -> System.out.print(t.getId() + t.getName() + "~" + t.getAge() + "  "));
        System.out.println();*/

        setListOrder(orderRegulation, targetList);

        System.out.println("排序的规则：");
        orderRegulation.forEach(t -> System.out.print(t + " "));
        System.out.println();
        System.out.println("排列后的数据：");
        targetList.forEach(t -> System.out.print(t.getId() ));

    }

    //平时排序可使用其中一种，下面是综合两个条件排序
    public static void setListOrder(List<String> orderRegulation, List<Posts> targetList) {
        //按照Posts的Id来排序
        Collections.sort(targetList, ((o1, o2) -> {
            int io1 = orderRegulation.indexOf(o1.getId());
            int io2 = orderRegulation.indexOf(o2.getId());

            if (io1 != -1) {
                io1 = targetList.size() - io1;
            }
            if (io2 != -1) {
                io2 = targetList.size() - io2;
            }

            return io2 - io1;
        }));

       /* //按照Posts的age来排序
        Collections.sort(targetList, ((o1, o2) -> {
            int io1 = orderRegulation.indexOf(o1.getAge());
            int io2 = orderRegulation.indexOf(o2.getAge());

            if (io1 != -1) {
                io1 = targetList.size() - io1;
            }
            if (io2 != -1) {
                io2 = targetList.size() - io2;
            }

            return io2 - io1;
        }));*/
    }
}
