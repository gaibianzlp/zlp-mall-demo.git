package com.zlp.steam;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author cx.hao
 * @date 2019/6/26 15:55
 */
public class SetSort {

    /**
     * 设置顺序
     *
     * @Des 一个list orderRegulation设置顺序，另一个list targetList按照规则排列自身的顺序，
    如果targetList的数据比 orderRegulation的数据多，多出来的那部分数据，按照原来的顺序，且排在开头
    结果即 7 9 6 8（未设置顺序的部分） +  0 1 2 3 4 5（排序部分）
     @适用点： 1. 数据内容一致，只是顺序不同
     2. orderRegulation的数据内容 包含 targetList的内容
      * @param orderRegulation
     * @param targetList
     */
    public static void setListOrder(List<Integer> orderRegulation, List<Integer> targetList){
        Collections.sort(targetList,((o1, o2) -> {
            int io1 = orderRegulation.indexOf(o1);
            int io2 = orderRegulation.indexOf(o2);
            return io1 - io2;
        }));

        System.out.println("规则数据：");
        orderRegulation.forEach(t-> System.out.print(t+" " ));
        System.out.println();
        System.out.println("排列后的数据：");
        targetList.forEach(t-> System.out.print(t+" " ));
    }

    public static void sortUp(List<Integer> list){
        if (null != list && !list.isEmpty()){
            Collections.sort(list, Comparator.comparingInt(x -> x));

            System.out.println("\n sortUp 小-大");
            list.forEach(t-> System.out.print(t+" " ));
        }
    }

    public static void sortDown(List<Integer> list){
        if (null != list && !list.isEmpty()){
            Collections.sort(list, (x, y)-> y - x );

            System.out.println("\n sortDown 大-小");
            list.forEach(t-> System.out.print(t+" " ));
        }
    }

    public static void main(String[] args) {
        List<Integer> orderRegulation = Arrays.asList(6,1,2,3,4,5);
        List<Integer> targetList  = Arrays.asList(1,3,5,7,9,2,4,6,8,0);

        SetSort.setListOrder(orderRegulation,targetList);

        sortUp(targetList);

        sortDown(targetList);
    }
}
