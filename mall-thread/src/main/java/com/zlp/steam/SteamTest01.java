package com.zlp.steam;

import com.alibaba.fastjson.JSON;
import lombok.SneakyThrows;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class SteamTest01 {

    @SneakyThrows
    public static void main(String[] args) {


//        filterData();
//        distinct();
        sorted();
//        skip();

//        listToMap();
//        List<List<?>> lists = splitList(empList, 3);
//        System.out.println(JSON.toJSONString(lists));
//        Double aDouble = new Double(101);f
//        System.out.println(aDouble/20);
       /* List<Integer> counter = new ArrayList<>();
        for (int i = 1; i <21000 ; i++) {
            counter.add(20*i);
        }*/
        groupingBy();

    }

    protected  static List<Emp> empList = new ArrayList<>();

    static {
        empList.add(new Emp(2,"test02","杨浦区",23,new Date()));
        empList.add(new Emp(1,"test01","浦东新区",23,new Date()));
        empList.add(new Emp(3,"test03","黄埔区",24,new Date()));
        empList.add(new Emp(4,"test04","宝山区",24,new Date()));
        empList.add(new Emp(6,"test06","青浦区",26,new Date()));
        empList.add(new Emp(5,"test05","青浦区",26,new Date()));
    }

    public static void filterData(){




        List<Emp> emps = empList.stream().filter(emp -> emp.getAddress().equals("黄埔区")).collect(Collectors.toList());
        emps.forEach(emp -> {
            System.out.println(emp.toString());
        });
    }

    public static void distinct(){

        empList.stream().distinct().collect(Collectors.toList()).forEach(emp ->{
            System.out.println(emp.toString());
        });

      /*  List<Integer> integers = Arrays.asList(3, 3, 323, 31, 2, 1);
        integers.stream().distinct().forEach(integer -> {
            System.out.println(integer);
        });*/

    }

    public static void sorted(){
     /*   empList.stream().map(emp -> emp.getAge()).sorted().forEach( emp -> {
            System.out.println(emp.toString());
        });*/

     /*   empList.stream().map(emp -> emp.getAge()).re.forEach( emp -> {
//            System.out.println(emp.toString());
        });*/
        List<Emp> studentsSortName = empList.stream().sorted(Comparator.comparing(Emp::getAge).reversed()).collect(Collectors.toList());
        for (Emp emp : studentsSortName) {
            System.out.println(emp.toString());
        }

       /* empList.stream().map(emp -> {
            Emp emp1 = new Emp("TEST09","闵行区",23,new Date());
            return emp1;
        }).distinct().forEach(address -> System.out.println(address.toString()));*/
    }

    public static void skip(){
        List<Emp> collect = empList.stream().skip(2).collect(Collectors.toList());
        for (Emp emp : collect) {
            System.out.println(emp.toString());
        }
    }

    public static void listToMap() throws Exception {
        Map<Integer, Emp> map = empList.stream().collect(Collectors.toMap(Emp::getId, emp -> emp));
        Emp emp = map.get(1);
        if (Objects.nonNull(map.get(3))){
            System.out.println("erwerwre");
        }
        System.out.println(emp.toString());

        Emp emp1 = new Emp(3, "test03", "黄埔区", 20, new Date());
        int i = empList.indexOf(emp1);
        System.out.println(i+1);

        throw new Exception("FSFEF");



    }


    /**
     * 按指定大小，分隔集合，将集合按规定个数分为n个部分
     *
     * @param list
     * @param len
     * @return
     */
    public static List<List<?>> splitList(List<?> list, int len) {

        if (list == null || list.size() == 0 || len < 1) {
            return null;
        }
        List<List<?>> result = new ArrayList<>();
        int size = list.size();
        int count = (size + len - 1) / len;

        for (int i = 0; i < count; i++) {
            List<?> subList = list.subList(i * len, ((i + 1) * len > size ? size : len * (i + 1)));
            result.add(subList);
        }
        return result;
    }

    public static void groupingBy(){

//  list.stream().collect(Collectors.groupingBy(CommentReport::getCommentId, Collectors.counting()));
//        R collect = empList.stream().collect(groupingBy(Emp::getUserName));
////        System.out.println(emps);

        List<Integer> collect = empList.stream().map(Emp::getAge).distinct().collect(Collectors.toList());


        ArrayList<Emp> collect1 = empList.stream().collect(Collectors.collectingAndThen(
                Collectors.toCollection(() -> new TreeSet<>(
                        Comparator.comparing(
                                Emp::getAge))), ArrayList::new));

        for (Emp emp : collect1) {
            System.out.println(emp);
        }
    }

    public BigDecimal geBigDecimal(){

       /* double vcmaxaAvg = pupilTestVOS.stream().map(PupilTestVO::getVCmax).collect(Collectors.toList()).stream().
                mapToDouble(BigDecimal::doubleValue).average().getAsDouble();*/

   /*     double sumScore = reports.stream().map(Report::getScore).collect(Collectors.toList()).stream().
                mapToDouble(BigDecimal::doubleValue).sum();*/

       return null;
    }


}
