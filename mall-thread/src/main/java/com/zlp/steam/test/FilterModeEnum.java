package com.zlp.steam.test;

/**
 *
 * ==============================================
 * Copy right 2015-2018 by Jalan
 * ----------------------------------------------
 * This is not a free software, without any authorization is not allowed to use and spread.
 * ==============================================
 *
 * @author : Jalan
 * @version : v1.0.0
 * @desc : 查询选项
 * @since : 2018/2/7 19:28
 */
public enum FilterModeEnum {
    startsWith ,
    equals,
    contains
}