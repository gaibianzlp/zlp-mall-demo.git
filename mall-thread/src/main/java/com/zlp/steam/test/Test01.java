package com.zlp.steam.test;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Test01 {

    public static void main(String[] args) {
        List<TestDataVO> data = new ArrayList<>();
        TestDataVO td1 = new TestDataVO();
        td1.setCode("测试A-code");
        td1.setName("测试A-name");
        TestDataVO td2 = new TestDataVO();
        td2.setCode("测试B-code");
        td2.setName("测试B-name");
        TestDataVO td3 = new TestDataVO();
        td3.setCode("测试C-code");
        td3.setName("测试C-name");
        TestDataVO td4 = new TestDataVO();
        td4.setCode("测试D-code");
        td4.setName("测试D-name");
        TestDataVO td5 = new TestDataVO();
        td5.setCode("测试E-code");
        td5.setName("测试E-name");

        data.add(td1);
        data.add(td2);
        data.add(td3);
        data.add(td4);
        data.add(td5);

        List<String> filterCodeCondition = new ArrayList<>();
        filterCodeCondition.add("测试A-code");
        filterCodeCondition.add("测试C-code");
        /**
         * 过滤结果为 td1,td3 对象
         */
        List<TestDataVO> filterRes = new ArrayList<>();
        for (TestDataVO datum : data) {
            if (filterCodeCondition.contains(datum.getCode())) {
                filterRes.add(datum);
            }
        }
        filterRes.forEach(d -> {
            System.out.println(d.toString());
        });

        List<TestDataVO> filterResult = queryArray(data, filterCodeCondition, TestDataVO::getCode, FilterModeEnum.startsWith);

//        List<String> filterNameCondition = new ArrayList<>();
//        filterCodeCondition.add("D-name");
//        filterCodeCondition.add("E-name");
//        /**
//         * 过滤结果为 td4,td5 对象
//         */
//        List<TestDataVO> filterResult1 = queryArray(data, filterCodeCondition, TestDataVO::getName, FilterModeEnum.contains);
    }

    /**
     * 通过批量过滤条件（codes） 过滤指定对象属性的值
     *
     * @param dataSource 数据源集合
     * @param codes      过滤条件集合
     * @param express    过滤集合对象目标属性表达式
     * @param filterMode 过滤方式
     * @return 返回符合过滤条件codes的对象集合
     */
    public static List<TestDataVO> queryArray(List<TestDataVO> dataSource, List<String> codes,
                                              ObjectPropertyPredicate<TestDataVO> express, FilterModeEnum filterMode) {

        List<TestDataVO> result = new ArrayList<>();
        if (dataSource == null) {
            return result;
        }
        if (codes == null || (long) codes.size() == 0) {
            return dataSource;
        }
        if (express == null) {
            return dataSource;
        }
        if (filterMode == FilterModeEnum.startsWith) {
            Predicate<TestDataVO> expressOr = f -> false;
            for (String code : codes) {
                Predicate<TestDataVO> press = (w) -> express.getProperty(w).toString().startsWith(code);
                expressOr = expressOr.or(press);
            }
            result = dataSource.stream().filter(expressOr).collect(Collectors.toList());
        } else if (filterMode == FilterModeEnum.contains) {
            result = dataSource.stream().filter(f -> codes.contains(express.getProperty(f).toString())).collect(Collectors.toList());
        } else if (filterMode == FilterModeEnum.equals) {
            Predicate<TestDataVO> expressOr = f -> false;
            for (String code : codes) {
                Predicate<TestDataVO> press = (w) -> StringUtils.equals(express.getProperty(w).toString(), code);
                expressOr = expressOr.or(press);
            }
            result = dataSource.stream().filter(expressOr).collect(Collectors.toList());
        }
        return result;
    }
}
