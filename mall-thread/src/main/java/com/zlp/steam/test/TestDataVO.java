package com.zlp.steam.test;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * ==============================================
 * Copy right 2015-2018 by Jalan
 * ----------------------------------------------
 * This is not a free software, without any authorization is not allowed to use and spread.
 * ==============================================
 *
 * @author : Jalan
 * @version : v1.0.0
 * @desc : 测试实体类
 * @since : 2018/2/7 11:58
 */
@Data
@EqualsAndHashCode
public class TestDataVO{

    private String code;
    private String name;
    private String remark;
}
