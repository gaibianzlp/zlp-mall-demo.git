package com.zlp.synchronization;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SyncTest2 {

     static Object lock = new Object();
     static int counter = 0;


    @SneakyThrows
    public static void main(String[] args) {

        synchronized (lock){
            counter++;
        }
        log.info("counter={}",counter);
    }
}


 /**


     public static void main(java.lang.String[]);
    descriptor: ([Ljava/lang/String;)V
            flags: ACC_PUBLIC, ACC_STATIC
            Code:
            stack=3, locals=3, args_size=1
            0: getstatic     #2                  // Field lock:Ljava/lang/Object;
            3: dup
            4: astore_1
            5: monitorenter
            6: getstatic     #3                  // Field counter:I
            9: iconst_1
            10: iadd
            11: putstatic     #3                  // Field counter:I
            14: aload_1
            15: monitorexit
            16: goto          24
            19: astore_2
            20: aload_1
            21: monitorexit
            22: aload_2
            23: athrow
            24: getstatic     #4                  // Field log:Lorg/slf4j/Logger;
            27: ldc           #5                  // String counter={}
            29: getstatic     #3                  // Field counter:I
            32: invokestatic  #6                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            35: invokeinterface #7,  3            // InterfaceMethod org/slf4j/Logger.info:(Ljava/lang/String;Ljava/lang/Object;)V
            40: goto          46
            43: astore_1
            44: aload_1
            45: athrow
            46: return
            Exception table:
            from    to  target type
            6    16    19   any
            19    22    19   any
            0    40    43   Class java/lang/Throwable
            LocalVariableTable:
            Start  Length  Slot  Name   Signature
            44       2     1   $ex   Ljava/lang/Throwable;
            0      47     0  args   [Ljava/lang/String;
            LineNumberTable:
            line 16: 0
            line 17: 6
            line 18: 14
            line 19: 24
            line 13: 40
            line 20: 46
            StackMapTable: number_of_entries = 4
            frame_type = 255 *//* full_frame *//*
            offset_delta = 19
            locals = [ class "[Ljava/lang/String;", class java/lang/Object ]
        stack = [ class java/lang/Throwable ]
        frame_type = 250 *//* chop *//*
        offset_delta = 4
        frame_type = 82 *//* same_locals_1_stack_item *//*
        stack = [ class java/lang/Throwable ]
        frame_type = 2 *//* same *//*
        MethodParameters:
        Name                           Flags
        args
*/
