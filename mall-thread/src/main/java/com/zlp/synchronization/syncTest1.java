package com.zlp.synchronization;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class syncTest1 {

    private static int counter = 0;
    private static Object lock = new Object();
    private static Object lock2 = new Object();


    @SneakyThrows
    public static void main(String[] args) {

        Thread t1 = new Thread(()->{

            for (int i = 0; i <1000 ; i++) {
                synchronized (lock){
                    counter++;
                }
            }
        });

        Thread t2 = new Thread(()->{

            for (int i = 0; i <1000 ; i++) {
                synchronized (lock){
                    counter--;
                }
            }
        });

        t1.start();
        t2.start();
        t1.join();
//        t2.join();
        log.info("counter={}",counter);
    }
}
