package com.zlp.synchronize;

import lombok.extern.slf4j.Slf4j;

/**
 * TODO
 * 1. 先创建一个Runnable
 * 2. 获取到锁
 * 3. 让线程处于阻塞状态
 * 4. 开启线程
 * 5. 新建一个线程中断
 * @date: 2020/10/28 16:12
 * @return:
 */
@Slf4j (topic = "InterruptTest02")
public class InterruptTest02 {

    private static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {

        // 1. 先创建一个Runnable
        Runnable runnable = () -> {
            synchronized (lock) {
                // 2. 获取到锁
                String name = Thread.currentThread().getName();
                log.info("当前{}进入同步代码块",name);
                // 3. 让线程处于阻塞状态
                try {
                    Thread.sleep(8888888L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        // 4. 开启线程
        Thread thread1 = new Thread(runnable);
        thread1.start();
        // 主线程睡眠1000ms
        Thread.sleep(1001L);

        Thread thread2 = new Thread(runnable);
        thread2.start();
        // thread2 中断线程
        log.info("thread2停止线程前状态{}",thread2.getState());
        thread2.interrupt();

        log.info("thread1线程状态{}",thread1.getState());
        log.info("thread2线程状态{}",thread2.getState());


    }
}
