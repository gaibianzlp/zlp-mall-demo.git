package com.zlp.synchronize;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "SyncTest01")
public class SyncTest01 {

    private static Object lock = new Object();

    public static void main(String[] args) {

        test01();
    }

    private static void test01() {

        new Thread(()->{
            synchronized (lock) {
                log.info("test01 当前线程名称为={}",Thread.currentThread().getName());
                test02();
            }

        },"t1").start();
    }

    private static void test02() {

            synchronized (lock) {
                log.info("test02 当前线程名称为={}",Thread.currentThread().getName());
            }

    }
}
