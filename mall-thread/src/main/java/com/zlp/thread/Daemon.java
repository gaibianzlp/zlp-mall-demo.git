package com.zlp.thread;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Daemon {

    @SneakyThrows
    public static void main(String[] args) {

        Thread thread = new Thread(() -> {

            log.info(" start....");
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
            }
            log.info(" end....");
        },"daemon");
        log.info(" execute....");
        long start = System.currentTimeMillis();
//        thread.setDaemon(true);
        thread.wait();
        thread.start();
        Thread.sleep(1000);
        long end = System.currentTimeMillis();
        log.info("时间相差={}",end-start);
    }
}
