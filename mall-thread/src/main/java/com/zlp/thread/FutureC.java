package com.zlp.thread;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
@Slf4j(topic = "FutureC")
public class FutureC {

    @SneakyThrows
    public static void main(String[] args) {


        FutureTask<Integer> futureTask = new FutureTask<>(new Callable<Integer>() {

            @Override
            public Integer call() throws Exception {
                log.info(" Runinng.....");
                Thread.sleep(1000);
                return 100;
            }
        });

        Thread thread = new Thread(futureTask,"t1");
        thread.start();
        Integer integer = futureTask.get();
        System.out.println(integer);
    }
}
