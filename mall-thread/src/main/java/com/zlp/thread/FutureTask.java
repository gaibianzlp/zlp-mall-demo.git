package com.zlp.thread;

import com.zlp.entity.ContentStatics;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class FutureTask {

    private static ExecutorService executor = Executors.newFixedThreadPool(16);

    @SneakyThrows
//    @Transactional(rollbackFor = Exception.class)
    public static void main(String[] args) {
        
        ContentStatics contentStatics = new ContentStatics();
        ContentStatics contentStatics1 = new ContentStatics();
        long start = System.currentTimeMillis();

        CompletableFuture<Void> clickFuture = CompletableFuture.runAsync(()->{
            Long clickCount = clickCount();
            contentStatics.setClickCount(clickCount);
        },executor);
        CompletableFuture<Void> upFuture = CompletableFuture.runAsync(()->{
            Long upCount = upCount();
            contentStatics.setUpCount(upCount);
        },executor);
        CompletableFuture<Void> shareFuture = CompletableFuture.runAsync(()->{
            Long shareCount = shareCount();
            contentStatics.setShareCount(shareCount);
        },executor);
        CompletableFuture<Void> commentFuture = CompletableFuture.runAsync(()->{
            Long commentCount = commentCount();
            contentStatics.setCommentCount(commentCount);
        },executor);
        CompletableFuture<Void> collectFuture = CompletableFuture.runAsync(() -> {
            Long collectCount = collectCount();
            contentStatics.setCollectCount(collectCount);
        }, executor);


        // 全部执行完成一起提交
        CompletableFuture.allOf(clickFuture,upFuture,shareFuture,commentFuture,collectFuture).get();
        log.info("contentStatics={}",contentStatics.toString());
        long end = System.currentTimeMillis();
        log.info("执行时间===>time:{}",end-start);



        // 没有异步执行
        Long clickCount = clickCount();
        contentStatics1.setClickCount(clickCount);
        Long upCount = upCount();
        contentStatics1.setUpCount(upCount);
        Long shareCount = shareCount();
        contentStatics1.setShareCount(shareCount);
        Long commentCount = commentCount();
        contentStatics1.setCommentCount(commentCount);
        Long collectCount = collectCount();
        contentStatics1.setCollectCount(collectCount);
        log.info("contentStatics1={}",contentStatics1.toString());
        long end1 = System.currentTimeMillis();
        log.info("执行时间===>time:{}",end1-end);




    }

    @SneakyThrows
    private static Long collectCount() {
        Thread.sleep(700L);
        return 100L;
    }

    @SneakyThrows
    private static Long commentCount() {
        Thread.sleep(1000L);
        return 100L;
    }

    @SneakyThrows
    private static Long shareCount() {
        Thread.sleep(500L);
        return 100L;
    }

    @SneakyThrows
    private static Long upCount() {
        Thread.sleep(800L);
        return 100L;
    }

    @SneakyThrows
    private static Long clickCount() {
        Thread.sleep(100L);
        return 100L;
    }
}
