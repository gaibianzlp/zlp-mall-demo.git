package com.zlp.thread;

import com.alibaba.fastjson.JSON;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class GuardedObject {

    private  Object response;
    private final Object lock = new Object();

    /**
     * 获取结果方法
     * @date: 2020/10/21 11:28
     * @return: java.lang.Object
     */
    public Object get() {
        synchronized (lock) {
            // 条件不满足则等待
            while (response == null) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return response;
        }
    }

    /**
     * 产生结果方法
     * @date: 2020/10/21 11:28
     * @return: java.lang.Object
     */
    public void complete(Object response) {

        synchronized (this) {
            this.response = response;
            lock.notifyAll();
        }
    }

    @SneakyThrows
    public static List<String> download(){
        Thread.sleep(1000);
        return   Arrays.asList("t1", "t2", "t2");
    }

    public static void main(String[] args) {

        /*GuardedObject guardedObject = new GuardedObject();

        new Thread(()->{
            log.info("执行下载.......");
            guardedObject.response = dowlowd();
            guardedObject.complete(response);


        },"t1").start();

        new Thread(()->{

            log.info("获取结果....");
            response =  guardedObject.get();
            log.info("结果 response={}",JSON.toJSONString(response));
        },"t2").start();
*/
        GuardedObject guardedObject = new GuardedObject();
        new Thread(() -> {
                // 子线程执行下载
                List<String> response = download();
                log.debug("download complete...");
                guardedObject.complete(response);

        },"t1").start();
        log.debug("waiting...");

        // 主线程阻塞等待
        Object response = guardedObject.get();
        log.debug("get response: [{}] lines", JSON.toJSONString(response.toString()));
    }
}
