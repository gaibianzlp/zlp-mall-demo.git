package com.zlp.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestThread8Monitor {

    public static void main(String[] args) {



        /**
         * 题目：判断打印的 "one" or "two" ？
         *
         * 1. 两个普通同步方法，两个线程，标准打印， 打印? //two  one 并行执行
         * 2. 新增 Thread.sleep() 给 getOne() ,打印? //one  two  串行执行
         * 3. 新增普通方法 getThree() , 打印? //three  one   two  串行执行
         * 4. 两个普通同步方法，两个 Number 对象，打印?  //two  one
         * 5. 修改 getOne() 为静态同步方法，打印?  //two   one  并发执行
         * 6. 修改两个方法均为静态同步方法，一个 Number 对象?  //one   two 同一类对象
         * 7. 一个静态同步方法，一个非静态同步方法，两个 Number 对象?  //two  one  两个对象
         * 8. 两个静态同步方法，两个 Number 对象?   //one  two
         *
         * 线程八锁的关键：
         * ①非静态方法的锁默认为  this,  静态方法的锁为 对应的 Class 实例
         * ②某一个时刻内，只能有一个线程持有锁，无论几个方法。
         */

        Number number = new Number();
        Number number1 = new Number();
       /* Number number1 = new Number();
        number.getOne();
        number1.getTwo();*/

        new Thread(()->{
            number.getOne();
        }).start();

        new Thread(() -> {
            number.getTwo();
        }).start();

      /*  new Thread(() -> {
            number.getThree();
        }).start();*/
    }
}




@Slf4j
class Number {

    public /*static synchronized */void getOne() {//Number.class
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        log.info("one");
    }

    public  /*synchronized*/ void getTwo() {//this
        log.info("two");
    }

   /* public void getThree() {
        log.info("three");
    }*/
    // three two one
    // two three one
}
