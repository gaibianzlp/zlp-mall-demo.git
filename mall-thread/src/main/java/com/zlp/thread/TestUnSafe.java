package com.zlp.thread;


import java.util.ArrayList;
import java.util.List;

public class TestUnSafe {

    final static Integer NUM_COUNT = 100;
    final static Integer THREAD_COUNT = 2;


    public static void main(String[] args) {

        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(()->{
//                method1(NUM_COUNT);
                method01(NUM_COUNT);

            }).start();
        }

    }

    private static List<String> list = new ArrayList<>();

    public static void method01(int counter){
        List<String> list = new ArrayList<>();
        for (int i = 0; i < counter; i++) {
            method02(list);
            method03(list);
        }

    }

    public static void method02(List<String> list){
        list.add("1");
    }

    public static void method03(List<String> list){
        list.remove(0);
    }

    public static void method1(int counter){

        for (int i = 0; i < counter; i++) {
            method2();
            method3();
        }

    }

    public static void method2(){
        list.add("1");
    }

    public static void method3(){
        list.remove(0);
    }
}
