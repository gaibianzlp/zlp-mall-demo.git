package com.zlp.thread;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "ThreadSeelp")
public class ThreadSeelp {

    @SneakyThrows
    public static void main(String[] args) {

        execute();

    }

    public static void execute() throws InterruptedException {

        for (int i = 0; i < 100 ; i++) {
            Thread.sleep(10);
            log.info("execute time={}", i*100);
        }
        new Thread(()->{
            try {
                Thread.sleep(1000);
                log.info("execute time Therd2={}",100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }



}
