package com.zlp.thread;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadState {

    @SneakyThrows
    public static void main(String[] args) {



        Thread t1= new Thread(()->{
            log.info("running.......");
        });


        Thread t2= new Thread(()->{
            while (true) {

            }
        });
        t2.start();

        Thread t3= new Thread(()->{
            log.info("running....");
        });
        t3.start();


        Thread t4 = new Thread(()->{
            synchronized (ThreadState.class){
                try {
                    Thread.sleep(10000000L); //time_wait
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t4.start();

        Thread t5 = new Thread(()->{
            try {
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t5.start();

        Thread t6 = new Thread(()->{
            synchronized (ThreadState.class){
                try {
                    Thread.sleep(10000000L); //TIMED_WAITING
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t6.start();

        Thread.sleep(500L);

        log.info("t1-线程状态 state={}",t1.getState());
        log.info("t2-线程状态 state={}",t2.getState());
        log.info("t3-线程状态 state={}",t3.getState());
        log.info("t4-线程状态 state={}",t4.getState());
        log.info("t5-线程状态 state={}",t5.getState());
        log.info("t6-线程状态 state={}",t6.getState());



    }

}
