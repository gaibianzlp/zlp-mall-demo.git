package com.zlp.thread;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadTest {

    @SneakyThrows
    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            log.info(" start.....");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });

        System.out.println(thread.getState());
        thread.start();
        Thread.sleep(500);
        System.out.println(thread.getState());

    }
}
