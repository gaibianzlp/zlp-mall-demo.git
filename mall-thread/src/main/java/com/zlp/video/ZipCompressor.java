package com.zlp.video;


import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
@Slf4j
public class ZipCompressor {

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        String ffmpeg="E:\\ruanjian\\javatools\\ffmpeg\\ffmpeg\\bin\\ffmpeg.exe";
        String NewfilePath  = "D:\\video\\mego77.mp4";
        List fromVideoFileList=new ArrayList();
        fromVideoFileList.add("D:\\video\\1.mp4");
        fromVideoFileList.add("D:\\video\\2.mp4");
        ZipCompressor.convetor(fromVideoFileList, ffmpeg,NewfilePath );
        log.info("mergeVideo execute time sec==>time:{}",System.currentTimeMillis()-start);
    }




    /**
     *  ffmpeg合并多个视频文件
     * @param fromVideoFileList 需要合并的多视频url地址以List存放
     * @param ffmpeg  此处是ffmpeg 配置地址，可写死如“E:/ffmpeg/bin/ffmpeg.exe”
     * @param NewfilePath  合并后的视频存放地址，如：E:/mergevideo.mp4
     * @date: 2021/4/17 9:31
     * @return: void
     */
    public static void convetor(List<String> fromVideoFileList, String ffmpeg,
                                String NewfilePath) throws IOException {
        new Thread(
                () -> {
                    try {
                        List<String> voidTS = new ArrayList<>();
                        Process process = null;
                        ProcessBuilder builder = null;
                        List<String> command = null;
                        for (int i = 0; i < fromVideoFileList.size(); i++) {
                            builder = getProcessBuilder(fromVideoFileList, ffmpeg, voidTS, i);
                            try {
                                process = builder.start();
                                InputStream errorStream = process .getErrorStream();
                                InputStreamReader inputStreamReader = new InputStreamReader(errorStream);
                                BufferedReader br = new BufferedReader( inputStreamReader);
                                String line = "";
                                StringBuffer sb = new StringBuffer();
                                while ((line = br.readLine()) != null) {
                                    sb.append(line);
                                }
                                String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
                                Pattern pattern = Pattern.compile(regexDuration);
                                Matcher m = pattern.matcher(sb.toString());
                                System.out.println(sb.toString());
                                br.close();
                                inputStreamReader.close();
                                errorStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        List<String> dos = new ArrayList<>();
                        StringBuffer tsPath = getStringBuffer(ffmpeg, NewfilePath, voidTS);
                        Process pr = Runtime.getRuntime().exec(tsPath.toString());
                        process.getInputStream();
                        pr.getOutputStream().close();
                        pr.getInputStream().close();
                        pr.getErrorStream().close();
                        try {
                            pr.waitFor();
                            Thread.sleep(1000);
                            pr.destroy();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        //删除生成的ts文件
                        for (String filePath : voidTS) {
                            File file = new File(filePath);
                            file.delete();
                            pr.destroy();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
    }

    private static StringBuffer getStringBuffer(String ffmpeg, String NewfilePath, List<String> voidTS) {
        StringBuffer tsPath = new StringBuffer();
        tsPath.append(ffmpeg);
        tsPath.append(" -i ");
        tsPath.append("concat:");
        for (int t = 0; t < voidTS.size(); t++) {
            if (t != voidTS.size() - 1) {
                tsPath.append(voidTS.get(t) + "|");
            } else {
                tsPath.append(voidTS.get(t));
            }
        }
        tsPath.append(" -vcodec ");
        tsPath.append(" copy ");
        tsPath.append(" -bsf:a ");
        tsPath.append(" aac_adtstoasc ");
        tsPath.append(" -movflags ");
        tsPath.append(" +faststart ");
        tsPath.append(NewfilePath);
        return tsPath;
    }

    private static ProcessBuilder getProcessBuilder(List<String> fromVideoFileList, String ffmpeg, List<String> voidTS, int i) {
        List<String> command;
        ProcessBuilder builder;
        String fromVideoFile = fromVideoFileList.get(i);
        command = new ArrayList<String>();
        command.add(ffmpeg);
        command.add("-y");
        command.add("-i");
        command.add(fromVideoFile);
        command.add("-vcodec");
        command.add("copy");
        command.add("-bsf:v");
        command.add("h264_mp4toannexb");
        command.add("-f");
        command.add("mpegts");
        command.add(fromVideoFile.substring(0,
                fromVideoFile.lastIndexOf(".")) + ".ts");
        builder = new ProcessBuilder(command);
        voidTS.add(fromVideoFile.substring(0,
                fromVideoFile.lastIndexOf("."))
                + ".ts");
        return builder;
    }
}


