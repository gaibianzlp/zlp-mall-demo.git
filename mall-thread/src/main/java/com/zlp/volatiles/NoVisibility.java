package com.zlp.volatiles;

import lombok.SneakyThrows;

import java.sql.SQLOutput;

public class NoVisibility {

    private  static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread {

        NoVisibility noVisibility = new NoVisibility();

        int count = 1;
        @Override
        public void run() {
            System.out.println(ready);
            while (!ready) {
                System.out.println("before:"+ready);
//                Thread.yield();
                System.out.println("count"+count++);
            }
            System.out.println("after:"+ready);
            System.out.println(number);
        }
    }

    public  void read(){

            System.out.println(ready);
        new Thread(()->{
            while (!ready) {
                System.out.println("before:"+ready);
//                Thread.yield();
            }
            System.out.println("after:"+ready);
            System.out.println(number);

        }).start();
    }

    @SneakyThrows
    public static void main(String[] args) {
        new ReaderThread().start();
        NoVisibility noVisibility = new NoVisibility();
//        noVisibility.read();
        Thread.sleep(1000);
        System.out.println(ready);
        number = 42;
        noVisibility.ready = true;
    }
}
