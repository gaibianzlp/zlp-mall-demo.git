package com.zlp.volatiles;

import lombok.extern.slf4j.Slf4j;

/**
 * 可见性
 * @date: 2020/10/27 20:54
 */
@Slf4j(topic = "TestVisibility01")
public class TestVisibility01 {

    public static  Boolean flag = true;

    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            while (flag) {
                // 打印会自动退出当前线程
//                log.info("全局flag值为={}",flag);
            }

        }).start();

        Thread.sleep(1000);
        new Thread(()->{
            flag = false;
            log.info("修改全局flag值为={}",flag);

        }).start();

    }


}
