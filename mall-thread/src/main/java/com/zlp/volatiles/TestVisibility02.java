package com.zlp.volatiles;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 *  原子性
 * @date: 2020/10/27 20:54
 */
@Slf4j(topic = "TestVisibility02")
public class TestVisibility02 {

    public static int number = 0;

    public static void main(String[] args) throws InterruptedException {

        // number++
        Runnable inc = ()->{
            for (int i = 0; i < 1000; i++) {
                number++;
            }
        };
        List<Thread> threadList = new ArrayList<>();
        // 创建5线程同时相加
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(inc);
            thread.start();
            threadList.add(thread);
        }
        for (Thread thread : threadList) {
            thread.join();
        }

        log.info("number={}",number);

    }


}
