package com.zlp.volatiles;

import lombok.SneakyThrows;


public class VolatileDemo1 {

    private  volatile static boolean flag = true;

    @SneakyThrows
    public static void main(String[] args) {

        new Thread(()->{
            long start = System.currentTimeMillis();
             while (flag) {
//                 System.out.println(flag);
             }
             long end = System.currentTimeMillis();
             System.out.println("终止了while循环！flag的值为：" + flag);
             System.out.println("耗时：" + ( end -start ));

        }).start();
//        TimeUnit.SECONDS.sleep(2);
        flag = false;
    }

}
