package com.zlp.mybatis.auto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * https://blog.csdn.net/bin470398393/article/details/90633557
 * @date: 2022/3/31 10:16
 * @return:
 */
@SpringBootApplication
public class AutoApp {

    public static void main(String[] args) {
        SpringApplication.run(AutoApp.class,args);
    }
}
