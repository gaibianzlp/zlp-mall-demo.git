package com.zlp.mockito;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
public class MockitoApp {

    public static void main(String[] args) {
        SpringApplication.run(MockitoApp.class,args);
    }
}
