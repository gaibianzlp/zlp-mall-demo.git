package com.zlp.mockito.controller;


import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 管理用户表 前端控制器
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@Slf4j
public class MockitoTestController {






    /**
     * 测试方法
     * @param a
     * @param b
     * @author ZouLiPing
     * @date 2024/4/14 21:42
     * @return int
     */
     public  int add(int a, int b) {
         log.info("add方法被调用");
         return a + b;
     }








}

