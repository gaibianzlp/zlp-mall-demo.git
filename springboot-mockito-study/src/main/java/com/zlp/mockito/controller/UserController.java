package com.zlp.mockito.controller;


import com.zlp.mockito.dto.LoginUserReq;
import com.zlp.mockito.dto.LoginUserResp;
import com.zlp.mockito.dto.UserAddReq;
import com.zlp.mockito.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 管理用户表 前端控制器
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@Slf4j
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;




    /**
     *  登录
     */
    @PostMapping(value = "login")
    public LoginUserResp login(@Valid @RequestBody LoginUserReq loginReq){

        return userService.login(loginReq);
    }

    /**
     *  创建用户
     */
    @PostMapping(value = "createUser")
    public Boolean createUser(@Valid @RequestBody UserAddReq userAddReq){

        return userService.createUser(userAddReq);
    }








}

