package com.zlp.mockito.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@ApiModel(value = "添加用户请求参数")
public class UserAddReq {


    @NotBlank(message = "用户名为空!")
    @ApiModelProperty(value = "用户名", required = true)
    private String username;
    
    @NotBlank(message = "密码为空!")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @NotBlank(message = "邮箱为空!")
    @ApiModelProperty(value = "邮箱", required = true)
    private String email;

}
