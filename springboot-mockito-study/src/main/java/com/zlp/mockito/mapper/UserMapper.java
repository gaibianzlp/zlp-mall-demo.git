package com.zlp.mockito.mapper;


import com.zlp.mockito.dto.UserAddReq;
import com.zlp.mockito.entity.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 管理用户表 Mapper 接口
 * </p>
 *
 * @author LiPing.Zou
 * @since 2021-03-11
 */
@Component
public class UserMapper  {

    public User findUserByUsernameAndPassword(String username, String password) {
        return null;
    }

    public int insert(UserAddReq userAddReq) {
        return 1;

    }
}
