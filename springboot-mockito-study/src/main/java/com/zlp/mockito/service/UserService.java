package com.zlp.mockito.service;

import com.zlp.mockito.dto.LoginUserReq;
import com.zlp.mockito.dto.LoginUserResp;
import com.zlp.mockito.dto.UserAddReq;


/**
 *
 * @Classname UserService
 * @Date 2024/4/14 21:37
 * @Created by ZouLiPing
 */
public interface UserService {



    /**
     * 登录
     * @param loginReq
     * @return
     */
    LoginUserResp login(LoginUserReq loginReq);

    Boolean createUser(UserAddReq userAddReq);
}
