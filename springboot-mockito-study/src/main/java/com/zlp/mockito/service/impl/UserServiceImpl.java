package com.zlp.mockito.service.impl;

import com.zlp.mockito.dto.LoginUserReq;
import com.zlp.mockito.dto.LoginUserResp;
import com.zlp.mockito.dto.UserAddReq;
import com.zlp.mockito.entity.User;
import com.zlp.mockito.mapper.UserMapper;
import com.zlp.mockito.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Classname UserService
 * @Date 2024/4/14 21:38
 * @Created by ZouLiPing
 */
@Service
@Slf4j(topic = "UserServiceImpl")
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;


    @Override
    public LoginUserResp login(LoginUserReq loginReq) {
        log.info("loginReq:{}", loginReq);
        User user = userMapper.findUserByUsernameAndPassword(loginReq.getUsername(), loginReq.getPassword());
        if (Objects.isNull(user)) {
            throw new RuntimeException("用户名或密码错误");
        }
        LoginUserResp loginUserResp = new LoginUserResp();
        loginUserResp.setId(0L);
        loginUserResp.setUsername(user.getUsername());
        loginUserResp.setNickName(user.getNickname());
        loginUserResp.setToken("token");
        loginUserResp.setPhone("phone");
        loginUserResp.setUserType(0);
        return loginUserResp;
    }


    @Override
    public Boolean createUser(UserAddReq userAddReq) {
        log.info("userAddReq:{}", userAddReq);
        String email = userAddReq.getEmail();
        if (Objects.isNull(email)) {
            throw new RuntimeException("邮箱不能为空");
        }
        if (!email.contains("@example.com")) {
            throw new RuntimeException("邮箱格式不正确");
        }
        userMapper.insert(userAddReq);
        return Boolean.TRUE;
    }



}
