package com.zlp.mockito.controller;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

/**
 * @Classname MockitoStaticTest
 * @Date 2024/4/15 10:46
 * @Created by ZouLiPing
 */
public class MockitoStaticTest {


    /**
     * 测试方法 test01，用于验证 StringUtils 类的 joinWith 方法的功能。
     * 该方法模拟了静态方法 StringUtils.joinWith 的行为，以检查其是否能正确地将列表元素用指定分隔符连接成一个字符串。
     */
    @Test
    public void testJoinWith() {

        // 使用 Mockito 框架模拟 StringUtils 类的静态方法
        try (MockedStatic<StringUtils> stringUtilsMockedStatic = Mockito.mockStatic(StringUtils.class)) {
            // 创建一个字符串列表，作为 joinWith 方法的输入参数
            List<String> stringList = Arrays.asList("a", "b", "c");

            // 配置模拟行为，当调用 StringUtils.joinWith(",", stringList) 时，返回 "a,b,c"
            stringUtilsMockedStatic.when(() -> StringUtils.joinWith(",", stringList)).thenReturn("a,b,c");

            // 断言验证模拟行为是否正确，即 joinWith 方法返回的字符串是否与预期的 "a,b,c" 相等
            Assertions.assertTrue(StringUtils.joinWith(",", stringList).equals("a,b,c"));
        }

    }


    /**
     * 测试StringUtils类中的join方法。
     * 该测试使用Mockito框架来模拟静态方法的行为，验证join方法是否按照预期工作。
     */
    @Test
    public void testJoin() {

        // 使用Mockito模拟StringUtils类的静态方法
        try (MockedStatic<StringUtils> stringUtilsMockedStatic = Mockito.mockStatic(StringUtils.class)) {
            // 创建一个字符串列表作为join方法的输入
            List<String> stringList = Arrays.asList("a", "b", "c");
            // 配置模拟行为，当调用StringUtils.join(",", stringList)时，返回字符串"a,b,c"
            stringUtilsMockedStatic.when(() -> StringUtils.join(",", stringList)).thenReturn("a,b,c");

            // 断言验证模拟行为是否正确，即 join 方法返回的字符串是否与预期的 "a,b,c" 相等
            Assertions.assertTrue(StringUtils.join(",", stringList).equals("a,b,c"));
        }


    }


}
