package com.zlp.mockito.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
class MockitoTestControllerTest {


    @Spy
    private MockitoTestController mockitoTestController;

    @BeforeEach
    void setUp() {

    }

    /**
     * 测试add方法
     * 该方法模拟调用mockitoTestController的add方法，传入参数1和2，期望返回值为3。
     * 首先，通过when语句设置mockitoTestController的add方法返回值为3；
     * 然后，使用assertThat断言验证调用add方法(1, 2)实际返回值确实为3；
     * 最后，通过verify语句确认mockitoTestController的add方法确实被调用了一次，并传入了参数1和2。
     */
    @Test
    void testAdd() {
        // 设置mock对象的行为(打桩)，当调用add(1, 2)时返回4
        when(mockitoTestController.add(1, 2)).thenReturn(4);
        // 调用mock对象的方法,返回为4
        int result = mockitoTestController.add(1, 2);
        log.info("mockitoTestController.add result={}",result);
        // 断言验证：调用add(1, 2)方法返回值是否为4
        assertThat(mockitoTestController.add(1, 2)).isEqualTo(4);
        // 验证：确保add方法(1, 2)被调用了一次
        verify(mockitoTestController,times(2)).add(1, 2);
    }

    /**
     * 测试方法，检查 Mockito 框架的使用。
     * 无参数。
     * 无返回值，但期望通过断言验证操作结果。
     */
    @Test
    void check() {
        // 调用实际的 mockitoTestController 对象的 add 方法，并验证结果是否为预期值
        int result = mockitoTestController.add(1, 2);
        Assertions.assertEquals(3, result);

        // 使用 Mockito 创建 mockitoTest 的 mock 对象，并对它调用 add 方法，然后验证结果
        MockitoTestController mockitoTest = Mockito.mock(MockitoTestController.class);
        int result1 = mockitoTest.add(1, 2);
        Assertions.assertEquals(3, result1);
    }




    /**
     * 测试当调用add方法时抛出RuntimeException异常的情况。
     * 该测试函数不接受参数，也没有返回值。
     */
    @Test
    void testAddException() {
        // 设置mock对象，在调用mockitoTestController的add方法时抛出RuntimeException异常
        when(mockitoTestController.add(1, 2)).thenThrow(new RuntimeException("add error"));

        // 验证是否抛出了RuntimeException异常
        Assertions.assertThrows(RuntimeException.class, () -> mockitoTestController.add(1, 2));

    }


}
