package com.zlp.mockito.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.util.Random;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @Classname RandomTest
 * @Date 2024/4/15 10:46
 * @Created by ZouLiPing
 */
public class RandomTest {







    /**
     * 测试Mockito框架的使用，模拟Random类的nextInt方法。
     * 该测试函数没有参数和返回值，主要用于演示Mockito的基本用法。
     */
    @Test
    public void test01() {

        // 使用Mockito模拟一个Random对象
        Random random = Mockito.mock(Random.class);
        // 调用nextInt()方法，输出随机数，因random 行为为进行打桩，故输出默认值0(random.nextInt() 返回的是int类型)
        System.out.println("第一次："+random.nextInt());
        // 指定当调用nextInt()时，始终返回1
        Mockito.when(random.nextInt()).thenReturn(1);
        System.out.println("第二次："+random.nextInt()); // 再次调用nextInt()，输出应为1
        // 断言nextInt()方法返回值是否为1
        Assertions.assertEquals(2,random.nextInt());
        // 验证nextInt()方法是否被调用了两次
        verify(random, times(3)).nextInt();

    }



}
