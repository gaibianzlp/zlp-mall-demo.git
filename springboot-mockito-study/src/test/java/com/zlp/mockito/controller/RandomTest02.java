package com.zlp.mockito.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Random;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @Classname RandomTest
 * @Date 2024/4/15 10:46
 * @Created by ZouLiPing
 */
@Slf4j(topic = "RandomTest02")
public class RandomTest02 {

    @Mock
    private Random random;


    @BeforeEach
    void setUp() {
        log.info("==============测试前准备===============");
        MockitoAnnotations.openMocks(this);
    }

    /**
     * 测试Mockito框架的使用，模拟Random类的nextInt方法。
     * 该测试函数没有参数和返回值，主要用于演示Mockito的基本用法。
     */
    @Test
    public void test02() {

        // 调用nextInt()方法，输出随机数，因random 行为为进行打桩，故输出默认值0(random.nextInt() 返回的是int类型)
        System.out.println("第一次："+random.nextInt());
        // 指定当调用nextInt()时，始终返回1
        Mockito.when(random.nextInt()).thenReturn(1);
        System.out.println("第二次："+random.nextInt()); // 再次调用nextInt()，输出应为1
        // 断言nextInt()方法返回值是否为1
        Assertions.assertEquals(1,random.nextInt());
        // 验证nextInt()方法是否被调用了两次
        verify(random, times(3)).nextInt();

    }

    @AfterEach
    void tearDown() {
        log.info("==============测试后结果===============");
    }



}
