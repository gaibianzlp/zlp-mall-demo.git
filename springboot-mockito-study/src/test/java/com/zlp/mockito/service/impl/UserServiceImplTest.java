package com.zlp.mockito.service.impl;

import com.zlp.mockito.dto.LoginUserReq;
import com.zlp.mockito.dto.LoginUserResp;
import com.zlp.mockito.dto.UserAddReq;
import com.zlp.mockito.entity.User;
import com.zlp.mockito.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserServiceImpl userService;

    private User testUser;
    private LoginUserReq testLoginReq;
    private LoginUserResp expectedLoginResp;

    private UserAddReq validUserAddReq;
    private UserAddReq invalidEmailUserAddReq;
    private UserAddReq nullEmailUserAddReq;

    @BeforeEach
    public void setUp() {
        testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("testUser");
        testUser.setNickname("TestNick");

        testLoginReq = new LoginUserReq();
        testLoginReq.setUsername("testUser");
        testLoginReq.setPassword("password");

        expectedLoginResp = new LoginUserResp();
        expectedLoginResp.setId(testUser.getId());
        expectedLoginResp.setUsername(testUser.getUsername());
        expectedLoginResp.setNickName(testUser.getNickname());
        expectedLoginResp.setToken("token");
        expectedLoginResp.setPhone("phone");
        expectedLoginResp.setUserType(0);


        validUserAddReq = new UserAddReq();
        validUserAddReq.setUsername("testUser");
        validUserAddReq.setPassword("testPass");
        validUserAddReq.setEmail("test@example.com");

        invalidEmailUserAddReq = new UserAddReq();
        invalidEmailUserAddReq.setUsername("testUser");
        invalidEmailUserAddReq.setPassword("testPass");
        invalidEmailUserAddReq.setEmail("test@example");

        nullEmailUserAddReq = new UserAddReq();
        nullEmailUserAddReq.setUsername("testUser");
        nullEmailUserAddReq.setPassword("testPass");
        nullEmailUserAddReq.setEmail(null);
    }

    /**
     * 测试使用有效的凭据进行登录时，应成功登录。
     *
     * Arrange 配置测试环境：
     * 设置当使用测试请求中的用户名和密码调用 userMapper.findUserByUsernameAndPassword 方法时，
     * 返回预设的测试用户对象。
     *
     * Act 执行动作：
     * 使用测试登录请求调用 userService.login 方法，获取实际的登录响应。
     *
     * Assert 断言结果：
     * 验证实际的登录响应不为空，并且其各个字段（用户名、昵称、令牌、电话、用户类型）与预期的登录响应相匹配。
     *
     * Verify 验证调用：
     * 验证 userMapper.findUserByUsernameAndPassword 方法确实被使用了正确的参数（测试请求中的用户名和密码）调用。
     */
    @Test
    public void whenValidCredentials_thenSuccessfulLogin() {
        // Arrange
        when(userMapper.findUserByUsernameAndPassword(testLoginReq.getUsername(), testLoginReq.getPassword())).thenReturn(testUser);

        // Act
        LoginUserResp actualLoginResp = userService.login(testLoginReq);

        // Assert
        assertNotNull(actualLoginResp);
        assertEquals(expectedLoginResp.getUsername(), actualLoginResp.getUsername());
        assertEquals(expectedLoginResp.getNickName(), actualLoginResp.getNickName());
        assertEquals(expectedLoginResp.getToken(), actualLoginResp.getToken());

        // Verify that the method was called with the correct parameters
        verify(userMapper).findUserByUsernameAndPassword(testLoginReq.getUsername(), testLoginReq.getPassword());
    }


    /**
     * 测试登录服务时，使用无效的用户名和密码应该导致登录失败。
     * 这个测试用例验证当提供的用户名和密码不匹配任何已知用户时，login方法是否抛出运行时异常。
     */
    @Test
    public void whenInvalidCredentials_thenLoginFailure() {
        // 准备测试数据和模拟行为
        when(userMapper.findUserByUsernameAndPassword(testLoginReq.getUsername(), testLoginReq.getPassword())).thenReturn(null);

        // 执行测试方法并验证期望的异常被抛出
        Exception exception = assertThrows(RuntimeException.class, () -> userService.login(testLoginReq));

        // 验证抛出的异常消息是否匹配预期
        assertEquals("用户名或密码错误", exception.getMessage());

        // 验证userMapper的findUserByUsernameAndPassword方法是否被正确调用
        verify(userMapper).findUserByUsernameAndPassword(testLoginReq.getUsername(), testLoginReq.getPassword());
    }


    /**
     * 测试创建用户功能。
     * 当提供的用户信息有效时，应该成功保存用户信息并返回true。
     */
    @Test
    public void createUser_WithValidUser_ShouldPersistAndReturnTrue() {
        // 准备测试环境
        when(userMapper.insert(any(UserAddReq.class))).thenReturn(1);

        // 执行测试动作
        Boolean result = userService.createUser(validUserAddReq);

        // 验证测试结果
        assertTrue(result);
        verify(userMapper).insert(validUserAddReq);
    }


    /**
     * 测试创建用户时使用无效邮箱地址应该抛出异常的情况。
     * 该测试方法不会返回任何值，它的目的是验证当提供一个无效的邮箱地址时，
     * {@link userService.createUser(UserAddReq)} 方法是否会抛出预期的 {@link RuntimeException} 异常。
     *
     * @param none 该测试方法不接受任何参数。
     * @return void 该测试方法没有返回值。
     * @throws RuntimeException 如果提供的用户添加请求中的邮箱地址无效，该方法将抛出异常。
     */
    @Test
    public void createUser_WithInvalidEmail_ShouldThrowException() {
        // 断言当尝试使用无效的邮箱创建用户时，会抛出运行时异常
        Exception exception = assertThrows(RuntimeException.class, () -> {
            userService.createUser(invalidEmailUserAddReq);
        });

        // 验证抛出的异常消息是否为预期的错误消息
        assertEquals("邮箱格式不正确", exception.getMessage());

        // 验证用户映射器的 insert 方法是否从未被调用
        verify(userMapper, never()).insert(any(UserAddReq.class));
    }


    /**
     * 测试创建用户时，如果邮箱为null，应该抛出异常。
     * 这个测试方法不接受任何参数，也不会返回任何值。
     * 它主要通过断言验证在尝试使用null邮箱创建用户时，是否会抛出运行时异常，并且异常的消息文本是否正确。
     */
    @Test
    public void createUser_WithNullEmail_ShouldThrowException() {
        // Act & Assert: 尝试使用null邮箱创建用户，并验证是否抛出了预期的运行时异常
        Exception exception = assertThrows(RuntimeException.class, () -> {
            userService.createUser(nullEmailUserAddReq);
        });

        assertEquals("邮箱不能为空", exception.getMessage()); // 验证异常消息是否正确
        verify(userMapper, never()).insert(any(UserAddReq.class)); // 验证用户映射器的insert方法是否从未被调用
    }

}
