package com.zlp.retry.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
/**
 *  全局重试配置
 */
@Configuration
public class CustomRetryConfig {

    /**
     *
     * 这段代码定义了一个 `CustomRetryConfig` 类，其中包含一个 `retryTemplate` 方法。该方法用于创建并配置一个 `RetryTemplate` 对象，该对象用于处理重试逻辑。
     *
     * 1. 创建 `RetryTemplate` 实例**：创建一个 `RetryTemplate` 对象。
     * 2. 设置重试策略：使用 `SimpleRetryPolicy` 设置最大重试次数为5次。
     * 3. 设置延迟策略：使用 `ExponentialBackOffPolicy` 设置初始延迟时间为1000毫秒，每次重试间隔时间乘以2。
     * 4. 应用策略：将重试策略和延迟策略应用到 `RetryTemplate` 对象。
     */
    @Bean
    public RetryTemplate retryTemplate() {
        RetryTemplate template = new RetryTemplate();

        // 设置重试策略
        SimpleRetryPolicy policy = new SimpleRetryPolicy();
        policy.setMaxAttempts(5);

        // 设置延迟策略
        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(1000);
        backOffPolicy.setMultiplier(2.0);

        template.setRetryPolicy(policy);
        template.setBackOffPolicy(backOffPolicy);

        return template;
    }
}