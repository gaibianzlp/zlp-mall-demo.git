package com.zlp.retry.controller;

import com.zlp.retry.dto.CreateOrderReq;
import com.zlp.retry.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @Classname RetryController
 * @Date 2024/11/18 21:00
 * @Created by ZouLiPing
 */
@RestController
@RequiredArgsConstructor
public class RetryController {

    private final OrderService orderService;



    @GetMapping("getRetry")
    public String retry(){
        CreateOrderReq createOrderReq = new CreateOrderReq();
        createOrderReq.setOrderId(UUID.randomUUID().toString());
        createOrderReq.setProductId("SKU001");
        createOrderReq.setCount(10);
        createOrderReq.setMoney(100);
        return orderService.createOrder(createOrderReq);
    }
}
