package com.zlp.retry.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * The type Create order req.
 *
 * @Classname CreateOrderReq
 * @Date 2024 /11/18 21:04
 * @Created by ZouLiPing
 */
@Data
public class CreateOrderReq implements Serializable {

    private String orderId;
    private String userId;
    private String productId;
    private Integer count;
    private Integer money;
    private String status;
}
