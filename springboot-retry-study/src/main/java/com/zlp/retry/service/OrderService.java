package com.zlp.retry.service;

import com.zlp.retry.dto.CreateOrderReq;

/**
 * @Classname OrderService
 * @Date 2024/11/18 21:03
 * @Created by ZouLiPing
 */
public interface OrderService {

    String createOrder(CreateOrderReq createOrderReq);
}
