package com.zlp.retry.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.zlp.retry.dto.CreateOrderReq;
import com.zlp.retry.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @Classname OrderServiceImpl
 * @Date 2024/11/18 21:06
 * @Created by ZouLiPing
 */
@Service
@Slf4j(topic = "OrderServiceImpl")
public class OrderServiceImpl implements OrderService {
    @Override
    @Retryable(value = {Exception.class},maxAttempts = 4, backoff = @Backoff(delay = 3000))
    public String createOrder(CreateOrderReq createOrderReq) {
        log.info("createOrder.req createOrderReq:{}", JSON.toJSONString(createOrderReq));
        try {
            log.info("createOrder.deductStock.调用时间={}", DateUtil.formatDateTime(DateUtil.date()));
            // 扣减库存服务
            this.deductStock(createOrderReq);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return UUID.randomUUID().toString();
    }

    /**
     * 模拟扣减库存
     */
    private void deductStock(CreateOrderReq createOrderReq) {
        throw new RuntimeException("库存扣减失败");
    }


    @Recover
    public String recover(Exception ex, CreateOrderReq createOrderReq) {
        log.error("recover.resp.重试四次还是失败.error:{},createOrderReq:{}",ex.getMessage(),JSON.toJSONString(createOrderReq));
        // 处理最终失败的情况
        // 可以记录日志,或者是投递MQ,采用最终一致性的方式处理
        return "fail";
    }
}
