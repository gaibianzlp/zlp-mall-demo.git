package com.zlp.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Slf4j(topic = "HtmlToPdfInterceptor")
public class HtmlToPdfInterceptor extends Thread {

    private InputStream is;
 
    HtmlToPdfInterceptor(InputStream is){
        this.is = is;
    }
 
    @Override
    public void run(){
        try{
            InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr);
            String line ;
            while ((line = br.readLine()) != null) {
                log.info(line);//输出内容
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}