package com.zlp.utils;


import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j(topic = "HtmlToPdfUtil")
public class HtmlToPdfUtil {

    /**
     * wkhtmltopdf在系统中的路径
     */
    private static String toPdfTool = Consts.CONVERSION_PLUGSTOOL_PATH_WINDOW;
//    private static String toPdfTool = Consts.CONVERSION_PLUGSTOOL_IMG_PATH_WINDOW;

    /**
     * html转 pdf
     *
     * @param srcPath  html路径，可以是硬盘上的路径，也可以是网络路径
     * @param destPath pdf保存路径
     * @return 转换成功返回true
     */
    public static boolean convert(String srcPath, String destPath) {

        log.info("convert.req srcPath={} ,destPath={}",srcPath,destPath);
        File file = new File(destPath);
        File parent = file.getParentFile();
        // 如果pdf保存路径不存在，则创建路径
        if (!parent.exists()) {
            parent.mkdirs();
        }
        StringBuilder cmd = new StringBuilder();
        if (System.getProperty("os.name").indexOf("Windows") == -1) {
            // 非windows 系统
            toPdfTool = Consts.CONVERSION_PLUGSTOOL_PATH_LINUX;
        }
        cmd.append(toPdfTool);
        cmd.append(" ");
        cmd.append(" \"");
        cmd.append(srcPath);
        cmd.append("\" ");
        cmd.append(" ");
        cmd.append(destPath);

        log.info("execute command={}",cmd.toString());
        boolean result = true;
        try {
            Process proc = Runtime.getRuntime().exec(cmd.toString());
            HtmlToPdfInterceptor error = new HtmlToPdfInterceptor(proc.getErrorStream());
            HtmlToPdfInterceptor output = new HtmlToPdfInterceptor(proc.getInputStream());
            error.start();
            output.start();
            proc.waitFor();
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        return result;
    }

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();
        log.info("generate pdf start ... ");
//        HtmlToPdfUtil.convert("http://baijiahao.baidu.com/s?id=1701222360380918660&wfr=spider&for=pc", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".pdf");
//        HtmlToPdfUtil.convert("https://blog.csdn.net/Hug_rj/article/details/89643290", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".pdf");
//        HtmlToPdfUtil.convert("https://www.suiren.com/work.html", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".pdf");
//        HtmlToPdfUtil.convert("https://www.suiren.com/plan.html", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".pdf");
//        HtmlToPdfUtil.convert("https://www.suiren.com/suiren-power.html", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".jpg");
        HtmlToPdfUtil.convert("http://dev-localcloud.suiren.com/info?id=36", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".jpg");
//        HtmlToPdfUtil.convert("https://blog.csdn.net/yin380697242/article/details/51893397?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase", "D:\\file\\test\\pdf" + StrUtil.SLASH + IdUtil.simpleUUID() + ".jpg");
        log.info("Time-consuming to generate pdf time(ms)={}",System.currentTimeMillis()-startTime);
    }
}