//package com.zlp.utils;
//
//import com.alibaba.fastjson.JSON;
//import com.suiren.linkbrain.enums.AssemblyMsgEnum;
//import com.suiren.linkbrain.service.ReportDataService;
//import com.suiren.linkbrain.socket.SockeUtil;
//import com.suiren.linkbrain.socket.SocketPacket;
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.env.Environment;
//
//import javax.annotation.Resource;
//import java.io.File;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
///**
// * 报告工具类
// */
//@Slf4j(topic = "ReportDataUtil")
//public class ReportDataUtil {
//
//    /**
//     * 截取字符长度
//     * @date: 2021/4/9 14:47
//     * @return:
//     */
//    public final static Integer SUB_LENGTH = 2;
//
//    @Resource
//    private ReportDataService reportDataService;
//
//
//    /**
//     * 解析检测回溯URL
//     * @param data
//     */
//    public static BacktrackDataUrl paraseBacktrackAllData(String data,String suserId){
//
//        BacktrackDataUrl backtrackDataUrl = new BacktrackDataUrl();
//        String userIdHex = data.substring(0, 60);
//        String userId = SockeUtil.convertHexToString(userIdHex);
//        System.out.println(userId);
//        if(userId.equals(suserId)){
//            String d1 = data.substring(60);
//            for (int i = 1; i <= 4 ; i++) {
//                int length = Integer.parseInt(d1.substring(0,SUB_LENGTH), 16)*2;
//                if (i == 1) {
//                    String eggUrl = SockeUtil.convertHexToString(d1.substring(SUB_LENGTH,length+SUB_LENGTH));
//                    backtrackDataUrl.setEggUrl(eggUrl);
//                    d1 = d1.substring(length+SUB_LENGTH);
//                }else if (i == 2 ) {
//                    String filterEggUrl = SockeUtil.convertHexToString(d1.substring(SUB_LENGTH,length+SUB_LENGTH));
//                    backtrackDataUrl.setFilterEggUrl(filterEggUrl);
//                    d1 = d1.substring(length+SUB_LENGTH);
//                }else if (i == 3 ) {
//                    String nearUrl = SockeUtil.convertHexToString(d1.substring(SUB_LENGTH,length+SUB_LENGTH));
//                    backtrackDataUrl.setNearUrl(nearUrl);
//                    d1 = d1.substring(length+SUB_LENGTH);
//                }else if (i == 4 ) {
//                    String eyeUrl = SockeUtil.convertHexToString(d1.substring(SUB_LENGTH,length+SUB_LENGTH));
//                    backtrackDataUrl.setEyeUrl(eyeUrl);
//                }
//            }
//        } else {
//            log.info("检测信息不符！！！");
//        }
//        log.info("paraseBacktrackData.resp backtrackDataUrl={}", JSON.toJSONString(backtrackDataUrl));
//        return backtrackDataUrl;
//    }
//
//
//    public static void main(String[] args) {
//
//        String data = "0A0B0C09FF01734831383033323131303138353034394230393034323131353435343034325544" +
//                "3A5C5275616E4A69616E5C616C676F726974686D5C646174615C7573657249647265706F727449645C483138" +
//                "3033323131303138353034395C4230393034323131353435343034325C6565675F7261772E74787458443A5C5275616E4A69616E5C616C676F726974686D5C646174615C7573657249647265706F727449645C4831383033323131303138353034395C4230393034323131353435343034325C6565675F66696C7465722E74787453443A5C5275616E4A69616E5C616C676F726974686D5C646174615C7573657249647265706F727449645C4831383033323131303138353034395C4230393034323131353435343034325C666E6972732E74787451443A5C5275616E4A69616E5C616C676F726974686D5C646174615C7573657249647265706F727449645C48" +
//                "31383033323131303138353034395C4230393034323131353435343034325C6579652E747874CC0D0A";
//
//
//        SocketPacket sp = SocketPacket.parseMsg(data);
//
//        String userId = "H18032110185049B09042115454042";
//
//        paraseBacktrackAllData(sp.getData(),userId);
//    }
//
//    /**
//     * 处理报告结果集
//     * @param data
//     * @param suserId
//     * @date: 2021/4/16 16:32
//     * @return: void
//     */
//    public static List<BacktrackResult> paraseProcessResultData(String data, String suserId) {
//
//        List<BacktrackResult> backtrackResultList = new ArrayList<>();
//
//        String userIdHex = data.substring(0, 60);
//        String userId = SockeUtil.convertHexToString(userIdHex);
//        System.out.println(userId);
//        if(userId.equals(suserId)){
//            String d1 = data.substring(60);
//            for (int i = 1; i <= 5 ; i++) {
//                BacktrackResult backtrackResult = new BacktrackResult();
//                int length = Integer.parseInt(d1.substring(0,SUB_LENGTH), 16)*2;
//                // TODO: 2021/4/16  更新 lb_report_singularity 风险值 报告表状态为20
//                backtrackResultList.add(backtrackResult);
//            }
//
//        } else {
//            log.info("检测信息不符！！！");
//        }
//        return backtrackResultList;
//
//    }
//
//    @Data
//    public static class  BacktrackResult{
//
//        /**
//         * 任务序号
//         */
//        Integer taskNumber;
//
//        /**
//         * AD可能性(百分比)
//         */
//        BigDecimal adPercentage;
//
//
//
//    }
//
//
//    @Data
//    public static class  BacktrackDataUrl{
//
//        /**
//         * 脑电URL
//         */
//        String eggUrl;
//
//        /**
//         * 脑电过滤波后
//         */
//        String filterEggUrl = File.separator + "javafile" + File.separator + "test" + File.separator + "eeg_filter.txt";
//
//        /**
//         * 近红外URL
//         */
//        String nearUrl = File.separator + "javafile" + File.separator + "test" + File.separator + "nirsit.txt" ;
//
//        /**
//         * 眼动URL
//         */
//        String eyeUrl = File.separator + "javafile" + File.separator + "test" + File.separator + "pupil.json";
//
//        String erpUrl ;
//
//        String connectionUrl ;
//
//        String midLeft ;
//
//        String midRight ;
//
//        String nearRight ;
//
//        String pdfUrl ;
//
//        String memoryAnswerUrl ;
//
//    }
//}
