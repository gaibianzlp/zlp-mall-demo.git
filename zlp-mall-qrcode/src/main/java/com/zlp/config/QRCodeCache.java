package com.zlp.config;

import com.zlp.dto.QRCode;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j(topic = "QRCodeCache")
public class QRCodeCache {

    public static Map<String, QRCode> cacheMap = new ConcurrentHashMap<>();

    public static QRCodeCache instance = null;

    private QRCodeCache() {

    }

    public static QRCodeCache getInstance() {
        if (instance == null) {
            instance = new QRCodeCache();
        }
        return instance;
    }


    public void putQRCodeCache(String key, QRCode qrCode) {
        cacheMap.put(key, qrCode);
    }

    public QRCode getQRCodeCache(String key) {
        return cacheMap.get(key);
    }

    public void removeQRCodeCache(String key) {
        cacheMap.remove(key);
    }

}