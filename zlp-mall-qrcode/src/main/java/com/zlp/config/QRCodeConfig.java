package com.zlp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
@ConfigurationProperties(prefix="qrcode")
public class QRCodeConfig {

    private int width;
    private int height;
    private long expireTime;
    private String filePath;
    private String fileType;
}