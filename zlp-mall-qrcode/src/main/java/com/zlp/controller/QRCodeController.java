package com.zlp.controller;

import com.google.zxing.WriterException;
import com.zlp.config.QRCodeCache;
import com.zlp.config.QRCodeConfig;
import com.zlp.dto.QRCode;
import com.zlp.service.QRCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.util.UUID;

@Slf4j
@RestController
@Api(value = "QRCodeController",tags = "二维码模块")
@AllArgsConstructor
public class QRCodeController {


    private final QRCodeService qrCodeService;
    
    private final QRCodeConfig qrCodeConfig;
    
    @ApiOperation("生成二维码")
    @PostMapping("/generatorQRCode")
    public String generatorQRCode(){

        String baseUrl = "跳转页面";
        String key = UUID.randomUUID().toString();
        String path = null;
        try {
            path = qrCodeService.generateQRCodeImage(baseUrl,key);
            QRCode qrCode = new QRCode();
            long expireTime = qrCode.getCreateTime() + qrCodeConfig.getExpireTime();
            qrCode.setExpireTime(expireTime);
            QRCodeCache.getInstance().putQRCodeCache(key, qrCode);
            return path;
        } catch (WriterException e) {
            log.info("生成二维码失败：" + e.getMessage());
            return path;
        } catch (IOException e) {
            log.info("生成二维码失败：" + e.getMessage());
            return path;
        }    
    }
    
    @PostMapping("/generator-scan")
    @ApiOperation("扫码二维码")
    public String scanQRCode(String uuid){

        QRCode qrCode = QRCodeCache.getInstance().getQRCodeCache(uuid);
        if(null ==qrCode){
            return "二维码已过期，请刷新获取";
        }
        if(qrCode.isScan()){
            return "二维码扫码过，请刷新获取";
        }
        if(qrCode.getExpireTime() < System.currentTimeMillis()){
            return "二维码已过期，请刷新获取";
        }
        qrCode.setScan(true);
        return "登录页面";
    }
    
    @PostMapping("/generator-hand")
    @ApiOperation("处理二维码")
    public String handQRCode(String uuid){

        QRCode qrCode = QRCodeCache.getInstance().getQRCodeCache(uuid);
        if(null ==qrCode){
            return "二维码已过期，请刷新获取";
        }

        if(qrCode.getExpireTime() < System.currentTimeMillis()){
            return "二维码已过期，请刷新获取";
        }
        QRCodeCache.getInstance().removeQRCodeCache(uuid);
        return "登录成功返回逻辑处理";
    }
}