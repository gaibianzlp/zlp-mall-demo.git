package com.zlp.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class QRCode {

    private Long createTime = System.currentTimeMillis();
    
    private Long expireTime;
        
    private boolean isScan = false;
}