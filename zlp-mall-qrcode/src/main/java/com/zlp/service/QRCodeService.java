package com.zlp.service;

import com.google.zxing.WriterException;

import java.io.IOException;

public interface QRCodeService {

    /**
     * 生成二维码图片
     * @param basePath
     * @param key
     * @date: 2021/6/10 17:37
     * @return: java.lang.String
     */
    String generateQRCodeImage(String basePath,String key) throws WriterException, IOException;
}
