package com.zlp.service.impl;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.zlp.config.QRCodeConfig;
import com.zlp.service.QRCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

@Service
public class QRCodeServiceImpl implements QRCodeService {

    @Autowired
    QRCodeConfig qrCodeConfig;


    @Override
    public String generateQRCodeImage(String basePath,String key) throws WriterException, IOException {

        String text = basePath + "/" + key;
        String path = qrCodeConfig.getFilePath() + File.separator + key +"." + qrCodeConfig.getFileType().toLowerCase();

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, qrCodeConfig.getWidth(), qrCodeConfig.getHeight());
        Path filePath = FileSystems.getDefault().getPath(path);
        MatrixToImageWriter.writeToPath(bitMatrix, qrCodeConfig.getFileType(), filePath);
        return path;
    }
}
