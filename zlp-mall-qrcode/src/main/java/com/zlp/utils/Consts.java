package com.zlp.utils;

public interface Consts {

    /**
     * wkhtmltopdf在系统中的路径-WINDOWS
     */
    String CONVERSION_PLUGSTOOL_PATH_WINDOW = "D:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
    String CONVERSION_PLUGSTOOL_IMG_PATH_WINDOW = "D:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltoimage.exe";

    /**
     * wkhtmltopdf在系统中的路径-LINUX
     */
    String CONVERSION_PLUGSTOOL_PATH_LINUX = "usr/local/bin/wkhtmltopdf.exe";
}
