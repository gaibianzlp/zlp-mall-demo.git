package com.zlp.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * RocketMqApp
 * @Date 2023年11月29日20:53:12
 * @Created by ZouLiPing
 */
@SpringBootApplication
public class RocketMqApp {

    public static void main(String[] args) {
        SpringApplication.run(RocketMqApp.class, args);
    }
}