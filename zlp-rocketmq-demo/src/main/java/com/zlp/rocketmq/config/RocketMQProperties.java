package com.zlp.rocketmq.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * MQ配置属性类
 * @author gubin
 * @date 2023-09-28
 */
@Data
@Component
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMQProperties {

    private String nameServer;
    private final Producer producer = new Producer();
    private final Consumer consumer = new Consumer();

    @Data
    public static class Producer {
        private String accessKey;
        private String secretKey;
        private boolean tlsEnable;
        private boolean enableMsgTrace;
        private Spu spu = new Spu();
        private Category category = new Category();
        private OrderCreateOG orderCreateOG = new OrderCreateOG();
    }

    @Data
    public static class Consumer {
        private String accessKey;
        private String secretKey;
        private boolean tlsEnable;
        private boolean enableMsgTrace;
        private Spu spu = new Spu();
        private Category category = new Category();
        private OrderCreateOG orderCreateOG = new OrderCreateOG();
    }

    @Data
    public static class Spu {
        private String topic;
        private String gid;
        private String tag;
    }

    @Data
    public static class Category {
        private String topic;
        private String gid;
        private String tag;
    }

    /**
     * OG创建订单，延迟查询队列
     */
    @Data
    public static class OrderCreateOG {
        private String topic;
        private String gid;
        private String tag;
        private Integer delay;
    }

}
