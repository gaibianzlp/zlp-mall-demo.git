package com.zlp.rocketmq.controller;

import com.zlp.rocketmq.producer.CreateOrderOGProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname MqTestController
 * @Description TODO
 * @Date 2023/11/29 21:17
 * @Created by ZouLiPing
 */
@RestController
@RequiredArgsConstructor
public class MqTestController {

    private final CreateOrderOGProducer createOrderOGProducer;


    /**
     *  发送延迟订单消息
     */
    @GetMapping("sendDelayedOrderMessage")
    public void sendDelayedOrderMessage(String orderNo){
        createOrderOGProducer.placeOGOrderCreateSendToMq(orderNo);

    }



}
