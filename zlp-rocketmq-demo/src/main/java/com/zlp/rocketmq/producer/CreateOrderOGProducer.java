package com.zlp.rocketmq.producer;

import com.alibaba.fastjson.JSON;
import com.zlp.rocketmq.config.RocketMQProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Classname CreateOrderOGProducer
 * @Date 2023/11/08 9:59
 * @Created by qianna
 */
@Slf4j(topic = "CreateOrderOGProducer")
@Component
public class CreateOrderOGProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Resource
    private RocketMQProperties properties;

    /**
     * 下单成功发送成功延迟消息监听单据状态
     *
     * @param orderNo 订单号
     */
    public void placeOGOrderCreateSendToMq(String orderNo) {
        log.info("placeOGOrderCreateSendToMq-request={}", orderNo);
        SendResult sendResult = rocketMQTemplate.syncSend(properties.getProducer().getOrderCreateOG().getTopic()
                        + ":" + properties.getProducer().getOrderCreateOG().getTag(), MessageBuilder.withPayload(orderNo)
                        .setHeader(RocketMQHeaders.KEYS, properties.getProducer().getOrderCreateOG().getGid())
                        .setHeader(RocketMQHeaders.TAGS, properties.getProducer().getOrderCreateOG().getTag()).build()
                ,3000,properties.getProducer().getOrderCreateOG().getDelay());
        log.info("sendResult: {}  ", JSON.toJSONString(sendResult));
    }

}